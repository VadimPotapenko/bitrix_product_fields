// файл подключения модулей приложения
let link = document.createElement('a'), dataLink;
link.href = document.URL;
dataLink = link.pathname.split('/');
const PATH = dataLink.filter((value, index) => !(index == dataLink.length - 1)).join('/');
const FIELDS_TYPE = {
	string:      'text',
	integer:     'number',
	product_file:'file',
	double:      'number',
	map:         'text',
	char:        'checkbox',
	text:        'textarea',
	date:        'date',
	datetime:    'datetime',
	list:        'select',
	employee:    'employee',
	crm:         'crm'
};
let click = false;
//============================ settings =====================================//
[...document.querySelectorAll('.path')].map(value => {
	value.addEventListener('click', function (e) {
		let func = e.target.dataset.path.split('/')[2];
		getData (document.querySelector('.display'), PATH + e.target.dataset.path, func);
		click = true;
	});
});

if (!click)
	getData(document.querySelector('.display'), PATH + '/apps/list_product/index.php', 'list_product');

// подключаем приложение и js скрипт
async function getData (target, url, func) {
	let wait = document.querySelector('.wait');
	wait.style.display = 'block';
	let response = await fetch(url);
	if (response.status == 200) {
		target.innerHTML = await response.text();
		wait.style.display = 'none';
		window[func]();
	} else
		wait.children[0].innerText = 'Произошла ошибка. Перезагрузите страницу';
}