<?php require_once __DIR__.'/settings.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Приложение расширенная карточка товара</title>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/form.css">
	<link rel="stylesheet" type="text/css" href="assets/css/add_product.css">
	<link rel="stylesheet" type="text/css" href="assets/css/product_list.css">
	<link rel="stylesheet" type="text/css" href="assets/css/setting.css">
</head>
<body>
	<div class='container-fluid'>
		<div class='row'>
			<!-- боковые ссылки -->
			<div class='mainborder'>
				<?php if (in_array($currentUser['user']['ID'], $responsible)): ?>
					<div class='path links1 mt-3' data-path=<?='/apps/form/index.php';?>></div>
					<div class='path links2' data-path=<?='/apps/add_product/index.php';?>></div>
					<div class='path links3' data-path=<?='/apps/list_product/index.php';?>></div>
					<div class='path links4' data-path=<?='/apps/setting/index.php';?>></div>
				<?php else: ?>
					<?php if($rules[$currentUser['user']['ID']]['editor'] == 'true'): ?>
						<div class='path links1 mt-3' data-path=<?='/apps/form/index.php';?>></div>
					<?php endif; ?>

					<?php if($rules[$currentUser['user']['ID']]['add'] == 'true'): ?>
						<div class='path links2 mt-3' data-path=<?='/apps/add_product/index.php';?>></div>
					<?php endif; ?>

					<?php if($rules[$currentUser['user']['ID']]['table'] == 'true'): ?>
						<div class='path links3 mt-3' data-path=<?='/apps/list_product/index.php';?>></div>
					<?php endif; ?>
				<?php endif; ?>
			</div>
			<!-- центральная панель -->
			<div class='col display'></div>
		</div>
	</div>

	<!-- различные уведомления -->
	<div class='context'></div>
	<div class='context-detail'></div>
	<div class='wait'><span>Идет загрузка данных...</span></div>
	<div class='display-address'></div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<script type="text/javascript">
		BX24.resizeWindow(frames.innerWidth,window.innerHeight);
		// данные с сервера для js
		let POST = <?= json_encode($_POST); ?>;                              // post
		let HOST = '<?= $HOST ?>';                                          // доменное имя
		let USERS = <?= json_encode($appsConfig['USERS']); ?>;             // активные пользователи портала
		let SECTIONS = <?= json_encode($appsConfig['SECTIONS_LIST']); ?>; // разделы товаров
		let FIELDS = <?= json_encode($appsConfig['PRODUCT_FIELDS']); ?>; // поля товаров   
		let CURRENT_USER = <?= json_encode($currentUser); ?>;           // текущий пользователь
		let DEAL_CONTEXT = <?= json_encode($DEAL_CONTEXT); ?>;         // сделка

		// .. пользовательские переменные .. //
		const PUBLISH = '<?=PUBLISH;?>', NOT_PUBLISH = '<?=NOT_PUBLISH;?>';
		const ADDRESS_CLASSIFIER = '<?=ADDRESS_CLASSIFIER;?>';
		const PHONE_FINDER = '<?=PHONE_FINDER;?>';
		const FLAT_NUMBER = '<?=FLAT_NUMBER;?>';
		const CITY        = '<?=CITY;?>';
		const INDEX       = '<?=INDEX;?>';
		const HOME_NUMBER = '<?=HOME_NUMBER;?>';
		const REGION      = '<?=REGION;?>';
		const COUNTRY     = '<?=COUNTRY;?>';
		const STREET      = '<?=STREET;?>';
		const LATITUDE    = '<?=LATITUDE;?>';
		const LONGITUDE   = '<?=LONGITUDE;?>';
		const DADATA_TOKEN = '<?=DADATA_TOKEN;?>';
		const RESPONSE_MANAGER = '<?=RESPONSE_MANAGER;?>';
	</script>
	<script src='apps/form/view/js/form.js'></script>
	<script src='apps/add_product/view/js/add_product.js'></script>
	<script src='apps/list_product/view/js/list_product.js'></script>
	<script src='apps/setting/view/js/setting.js'></script>
	<script src='assets/js/main.js'></script>
</body>
</html>