<?php
/**
* Возвращает значения списочных полей
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
$option = array();
#====================== settings ==========================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);
$option = getValueListProperty(array('id' => $json_arr));
echo json_encode($option);