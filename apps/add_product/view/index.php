<div class='container-fluid'>
	<div class='row'>
		<!-- панель с разделами товаров -->
		<div class='col p-2 topborder'>
			<form class='form-inline'>
				<div class="form-group">
	    			<select class="form-control pr-5 pl-5 mr-5 ml-3 section-id" id="exampleFormControlSelect1">
	      				<?php foreach($appsConfig['SECTIONS'] as $key => $value): ?>
	      					<?php foreach($value as $k => $v): ?>
	      						<?php if(is_array($v)): ?>

	      							<?php foreach($v as $id => $data): ?>
	      								<?php if(is_array($data)): ?>
	      									<option value="<?=$id;?>"><?='-- '.current($data);?></option>
	      								<?php else: ?>
	      									<option value="<?=$k;?>"><?='- '.$data;?></option>
	      								<?php endif; ?>
	      							<?php endforeach; ?>

	      						<?php else: ?>
	      							<option value="<?=$key;?>"><?=$v;?></option>
	      						<?php endif; ?>
	      					<?php endforeach; ?>
	      				<?php endforeach; ?>
	    			</select>
	    			<button type='button' class='btn btn-info form-control pr-5 pl-5 save-editor'>Выбрать</button>
	  			</div>
	  		</form>
		</div>
	</div>

	<!-- подгружаемая форма -->
	<div class='row'>
		<div class='col'>
			<div class='form-creator'></div>
		</div>
	</div>

	<div class="alert alert-danger" role="alert">Не найдено шаблона формы для этого раздела!</div>
	<div class="alert alert-success" role="alert"></div>
</div>