// скрипты клиентской части модуля добавление товара
function add_product () {
	const CURRENT_ADDRESS = {};
	document.querySelector('.save-editor').addEventListener('click', function (e) { // при клике на кнопку грузим и рендерим форму
		getFields(document.querySelector('.section-id').value, createForm);
	});

	// загрузка формы
	async function getFields (section, callback) {
		let response = await fetch(PATH + '/apps/add_product/get_form.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify('SECTION_' + section)
		});

		if (response.ok) {
			let json = await response.json();
			callback(json.form);
			console.log('Answer server: OK');
		} else
			console.log("Error HTTP: " + response.status);
	}

	// рендеринг формы
	function createForm (data) {
		if (data.result.length > 0) {
			let editor = JSON.parse(data.result[0].PROPERTY_VALUES.EDITOR);
			let logic = JSON.parse(data.result[0].PROPERTY_VALUES.LOGIC);

			let displayForm = document.querySelector('.form-creator');
			displayForm.classList.add('display-form');
			displayForm.innerHTML = '';

			let formName = document.createElement('p');
			formName.classList.add('h3');
			formName.innerText = editor.formName;

			let form = document.createElement('form');
			form.enctype = 'multipart/form-data';
			form.name = 'formreact';
			form.classList.add('loadForm');
			let row = document.createElement('div');
			row.classList.add('row');
			let left = document.createElement('div');
			left.classList.add('col');
			left.innerHTML = createFields(editor.left, Object.values(logic.required));
			let right = document.createElement('div');
			right.classList.add('col');
			right.innerHTML = createFields(editor.right, Object.values(logic.required));

			let btn = document.createElement('button');
			btn.innerText = 'Отправить';
			btn.type = 'button';
			btn.classList.add('btn');
			btn.classList.add('btn-block');
			btn.classList.add('btn-lg');
			btn.classList.add('bg-info');
			btn.classList.add('mt-5');
			btn.classList.add('text-white');
			btn.addEventListener('click', addProduct);

			row.append(left);
			row.append(right);
			form.append(row);
			form.append(btn);
			displayForm.append(formName);
			displayForm.append(form);
			// логика if else
			setIfElse(logic.ifelse);
			bindAddress();

		} else {
			let alert = document.querySelector('.alert-danger');
			alert.style.display = 'block';
			setTimeout(() => alert.style.display = 'none', 4000);
		}
	}

	function createFields (fields, requiredFields) {
		let returnHTML = '', x = 0;
		for (let i in fields) {
			if (FIELDS_TYPE[fields[i].split('|')[1]] == 'textarea' || i == 'DESCRIPTION') {
				returnHTML += `<div class="form-group">
					<label for='${i + '_' + x}'>${requiredFields.includes(i) ? '*' : ''}${fields[i].split('|')[0]}:</label>
					<textarea 
						${requiredFields.includes(i) ? 'required' : ''}
						name='${i}${fields[i].split('|')[2] == 'Y' ? '[]' : ''}' id='${i + '_' + x}'
						class="form-control" rows="3" 
						placeholder='${fields[i].split('|')[0]}'
					></textarea>
				</div>`;

			} else if (FIELDS_TYPE[fields[i].split('|')[1]] == 'select') {
				returnHTML += `<div class="form-group">
					<label for='${i + '_' + x}'>${requiredFields.includes(i) ? '*' : ''}${fields[i].split('|')[0]}:</label>
					<select
						id='${i + '_' + x}'
						${requiredFields.includes(i) ? 'required' : ''}
						name='${i}${fields[i].split('|')[2] == 'Y' ? '[]' : ''}'
						class="form-control"
						${fields[i].split('|')[2] == 'Y' ? 'multiple' : ''}
					><option value=''>Значение не установлено</option></select>
				</div>`;
				getList(i.split('_')[1], createOptions, i + '_' + x);

			} else if (FIELDS_TYPE[fields[i].split('|')[1]] == 'crm') {
				returnHTML += `<div class="form-group">
					<label class='col-12' for='${i + '_' + x}'>${requiredFields.includes(i) ? '*' : ''}${fields[i].split('|')[0]}:</label>
					<input 
						${requiredFields.includes(i) ? 'required' : ''}
						type='text'
						class='form-control'
						name='${i}'
						id='${i + '_' + x}'
					></div>`;
				setCRMSetting(i + '_' + x, FIELDS[i]);

			} else if (FIELDS_TYPE[fields[i].split('|')[1]] == 'employee') {
				returnHTML += `<div class="form-group">
					<label class='col-12' for='${i + '_' + x}'>${requiredFields.includes(i) ? '*' : ''}${fields[i].split('|')[0]}:</label>
					<select class='form-control' name='${i}' id='${i + '_' + x}' ${requiredFields.includes(i) ? 'required' : ''}>`;
				for (let i in USERS)
					returnHTML += `<option ${CURRENT_USER['user']['ID'] == i ? 'selected' : ''} value='${i}'>${USERS[i]}</option>`;
				
				returnHTML += '</select></div>';

			} else if (i == 'CURRENCY_ID') {
				returnHTML += `<div class="form-group">
					<label for='${i + '_' + x}'>${requiredFields.includes(i) ? '*' : ''}${fields[i].split('|')[0]}:</label>
					<select name='${i}' id='${i + '_' + x}' class='form-control'>
						<option value='RUB'>Российский рубль</option>
						<option value='USD'>Доллар США</option>
						<option value='EUR'>Евро</option>
						<option value='UAH'>Гривна</option>
						<option value='BR'>Белорусский доллар</option>
					</select>
				</div>`;

			} else {
				returnHTML += `<div class="form-group">
					<label for='${i + '_' + x}'>${requiredFields.includes(i) ? '*' : ''}${fields[i].split('|')[0]}:</label>
					<input 
						${requiredFields.includes(i) ? 'required' : ''}
						class='mb-2 ${
							FIELDS_TYPE[fields[i].split('|')[1]] == 'file' 
								? 'form-control-file' 
								: FIELDS_TYPE[fields[i].split('|')[1]] == 'checkbox' ? 'form-check-label' : 'form-control'
						}' type='${FIELDS_TYPE[fields[i].split('|')[1]]}' id='${i + '_' + x}' name='${i}${fields[i].split('|')[2] == 'Y' ? '[]' : ''}' ${
							FIELDS_TYPE[fields[i].split('|')[1]] == 'file' ? "multiple" : `placeholder='${fields[i].split('|')[0]}'`
				}></div>`;
			}
			x+=1;
		}
		return returnHTML
	}

	// получаем значения списочных полей
	async function getList (id, callback, x) {
		let response = await fetch(PATH + '/apps/add_product/get_list_value.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify(id)
		});

		if (response.ok) {
			let json = await response.json();
			let options = callback(json, x);
			console.log('Answer server: OK');
		} else
			console.log("Error HTTP: " + response.status);
	}

	function createOptions (list, x) {
		let select = document.querySelector(`#${x}`);
		for (let i in list)
			select.innerHTML += `<option value='${i}'>${list[i]}</option>`
	}

	// получаем данные формы
	function addProduct () {
		let form = [...document.querySelector('.loadForm')];
		let formData = new FormData();
		for (let i = 0; i < form.length - 1; i++) {
			if (form[i].tagName == 'INPUT' && form[i].type == 'file') {
				for (let x = 0; x < form[i].files.length; x++)
					formData.append(form[i].name, form[i].files[x]);
				continue;

			} else if (form[i].name == ADDRESS_CLASSIFIER) {
				for (let i in CURRENT_ADDRESS)
					formData.append(i, CURRENT_ADDRESS[i]);
				formData.append(form[i].name, form[i].value);
				continue;

			} else if (form[i].type == 'checkbox') {
				formData.append(form[i].name, ((form[i].checked == true) ? 'Y' : 'N'));
				continue;

			} else if (form[i].tagName == 'SELECT' && form[i].multiple == true) {
				let multiArr = [];
				for (let j = 0; j < form[i].length; j++) {
					if (form[i][j].selected == true)
						multiArr.push(form[i][j].value);
				}
				formData.append(form[i].name, multiArr);
				continue;

			} else if (form[i].tagName == 'SELECT' && form[i].dataset.crm == '1') {
				formData.append(form[i+1].name, (form[i].value + '_' + form[i+1].value));
				continue;

			} else if (form[i].tagName == 'INPUT' && form[i].dataset.crm == '1') continue;
			formData.append(form[i].name, form[i].value);
		}

		let section = document.querySelector('.section-id');
		let sectionName = '';
		for (let i = 0; i < section.length; i++) {
			if (section[i].selected == true)
				sectionName = section[i].value + '|' + section[i].innerText;
		}
		formData.append('SECTION_ID', sectionName);

		if (checkRequiredFields(form)) requestProduct(formData);
		else {
			let alert = document.querySelector('.alert-danger');
			alert.innerText = 'Обязательное поле не заполнено!';
			alert.style.display = 'block';
			setTimeout(() => alert.style.display = 'none', 4000);
		}
	}

	// добавление товара
	async function requestProduct (product) {
		let response = await fetch(PATH + '/apps/add_product/add_product.php', {
			method: 'POST',
			body: product
		});

		if (response.ok) {
			let json = await response.json();
			console.log('Answer server: OK');
			window.location.reload();

		} else {
			let alert = document.querySelector('.alert-danger');
			alert.innerText = 'Ошибка! Запрос не обработан, перезагрузите страницу и повторите действие.';
			alert.style.display = 'block';
			setTimeout(() => alert.style.display = 'none', 4000);
			console.log("Error HTTP: " + response.status);
		}
	}

	// логика обязательные поля
	function checkRequiredFields (form) {
		let control = true;
		for (let i = 0; i < form.length - 1; i++) {
			if (['text', 'number'].includes(form[i].type) && form[i].required === true && form[i].value === '') {
				form[i].labels[0].style.color = 'red';
				setTimeout(() => form[i].labels[0].style.color = '#303030', 6500);
				control = false;

			} else if (['checkbox'].includes(form[i].type) && form[i].required === true && form[i].checked === false) {
				form[i].labels[0].style.color = 'red';
				setTimeout(() => form[i].labels[0].style.color = '#303030', 6500);
				control = false;

			} else if (['file'].includes(form[i].type) && form[i].required === true && form[i].files.length === 0) {
				form[i].labels[0].style.color = 'red';
				setTimeout(() => form[i].labels[0].style.color = '#303030', 6500);
				control = false;

			} else if (form[i].tagName == 'SELECT' && form[i].required === true && form[i].value === '' && form[i].attributes.name.value !== 'crmType') {
				form[i].labels[0].style.color = 'red';
				setTimeout(() => form[i].labels[0].style.color = '#303030', 6500);
				control = false;
			}
		}
		return control;
	}

	// логика ifelse
	function setIfElse (ifElseRule) {
		for (let i = 0; i < ifElseRule.length; i++) {
			document.querySelector('[name=' + ifElseRule[i].if +']').addEventListener('change', function (e) {
				let arrayElse = ifElseRule[i].to.split('|');
				let index = arrayElse.shift();
				let objectElse = {[index]:arrayElse.join('|')};
				let elseElement = document.querySelector('[name=' + index + ']');
				if (['text', 'number'].includes(e.target.type)) {
					if (e.target.value !== '' && !elseElement) {
						let item = document.createElement('div');
						item.innerHTML = createFields(objectElse, []);
						e.target.parentNode.parentNode.insertBefore(item.children[0], e.target.parentNode.nextSibling);
					} else if (e.target.value === '' && elseElement) elseElement.parentNode.remove();
				}

				if (['checkbox'].includes(e.target.type)) {
					if (e.target.checked === true && !elseElement) {
						let item = document.createElement('div');
						item.innerHTML = createFields(objectElse, []);
						e.target.parentNode.parentNode.insertBefore(item.children[0], e.target.parentNode.nextSibling);
					} else if ((e.target.checked !== true && elseElement)) elseElement.parentNode.remove();
				}

				if (['file'].includes(e.target.type)) {
					if (e.target.files.length > 0 && !elseElement) {
						let item = document.createElement('div');
						item.innerHTML = createFields(objectElse, []);
						e.target.parentNode.parentNode.insertBefore(item.children[0], e.target.parentNode.nextSibling);
					} else if (e.target.files.length === 0 && elseElement) elseElement.parentNode.remove();
				}
			});
		}
	}

	// адресный класификатор
	function bindAddress () {
		[...document.querySelectorAll(`[name="${ADDRESS_CLASSIFIER}"]`)]
		.forEach (value => value.addEventListener('input', function (event) {
			let url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
			let token = "69b8e1ce17ea7c4b210685752ed28b822efbd96b";
			let query = event.target.value;
			
			let options = {
			    method: "POST",
			    mode: "cors",
			    headers: {
			        "Content-Type": "application/json",
			        "Accept": "application/json",
			        "Authorization": "Token " + token
			    },
			    body: JSON.stringify({query: query})
			}

			fetch(url, options)
			.then(response => response.json())
			.then(result => renderAddress(result, event.target))
			.catch(error => console.log("error", error));
			event.preventDefault();
		}));
	}

	function renderAddress(data, target) {
		const field = target;
		let coords = getCoords(target);
		let display = document.querySelector('.display-address');
		display.style['top'] = coords['top'] + 'px';
		display.style['left'] = coords['left'] + 'px';
		display.style['display'] = 'block';
		display.innerHTML = '';

		if (data.suggestions) {
			for (let i = 0; i < data.suggestions.length; i++) {
				let value = document.createElement('div');
				value.classList.add('address-value');
				value.addEventListener('click', addValue);
				let title = document.createElement('span');
				title.classList.add('ml-3');
				title.innerText = data.suggestions[i].value;
				title.dataset.index = i;
				value.append(title);
				display.append(value);
			}
		}

		function addValue (e) {
			let index = this.children[0].dataset.index;
			CURRENT_ADDRESS[FLAT_NUMBER] = data['suggestions'][index]['data']['flat'] || '';
			CURRENT_ADDRESS[CITY]        = data['suggestions'][index]['data']['city_with_type'] || '';
			CURRENT_ADDRESS[INDEX]       = data['suggestions'][index]['data']['postal_code'] || '';
			CURRENT_ADDRESS[HOME_NUMBER] = data['suggestions'][index]['data']['house'] || '';
			CURRENT_ADDRESS[REGION]      = data['suggestions'][index]['data']['region_with_type'] || '';
			CURRENT_ADDRESS[COUNTRY]     = data['suggestions'][index]['data']['country'] || '';
			CURRENT_ADDRESS[STREET]      = data['suggestions'][index]['data']['street_with_type'] || '';
			CURRENT_ADDRESS[LATITUDE]    = data['suggestions'][index]['data']['geo_lat'] || '';
			CURRENT_ADDRESS[LONGITUDE] = data['suggestions'][index]['data']['geo_lon'] || '';
			field.value = this.children[0].innerText;
			this.parentNode.style['display'] = 'none';
		}
	}

	function getCoords (elem) {
		let box = elem.getBoundingClientRect();
		return {
			top: box.top + pageYOffset + 40,
			left: box.left + pageXOffset
		};
	}

	// поиск собственика по телефону
	function setCRMSetting (crmId, data) {
		let enitites = ['COMPANY', 'DEAL', 'LEAD', 'CONTACT'];
		let translate = {company:'Компания', deal:'Сделка', lead:'Лид', contact:'Контакт'}
		let entityTypes = enitites
		.filter(value => data.type_setting[value] == 'Y' || data.type_setting[value] == 'VISIBLE')
		.map(value => value.toLowerCase());
		let currentType = entityTypes[1];
		
		let promise = new Promise((rs) => {
			let crmField
			setTimeout(() => {
				crmField = document.querySelector(`#${crmId}`);
				rs(crmField);
			}, 500);
		});
		promise.then(data => {
			data.addEventListener('click', function (trigger) {
				let findWndw = document.createElement('div');
				findWndw.classList.add('finder');

				let titleWndw = document.createElement('p');
				titleWndw.innerText = 'Поиск данных CRM';
				titleWndw.classList.add('mt-4');
				titleWndw.classList.add('text-center');
				titleWndw.classList.add('h5');
				findWndw.append(titleWndw);

				let typesLinksContainer = document.createElement('div');
				typesLinksContainer.classList.add('mt-1');
				typesLinksContainer.classList.add('mb-1');
				typesLinksContainer.style['display'] = 'flex';
				typesLinksContainer.style['justify-content'] = 'space-around';
				typesLinksContainer.style['width'] = '90%';
				typesLinksContainer.style['margin'] = '0 auto';

				for (let i = 0; i < entityTypes.length; i++) {
					let link = document.createElement('span');
					link.classList.add('badge');
					link.classList.add('badge-light');
					link.innerText = translate[entityTypes[i]];
					link.dataset.type = entityTypes[i];
					link.addEventListener('click', function (e) {
						[...document.querySelectorAll('.badge')].forEach(value => value.classList.remove('badge-active'));
						e.target.classList.add('badge-active');
						currentType = link.dataset.type;
					});
					typesLinksContainer.append(link);
				}
				findWndw.append(typesLinksContainer);

				let titleField = document.createElement('input');
				titleField.classList.add('form-control');
				titleField.placeholder = 'по имени';
				titleField.style['width'] = '90%';
				titleField.style['display'] = 'block';
				titleField.style['margin'] = '0 auto';
				titleField.addEventListener('input', e => {
					getBXValueByTitle(currentType, e.target.value, trigger.target);
				});
				findWndw.append(titleField);

				let surname = document.createElement('input');
				surname.classList.add('form-control');
				surname.classList.add('mt-2');
				surname.placeholder = 'по фамилии';
				surname.style['width'] = '90%';
				surname.style['display'] = 'block';
				surname.style['margin'] = '0 auto';
				surname.addEventListener('input', e => {
					getBXValueBySurname(currentType, e.target.value, trigger.target);
				});
				findWndw.append(surname);

				let finderField = document.createElement('input');
				finderField.classList.add('form-control');
				finderField.classList.add('mt-2');
				finderField.placeholder = 'по телефону';
				finderField.style['width'] = '90%';
				finderField.style['display'] = 'block';
				finderField.style['margin'] = '0 auto';
				finderField.addEventListener('input', e => {
					getBXValue(currentType, e.target.value, trigger.target);
				});
				findWndw.append(finderField);

				let close = document.createElement('span');
				close.classList.add('finderClose');
				close.addEventListener('click', e => e.target.parentNode.remove());
				findWndw.append(close);

				let valueContainer = document.createElement('div');
				valueContainer.classList.add('border-top');
				valueContainer.classList.add('mt-3');
				valueContainer.classList.add('valueList');
				findWndw.append(valueContainer);

				document.body.append(findWndw);
			});
		});
	}

	function getBXValue (types, filterValue, trigger) {
		let filter = types == 'contact' ? '?' + PHONE_FINDER : 'PHONE';
		let value = filterValue + '%';
		let method = 'crm.' + types + '.list';
		BX24.callMethod(method, {filter:{[filter]:value}, select:['*','PHONE']}, function (res) {
			if (res.data) showAnswer(res.answer, trigger, types);
		});
	}
	function getBXValueByTitle (types, filterValue, trigger) {
		let filter = types == 'contact' ? '?NAME' : '?TITLE';
		let value = '%' + filterValue + '%';
		let method = 'crm.' + types + '.list';
		BX24.callMethod(method, {'filter':{[filter]:value}, select:['*','PHONE']}, function (res) {
			if (res.data) showAnswer(res.answer, trigger, types);
		});
	}
	function getBXValueBySurname (types, filterValue, trigger) {
		let filter = types == 'contact' ? '?LAST_NAME' : '?TITLE';
		let value = '%' + filterValue + '%';
		let method = 'crm.' + types + '.list';
		BX24.callMethod(method, {'filter':{[filter]:value}, select:['*','PHONE']}, function (res) {
			if (res.data) showAnswer(res.answer, trigger, types);
		});
	}

	function showAnswer (result, trigger, types) {
		let display = document.querySelector('.valueList');
		let type = {company:'CO', contact:'C', lead:'L', deal:'D'};
		display.innerHTML = '';
		if (result.result.length > 0) {
			for (let i = 0; i < result.result.length; i++) {
				let element = document.createElement('div');
				element.classList.add('border-bottom');
				element.classList.add('finder-element');
				element.addEventListener('click', e => {
					inputValue(e, trigger);
				});
				let title = document.createElement('p');
				title.classList.add('ml-3');
				title.classList.add('pt-2');

				if (types == 'contact') {
					let name = result.result[i].NAME || '';
					let lastName = result.result[i].LAST_NAME || '';
					let phone = result.result[i].PHONE ? result.result[i].PHONE[0]['VALUE'] : 'не указан';
					title.innerText = name + ' ' + lastName + ' тел.' + phone;
					title.dataset.value = result.result[i].NAME + '|' + type[types] + '_' + result.result[i].ID;

				} else {
					title.innerText = result.result[i].TITLE;
					title.dataset.value = result.result[i].TITLE + '|' + type[types] + '_' + result.result[i].ID;
				}

				element.append(title);
				display.append(element)
			}

		} else {
			let btnAddEntity = document.createElement('div');
			btnAddEntity.classList.add('addEntity');
			btnAddEntity.innerHTML = '<p class="text-center">Создать</p>';
			btnAddEntity.addEventListener('click', function () {addEntity(this, trigger)});
			display.append(btnAddEntity);
		}
	}

	function inputValue (e, trigger) {
		trigger.value = e.target.dataset.value;
		e.target.parentNode.parentNode.parentNode.remove();
	}

	function addEntity (e, trigger) {
		let badge = document.querySelector('.badge-active');
		let type = badge ? badge.dataset.type : 'company';

		let display = e.parentElement.parentElement;
		[...display.children]
		.forEach(value=> {
			if (!['P', 'SPAN'].includes(value.tagName)) value.remove();
			if (['P'].includes(value.tagName)) value.innerText = 'Создание нового элемента';
		});

		let form = document.createElement('form');
		form.classList.add('mt-3');
		form.classList.add('p-3');
		if (type == 'contact') {
			form.innerHTML = `
				<div class='form-group'>
					<label for='NAME'>Имя</label>
					<input class='form-control' type='text' name='NAME' id='NAME'>
				</div>

				<div class='form-group'>
					<label for='LAST_NAME'>Фамилия</label>
					<input class='form-control' type='text' name='LAST_NAME' id='LAST_NAME'>
				</div>

				<div class='form-group'>
					<label for='PHONE'>Телефон</label>
					<input class='form-control' type='text' name='PHONE' id='PHONE'>
				</div>

				<div class='form-group'>
					<label for='EMAIL'>Email</label>
					<input class='form-control' type='email' name='EMAIL' id='EMAIL'>
				</div>
			`;

		} else {
			form.innerHTML = `
				<div class='form-group'>
					<label for='TITLE'>Заглавие</label>
					<input class='form-control' type='text' name='TITLE' id='TITLE'>
				</div>

				<div class='form-group'>
					<label for='PHONE'>Телефон</label>
					<input class='form-control' type='text' name='PHONE' id='PHONE'>
				</div>

				<div class='form-group'>
					<label for='EMAIL'>Email</label>
					<input class='form-control' type='email' name='EMAIL' id='EMAIL'>
				</div>
			`;
		}

		let btn = document.createElement('button');
		btn.classList.add('btn');
		btn.classList.add('btn-success');
		btn.classList.add('btn-block');
		btn.style['width'] = '92%';
		btn.style['margin'] = '0 auto';
		btn.innerText = 'Создать';
		btn.addEventListener('click', async function (e) {
			let formData = new FormData(form);
			formData.append('ASSIGNED_BY_ID', CURRENT_USER['user']['ID']);
			formData.append('type', type);
			let url = PATH + '/apps/list_product/add_entity.php';
			let response = await fetch(url, {
				method: 'POST',
				body: formData
			});

			if (response.ok) {
				let json = await response.json();
				trigger.value = json;
				console.log('Answer server: Ok');
				display.remove();
			}
		});

		display.append(form);
		display.append(btn);
	}
}