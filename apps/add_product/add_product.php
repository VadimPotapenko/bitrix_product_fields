<?php
/**
* Добавление товара
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
define ('DELIMETR', ';'); // делитель для ножественных полей
$fields = array();
$sectionValue = array();
#====================== settings ==========================#
if (isset($_REQUEST)) {
	foreach ($_POST as $key => $value) {
		if ($appsConfig['PRODUCT_FIELDS'][$key]['multi'] == 'Y') {
			$fields[$key] = explode(',', current($value));
			continue;
		}
		### crm ###
		if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'crm') {
			$fields[$key] = explode('|', $value)[1];
			continue;
		}

		### ответственный ###
		if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'employee') {
			$fields[$key] = array('value' => $value);
			continue;
		}

		### ндс ###
		if ($key == 'VAT_ID') {
			$fields[$key] = !empty($value) ? '3' : '1';
			continue;
		}

		### множественным полям ###
		if (is_array($value)) {
			$fields[$key] = explode(DELIMETR, current($value));
			continue;
		}

		### раздел ###
		if ($key == 'SECTION_ID') {
			$sectionValue = explode('|', $value);
			$fields[$key] = $sectionValue[0];
			continue;
		}

		$fields[$key] = $value;
	}
	if (!empty($_FILES)) {
		foreach ($_FILES as $key => $file) {
			if (is_array($file['name'])) {
				for ($i = 0; $i < count($file['name']); $i++) {
					move_uploaded_file($file['tmp_name'][$i], __DIR__.'/files/'.$file['name'][$i]);
					$fields[$key][] = array('fileData' => array($file['name'][$i], base64_encode(file_get_contents(__DIR__.'/files/'.$file['name'][$i]))));
					$path[] = __DIR__.'/files/'.$file['name'][$i];
				}

			} else {
				move_uploaded_file($file['tmp_name'], __DIR__.'/files/'.$file['name']);
				$fields[$key] = array('fileData' => array($file['name'], base64_encode(file_get_contents(__DIR__.'/files/'.$file['name']))));
				$path[] = __DIR__.'/files/'.$file['name'];
			}
		}
	}

	$fields['NAME'] = $sectionValue[1].' '.$_POST['PROPERTY_268'];
	$add = CRestPlus::call('crm.product.add', array('fields' => $fields));
	foreach ($path as $value) unlink($value);
	echo json_encode($add['result']);
}