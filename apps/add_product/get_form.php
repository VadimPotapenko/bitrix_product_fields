<?php
/**
* Получение формы из хранилища
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#============================ setting =============================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);
$item = CRestPlus::call('entity.item.get', array('ENTITY' => ENTITY_NAME, 'FILTER' => array('NAME' => $json_arr)));
echo json_encode(array('form' => $item));