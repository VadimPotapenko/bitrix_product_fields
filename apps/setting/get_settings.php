<?php
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#========================================= settings =================================#
$settings = CRestPlus::call('entity.item.get', array('ENTITY' => SETTINGS_ENTITY));
foreach (current($settings['result'])['PROPERTY_VALUES'] as $key => $value)
	$returnArr[$key] = json_decode($value, 1);

echo json_encode($returnArr);