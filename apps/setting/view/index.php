<div class='container-fluid'>
	<div class='row'>
		<div class='col topborder'>
			<p class='text-center h3 mt-2 text-dark'>Административный раздел</p>
		</div>
	</div>
	<div class='row'>
		<div class='col'>
			<div class='central s-central'>
				<div class='container'>

					<div class='row'>
						<div class='col border-bottom mt-3'>
							<div class='row mt-2 mb-3'>
								<p class='col h5'>Установить администратора приложения:</p>
								<select class='responsible form-control col' multiple>
									<option value=''>Оставить текущего пользователя</option>
									<?php foreach($appsConfig['USERS'] as $key => $value): ?>
										<option value='<?=$key;?>'><?=$value;?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>

					<div class='row'>
						<div class='col mt-3'>
							<p class='text-center h5 mt-2'>Настройка прав доступа:</p>
							<span class='open'></span>
							<div class='border close-dipslay'>
								<div class='access-close mt-2 mb-2'>
									<span class='go-rules'></span>
									<table class="table">
  										<thead>
    										<tr>
      											<th scope="col">Пользователь</th>
      											<th scope="col"><span class="asc-editor asc-pic"></span></th>
      											<th scope="col"><span class="asc-add-product asc-pic"></span></th>
      											<th scope="col"><span class="asc-table asc-pic"></span></th>
    										</tr>
  										</thead>
  										<tbody>
  											<?php foreach($appsConfig['USERS'] as $key => $value): ?>
	   											<tr class='rules'>
	      											<th scope="row" data-id='<?=$key;?>'><?=$value;?></th>
	      											<td><span class='asc-block'></span></td>
	      											<td><span class='asc-block'></span></td>
	      											<td><span class='asc-block'></span></td>
	    										</tr>
	    									<?php endforeach; ?>
 										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class='row'>
						<div class='col border-bottom mt-3'>
							<p class='text-center h5 mt-2'>Поля фильтра:</p>
							<div class='filter-fields mb-3'>
								<div class='filter-list'></div>
								<div class='filter-add admin-component-add'><span>+</span></div>
							</div>
						</div>

						<div class='col border-bottom mt-3'>
							<p class='text-center h5 mt-2'>Поля таблицы:</p>
							<div class='table-fields mb-3'>
								<div class='field-list'></div>
								<div class='table-add admin-component-add'><span>+</span></div>
							</div>
						</div>
					</div>

					<div class='row'>
						<div class='col mt-3 mb-4'>
							<button class='btn btn-info btn-block set-setting'>Установить настройки приложения</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>