// скрипты клиентской части модуля админпанель
function setting () {
	const REVERSE_TYPE = {
		integer:'number',
		string:'string',
		date:'date',
		datetime:'date',
		product_file:'file',
		list:'list',
		employee:'employee',
		crm:'crm',
		double:'number'
	};
	// плюс поле фильтра
	document.querySelector('.filter-add').addEventListener('click', createFieldForFilter);
	// плюс поле таблицы
	document.querySelector('.table-add').addEventListener('click',  createFieldForTable);
	loadsettings();

	function createFieldForFilter (value = '') {
		let block = document.createElement('div');
		block.classList.add('new-field');
		block.classList.add('new-filter-field');
		block.classList.add('row');

		let close = document.createElement('span');
		close.classList.add('close-field');
		close.addEventListener('click', e => e.target.parentNode.remove());

		let select = document.createElement('select');
		select.classList.add('form-control');
		select.classList.add('col');
		select.addEventListener('change', defaultType);

		
		for (let i in FIELDS) {
			if (['text', 'product_file', 'char'].includes(FIELDS[i].type)) continue;
			if (['CODE', 'XML_ID', 'SORT'].includes(i)) continue;
			let option = document.createElement('option');
			if (value && value.field == i) option.selected = true;
			option.value = i;
			option.innerText = FIELDS[i].name;
			select.append(option);
		}

		let selectType = document.createElement('select');
		selectType.classList.add('form-control');
		selectType.classList.add('col');

		let defaultOption = document.createElement('option');
		defaultOption.classList.add('default-option');
		defaultOption.innerText = 'Не установлено';
		selectType.innerHTML = '';
		selectType.append(defaultOption);
		selectType.innerHTML += `
			<option ${(value && value.type == 'section')   ? 'selected' : ''} value='section'>Раздел товаров</option>
			<option ${(value && value.type == 'lists')     ? 'selected' : ''} value='lists'>Список</option>
			<option ${(value && value.type == 'range')     ? 'selected' : ''} value='range'>Числовой диапозон</option>
			<option ${(value && value.type == 'daterange') ? 'selected' : ''} value='daterange'>Диапозон дат</option>
			<option ${(value && value.type == 'string')    ? 'selected' : ''} value='string'>Строка</option>
			<option ${(value && value.type == 'text')      ? 'selected' : ''} value='text'>Текст</option>
			<option ${(value && value.type == 'number')    ? 'selected' : ''} value='number'>Число</option>
			<option ${(value && value.type == 'crm')       ? 'selected' : ''} value='crm'>CRM</option>
			<option ${(value && value.type == 'employee')  ? 'selected' : ''} value='employee'>Сотрудник</option>
		`;

		block.append(close);
		block.append(select);
		block.append(selectType);
		document.querySelector('.filter-list').append(block);
	}

	function defaultType (e) {
		const TYPE_NAME = {
			number: 'число',
			string: 'строка',
			date: 'дата',
			datetime: 'дата',
			file:'файл',
			list:'список',
			employee:'сотрудник',
			crm:'CRM-элемент'
		}

		let defaultOption = e.target.nextSibling.children[0];
		if (['VAT_ID', 'CURRENCY_ID'].includes(e.target.value)) {
			defaultOption.innerText = 'Дефолтный тип: список';
			defaultOption.selected = true;
			defaultOption.value = 'list';

		} else if (['SECTION_ID'].includes(e.target.value)) {
			defaultOption.innerText = 'Дефолтный тип: раздел товаров';
			defaultOption.selected = true;
			defaultOption.value = 'section';

		} else if (['MODIFIED_BY', 'CREATED_BY'].includes(e.target.value)) {
			defaultOption.innerText = 'Дефолтный тип: сотрудник';
			defaultOption.selected = true;
			defaultOption.value = 'employee';

		} else if (['DESCRIPTION'].includes(e.target.value)) {
			defaultOption.innerText = 'Дефолтный тип: текст';
			defaultOption.selected = true;
			defaultOption.value = 'text';

		} else {
			defaultOption.innerText = 'Дефолтный тип: ' + TYPE_NAME[REVERSE_TYPE[FIELDS[e.target.value].type]];
			defaultOption.selected = true;
			defaultOption.value = REVERSE_TYPE[FIELDS[e.target.value].type];
		} 
	}

	function createFieldForTable (value = '') {
		let block = document.createElement('div');
		block.classList.add('new-field');
		block.classList.add('new-table-field');

		let close = document.createElement('span');
		close.classList.add('close-field');
		close.addEventListener('click', e => e.target.parentNode.remove());

		let select = document.createElement('select');
		select.classList.add('form-control');
		select.classList.add('col');
		for (let i in FIELDS) {
			if (['CODE', 'XML_ID', 'SORT'].includes(i)) continue;
			let option = document.createElement('option');
			if (value == i) option.selected = true;
			option.value = i;
			option.innerText = FIELDS[i].name;
			select.append(option);
		}

		block.append(close);
		block.append(select);
		document.querySelector('.field-list').append(block);
	}

	// права доступа
	document.querySelector('.open').addEventListener('click', function (e) {
		let display = e.target.nextElementSibling;
		display.classList.toggle('open-display');
		display.children[0].classList.toggle('access-open');
	});

	document.querySelector('.go-rules').addEventListener('click', function (e) {
		if (e.target.dataset.go == 'true') {
			e.target.classList.remove('go-choose');
			e.target.dataset.go = 'false';
		} else {
			e.target.classList.add('go-choose');
			e.target.dataset.go = 'true';
		}
	});

	document.querySelectorAll('.asc-block').forEach(value => {
		value.addEventListener('click', function (e) {
			if ([...e.target.classList].includes('asc-block-choose')) {
				e.target.classList.remove('asc-block-choose');
				e.target.dataset.set = false;
			} else {
				e.target.classList.add('asc-block-choose');
				e.target.dataset.set = true;
			}
		});
	});

	// установка настроек
	document.querySelector('.set-setting').addEventListener('click', function (e) {
		let settings = {}, rules = [];
		settings.admin = [...document.querySelector('.responsible').children]
		.filter(value => {
			if (value.selected) return value;
		})
		.map(value => value.value);

		let filterFields = [...document.querySelectorAll('.new-filter-field')]
		.map(value => {
			return {field:value.children[1].value, type:value.children[2].value};
		});
		let tableFields = [...document.querySelectorAll('.new-table-field')]
		.map(value => value.children[1].value);

		if (document.querySelector('.go-rules').dataset.go == 'true') {
			rules = [...document.querySelectorAll('.rules')].map(value => {
				return {
					user:value.children[0].dataset.id,
					editor:value.children[1].children[0].dataset.set || 'false', 
					add:value.children[2].children[0].dataset.set || 'false', 
					table:value.children[3].children[0].dataset.set || 'false', 
				}
			});
		}

		settings.filter = filterFields;
		settings.table = tableFields;
		settings.rules = rules || '';
		setSetting(settings);
	});

	// установка настроек приложения
	async function setSetting (data) {
		let url = PATH + '/apps/setting/set_settings.php';
		let response = await fetch(url, {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify(data)
		});

		if (response.ok) {
			let json = await response.json();
			console.log('Answer server: OK');
			window.location.reload();

		} else {
			console.log("Error HTTP: " + response.status);
			window.location.reload();
		}
	}

	// загружаем настройки если есть
	async function loadsettings () {
		let url = PATH + '/apps/setting/get_settings.php';
		let response = await fetch(url);
		if (response.ok) {
			let json = await response.json();
			renderSettings(json);
			console.log('Answer server: OK');

		} else {
			console.log("Error HTTP: " + response.status);
		}
	}

	function renderSettings (data) {
		// админы
		let adminsDisplay = document.querySelector('.responsible');
		[...adminsDisplay].forEach(value => {
			if (data.ADMIN.includes(value.value))
				value.selected = true;
		});

		// таблица доступов
		[...document.querySelectorAll('.rules')]
		.forEach(value => {
			data.ACCESS.forEach(item => {
				if (item.user == value.children[0].dataset.id) {
					if (item.editor) {
						value.children[1].children[0].classList.add('asc-block-choose');
						value.children[1].children[0].dataset.set = 'true';
					}

					if (item.add) {
						value.children[2].children[0].classList.add('asc-block-choose');
						value.children[2].children[0].dataset.set = 'true';
					}

					if (item.table) {
						value.children[3].children[0].classList.add('asc-block-choose');
						value.children[3].children[0].dataset.set = 'true';
					}
				}
			});
		});

		// поля фильтра
		data.FILTER_FIELDS.forEach(value => createFieldForFilter(value));
		// поля таблицы
		data.TABLE_FIELDS.forEach(value => createFieldForTable(value));
	}
}