<?php
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
define ('ITEM', 'SETTING');
define ('PROPERTY', array('admin' => 'ADMIN', 'filter' => 'FILTER_FIELDS', 'table' => 'TABLE_FIELDS', 'rules' => 'ACCESS'));
#====================== settings ==========================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);
foreach ($json_arr as $k => $v)
	if (!empty($v)) $propertyValues[PROPERTY[$k]] = json_encode($v);

if (isset($propertyValues) && !empty($propertyValues)) {
	$item = CRestPlus::call('entity.item.get', array('ENTITY' => SETTINGS_ENTITY, 'FILTER' => array('NAME' => ITEM)));
	if (!isset($item['result']) || empty($item['result'])) {
		CRestPlus::call('entity.item.add', array(
			'ENTITY' => SETTINGS_ENTITY,
			'NAME'   => ITEM,
			'PROPERTY_VALUES' => $propertyValues
		));

	} else {
		CRestPlus::call('entity.item.update', array(
			'ENTITY' => SETTINGS_ENTITY,
			'NAME'   => ITEM,
			'ID'     => $item['result'][0]['ID'],
			'PROPERTY_VALUES' => $propertyValues
		));
	}

	deleteCash();
}
echo json_encode('OK');