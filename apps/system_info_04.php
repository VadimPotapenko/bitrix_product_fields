<?php
$appsConfig = array (
  'USERS' => 
  array (
    1 => 'Васильев Андрей',
    8 => 'Захарова Оксана',
    10 => 'Григорьев Александр',
    14 => 'Меринова Елена',
    16 => 'Веселова Ксения',
    58 => 'Гуриев Никита',
    60 => 'Сморж Роман',
    72 => 'ИНВЕСТ АУРУМ',
    92 => 'Усанов Сергей',
    150 => 'БОТ АИ',
    182 => ' Разработчик',
    256 => 'Курников Кирилл',
  ),
  'LISTS' => 
  array (
    'PROPERTY_250' => 
    array (
      874 => 
      array (
        'ID' => '874',
        'VALUE' => 'Продажа',
        'XML_ID' => 'sale',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      876 => 
      array (
        'ID' => '876',
        'VALUE' => 'Аренда',
        'XML_ID' => 'rent',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_252' => 
    array (
      878 => 
      array (
        'ID' => '878',
        'VALUE' => 'комната',
        'XML_ID' => 'room',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      880 => 
      array (
        'ID' => '880',
        'VALUE' => 'квартира',
        'XML_ID' => 'flat',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      882 => 
      array (
        'ID' => '882',
        'VALUE' => 'таунхаус',
        'XML_ID' => 'townhouse',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      884 => 
      array (
        'ID' => '884',
        'VALUE' => 'дом',
        'XML_ID' => 'house',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      886 => 
      array (
        'ID' => '886',
        'VALUE' => 'часть дома',
        'XML_ID' => 'house_part',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      888 => 
      array (
        'ID' => '888',
        'VALUE' => 'участок',
        'XML_ID' => 'lot',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      890 => 
      array (
        'ID' => '890',
        'VALUE' => 'дом с участком',
        'XML_ID' => 'house_with_lot',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      892 => 
      array (
        'ID' => '892',
        'VALUE' => 'дача / коттедж',
        'XML_ID' => 'cottage',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      894 => 
      array (
        'ID' => '894',
        'VALUE' => 'дача',
        'XML_ID' => 'dacha',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      896 => 
      array (
        'ID' => '896',
        'VALUE' => 'коттедж',
        'XML_ID' => 'kottedzh',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      898 => 
      array (
        'ID' => '898',
        'VALUE' => 'коммерческая',
        'XML_ID' => 'commercial',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      900 => 
      array (
        'ID' => '900',
        'VALUE' => 'гараж',
        'XML_ID' => 'garage',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_254' => 
    array (
      902 => 
      array (
        'ID' => '902',
        'VALUE' => 'автосервис',
        'XML_ID' => 'auto_repair',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      904 => 
      array (
        'ID' => '904',
        'VALUE' => 'готовый бизнес',
        'XML_ID' => 'business',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      906 => 
      array (
        'ID' => '906',
        'VALUE' => 'помещения свободного назначения',
        'XML_ID' => 'free_purpose',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      908 => 
      array (
        'ID' => '908',
        'VALUE' => 'офисные помещения',
        'XML_ID' => 'office',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      910 => 
      array (
        'ID' => '910',
        'VALUE' => 'гостиница',
        'XML_ID' => 'hotel',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      912 => 
      array (
        'ID' => '912',
        'VALUE' => 'земли коммерческого назначения',
        'XML_ID' => 'land',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      914 => 
      array (
        'ID' => '914',
        'VALUE' => 'юридический адрес',
        'XML_ID' => 'legal_address',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      916 => 
      array (
        'ID' => '916',
        'VALUE' => 'производственное помещение',
        'XML_ID' => 'manufacturing',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      918 => 
      array (
        'ID' => '918',
        'VALUE' => 'общепит',
        'XML_ID' => 'public catering',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      920 => 
      array (
        'ID' => '920',
        'VALUE' => 'торговые помещения',
        'XML_ID' => 'retail',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      922 => 
      array (
        'ID' => '922',
        'VALUE' => 'склад',
        'XML_ID' => 'warehouse',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      924 => 
      array (
        'ID' => '924',
        'VALUE' => 'здание',
        'XML_ID' => 'building',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_256' => 
    array (
      926 => 
      array (
        'ID' => '926',
        'VALUE' => 'бизнес-центр',
        'XML_ID' => 'business center',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      928 => 
      array (
        'ID' => '928',
        'VALUE' => 'отдельно стоящее здание',
        'XML_ID' => 'detached building',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      930 => 
      array (
        'ID' => '930',
        'VALUE' => 'встроенное помещение',
        'XML_ID' => 'residential building',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      932 => 
      array (
        'ID' => '932',
        'VALUE' => 'торговый центр',
        'XML_ID' => 'shopping center',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      934 => 
      array (
        'ID' => '934',
        'VALUE' => 'складской комплекс',
        'XML_ID' => 'warehouse',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_258' => 
    array (
      936 => 
      array (
        'ID' => '936',
        'VALUE' => 'первичная продажа',
        'XML_ID' => 'primary',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      938 => 
      array (
        'ID' => '938',
        'VALUE' => 'переуступка',
        'XML_ID' => 'reassignment',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_260' => 
    array (
      940 => 
      array (
        'ID' => '940',
        'VALUE' => 'прямая продажа',
        'XML_ID' => 'sale',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      942 => 
      array (
        'ID' => '942',
        'VALUE' => 'первичная продажа вторички',
        'XML_ID' => 'primary sale of secondary',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      944 => 
      array (
        'ID' => '944',
        'VALUE' => 'встречная продажа',
        'XML_ID' => 'countersale',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_262' => 
    array (
      946 => 
      array (
        'ID' => '946',
        'VALUE' => 'прямая аренда',
        'XML_ID' => 'direct rent',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      948 => 
      array (
        'ID' => '948',
        'VALUE' => 'субаренда',
        'XML_ID' => 'subrent',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      950 => 
      array (
        'ID' => '950',
        'VALUE' => 'продажа права аренды',
        'XML_ID' => 'sale of lease rights',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_264' => 
    array (
      952 => 
      array (
        'ID' => '952',
        'VALUE' => 'Студия',
        'XML_ID' => 'studio',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      954 => 
      array (
        'ID' => '954',
        'VALUE' => '1',
        'XML_ID' => '1',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      956 => 
      array (
        'ID' => '956',
        'VALUE' => '2',
        'XML_ID' => '2',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      958 => 
      array (
        'ID' => '958',
        'VALUE' => '3',
        'XML_ID' => '3',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      960 => 
      array (
        'ID' => '960',
        'VALUE' => '4',
        'XML_ID' => '4',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      962 => 
      array (
        'ID' => '962',
        'VALUE' => '5',
        'XML_ID' => '5',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      964 => 
      array (
        'ID' => '964',
        'VALUE' => '6',
        'XML_ID' => '6',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      966 => 
      array (
        'ID' => '966',
        'VALUE' => '7',
        'XML_ID' => '7',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_276' => 
    array (
      968 => 
      array (
        'ID' => '968',
        'VALUE' => 'кв. м',
        'XML_ID' => 'sq. m',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      970 => 
      array (
        'ID' => '970',
        'VALUE' => 'сотка',
        'XML_ID' => 'sotka',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      972 => 
      array (
        'ID' => '972',
        'VALUE' => 'гектар',
        'XML_ID' => 'hectare',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_278' => 
    array (
      974 => 
      array (
        'ID' => '974',
        'VALUE' => 'день',
        'XML_ID' => 'day',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      976 => 
      array (
        'ID' => '976',
        'VALUE' => 'месяц',
        'XML_ID' => 'month',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_284' => 
    array (
      978 => 
      array (
        'ID' => '978',
        'VALUE' => 'Залог',
        'XML_ID' => 'rent-pledge',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      980 => 
      array (
        'ID' => '980',
        'VALUE' => 'Клининг включен в стоимость',
        'XML_ID' => 'cleaning-included',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      982 => 
      array (
        'ID' => '982',
        'VALUE' => 'Коммунальные услуги включены в стоимость',
        'XML_ID' => 'utilities-included',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      984 => 
      array (
        'ID' => '984',
        'VALUE' => 'Электроэнергия включена в стоимость',
        'XML_ID' => 'electricity-included',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_292' => 
    array (
      986 => 
      array (
        'ID' => '986',
        'VALUE' => 'гараж',
        'XML_ID' => 'garage',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      988 => 
      array (
        'ID' => '988',
        'VALUE' => 'машиноместо',
        'XML_ID' => 'parking place',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      990 => 
      array (
        'ID' => '990',
        'VALUE' => 'бокс',
        'XML_ID' => 'box',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_296' => 
    array (
      992 => 
      array (
        'ID' => '992',
        'VALUE' => 'НДС не включен',
        'XML_ID' => 'notincluded',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      994 => 
      array (
        'ID' => '994',
        'VALUE' => 'НДС',
        'XML_ID' => 'nds',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      996 => 
      array (
        'ID' => '996',
        'VALUE' => 'УСН',
        'XML_ID' => 'usn',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_300' => 
    array (
      998 => 
      array (
        'ID' => '998',
        'VALUE' => 'Торг',
        'XML_ID' => 'haggle',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1000 => 
      array (
        'ID' => '1000',
        'VALUE' => 'Ипотека',
        'XML_ID' => 'mortgage',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1002 => 
      array (
        'ID' => '1002',
        'VALUE' => 'Пометка «Просьба агентам не звонить»',
        'XML_ID' => 'not-for-agents',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1004 => 
      array (
        'ID' => '1004',
        'VALUE' => 'Коммунальные услуги включены в стоимость (только для аренды)',
        'XML_ID' => 'utilities-included',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1006 => 
      array (
        'ID' => '1006',
        'VALUE' => 'Залог (только для аренды)',
        'XML_ID' => 'rent-pledge',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_318' => 
    array (
      1008 => 
      array (
        'ID' => '1008',
        'VALUE' => 'собственность',
        'XML_ID' => 'private',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1010 => 
      array (
        'ID' => '1010',
        'VALUE' => 'по доверенности',
        'XML_ID' => 'by proxy',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1012 => 
      array (
        'ID' => '1012',
        'VALUE' => 'кооператив',
        'XML_ID' => 'cooperative',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_326' => 
    array (
      1014 => 
      array (
        'ID' => '1014',
        'VALUE' => 'Дизайнерский',
        'XML_ID' => 'designer',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1016 => 
      array (
        'ID' => '1016',
        'VALUE' => 'Евроремонт',
        'XML_ID' => 'euro',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1018 => 
      array (
        'ID' => '1018',
        'VALUE' => 'Хороший',
        'XML_ID' => 'good',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1020 => 
      array (
        'ID' => '1020',
        'VALUE' => 'Косметический',
        'XML_ID' => 'redecorating',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1022 => 
      array (
        'ID' => '1022',
        'VALUE' => 'Обычный',
        'XML_ID' => 'common',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1024 => 
      array (
        'ID' => '1024',
        'VALUE' => 'Требуется косметический',
        'XML_ID' => 'redecorating_needed',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1026 => 
      array (
        'ID' => '1026',
        'VALUE' => 'Требуется ремонт',
        'XML_ID' => 'renovation_needed',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1326 => 
      array (
        'ID' => '1326',
        'VALUE' => 'Предчистовая отделка',
        'XML_ID' => '837d1f26a710dc3d4f1106cec7d051e1',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1328 => 
      array (
        'ID' => '1328',
        'VALUE' => 'Черновая отделка',
        'XML_ID' => '4a339c5f8c0a216d9780a557fe17059f',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1330 => 
      array (
        'ID' => '1330',
        'VALUE' => 'Чистовая отделка',
        'XML_ID' => 'fbd62f02c97380a783a4b6c65d3dcd40',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1332 => 
      array (
        'ID' => '1332',
        'VALUE' => 'Без отделки',
        'XML_ID' => '8470ee0a819f28c34e802a88330ddd21',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1376 => 
      array (
        'ID' => '1376',
        'VALUE' => 'Офисная отделка',
        'XML_ID' => '372b948d9250df4def30bd854e1308ed',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_332' => 
    array (
      1028 => 
      array (
        'ID' => '1028',
        'VALUE' => 'Поселения (ИЖС)',
        'XML_ID' => 'izhs',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1030 => 
      array (
        'ID' => '1030',
        'VALUE' => 'Сельхозназначения (СНТ, ДНП)',
        'XML_ID' => 'snt_dnp',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1032 => 
      array (
        'ID' => '1032',
        'VALUE' => 'Промназначения',
        'XML_ID' => 'prom',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_334' => 
    array (
      1034 => 
      array (
        'ID' => '1034',
        'VALUE' => 'блочный',
        'XML_ID' => 'block',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1036 => 
      array (
        'ID' => '1036',
        'VALUE' => 'деревянный',
        'XML_ID' => 'wooden',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1038 => 
      array (
        'ID' => '1038',
        'VALUE' => 'кирпичный',
        'XML_ID' => 'brick',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1040 => 
      array (
        'ID' => '1040',
        'VALUE' => 'кирпично-монолитный',
        'XML_ID' => 'brick_monolithic',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1042 => 
      array (
        'ID' => '1042',
        'VALUE' => 'монолит',
        'XML_ID' => 'monolithic',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1044 => 
      array (
        'ID' => '1044',
        'VALUE' => 'панельный',
        'XML_ID' => 'panel',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_336' => 
    array (
      1046 => 
      array (
        'ID' => '1046',
        'VALUE' => 'кирпич',
        'XML_ID' => 'brick',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1048 => 
      array (
        'ID' => '1048',
        'VALUE' => 'брус',
        'XML_ID' => 'bar',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1050 => 
      array (
        'ID' => '1050',
        'VALUE' => 'бревно',
        'XML_ID' => 'log',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1052 => 
      array (
        'ID' => '1052',
        'VALUE' => 'газоблоки',
        'XML_ID' => 'aerocrete',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1054 => 
      array (
        'ID' => '1054',
        'VALUE' => 'металл',
        'XML_ID' => 'metal',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1056 => 
      array (
        'ID' => '1056',
        'VALUE' => 'пеноблоки',
        'XML_ID' => 'foamblocks',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1058 => 
      array (
        'ID' => '1058',
        'VALUE' => 'сэндвич-панели',
        'XML_ID' => 'sandwich_panels',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1060 => 
      array (
        'ID' => '1060',
        'VALUE' => 'ж/б панели',
        'XML_ID' => 'concrete_panels',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1062 => 
      array (
        'ID' => '1062',
        'VALUE' => 'экспериментальные материалы',
        'XML_ID' => 'experimental',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1334 => 
      array (
        'ID' => '1334',
        'VALUE' => 'монолитно-кирпичный',
        'XML_ID' => '669c3a5fb43f15e1ef2fc873e70796a3',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1336 => 
      array (
        'ID' => '1336',
        'VALUE' => 'каркасно-щитовой',
        'XML_ID' => '257a34dd11c7d17171958f767c58310c',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1338 => 
      array (
        'ID' => '1338',
        'VALUE' => 'блочный',
        'XML_ID' => '39ef079ef5f8780295aef91db3cb35c8',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1340 => 
      array (
        'ID' => '1340',
        'VALUE' => 'монолитный',
        'XML_ID' => '98534df142312bf16b7d75f4d8e45d28',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1342 => 
      array (
        'ID' => '1342',
        'VALUE' => 'панельный',
        'XML_ID' => '93369f0de68730adb64ba966c2a811cb',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1344 => 
      array (
        'ID' => '1344',
        'VALUE' => 'железобетон',
        'XML_ID' => '243943a411679d558e20c6d9339f2dc3',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1346 => 
      array (
        'ID' => '1346',
        'VALUE' => 'деревянный',
        'XML_ID' => '43c9bc9f175fd1eb41933d72f9c209c6',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_350' => 
    array (
      1064 => 
      array (
        'ID' => '1064',
        'VALUE' => 'Общий',
        'XML_ID' => 'common',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1066 => 
      array (
        'ID' => '1066',
        'VALUE' => 'Отдельный',
        'XML_ID' => 'separate',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_352' => 
    array (
      1068 => 
      array (
        'ID' => '1068',
        'VALUE' => 'подземная',
        'XML_ID' => 'underground',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1070 => 
      array (
        'ID' => '1070',
        'VALUE' => 'наземная',
        'XML_ID' => 'ground',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1072 => 
      array (
        'ID' => '1072',
        'VALUE' => 'многоуровневая',
        'XML_ID' => 'multilevel',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1378 => 
      array (
        'ID' => '1378',
        'VALUE' => 'открытая',
        'XML_ID' => '864ea9d4193422da852f0e4d40cdfe7d',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_354' => 
    array (
      1074 => 
      array (
        'ID' => '1074',
        'VALUE' => 'кирпичный',
        'XML_ID' => 'brick',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1076 => 
      array (
        'ID' => '1076',
        'VALUE' => 'железобетонный',
        'XML_ID' => 'ferroconcrete',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1078 => 
      array (
        'ID' => '1078',
        'VALUE' => 'металлический',
        'XML_ID' => 'metal',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_356' => 
    array (
      1080 => 
      array (
        'ID' => '1080',
        'VALUE' => 'Отопление',
        'XML_ID' => 'heating',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1082 => 
      array (
        'ID' => '1082',
        'VALUE' => 'Электроснабжение',
        'XML_ID' => 'electricity',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1084 => 
      array (
        'ID' => '1084',
        'VALUE' => 'Водопровод',
        'XML_ID' => 'water',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1086 => 
      array (
        'ID' => '1086',
        'VALUE' => 'Охрана',
        'XML_ID' => 'security',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1088 => 
      array (
        'ID' => '1088',
        'VALUE' => 'Автоматические ворота',
        'XML_ID' => 'automatic-gates',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1090 => 
      array (
        'ID' => '1090',
        'VALUE' => 'Видеонаблюдение',
        'XML_ID' => 'cctv',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1092 => 
      array (
        'ID' => '1092',
        'VALUE' => 'Пожарная сигнализация',
        'XML_ID' => 'fire-alarm',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1094 => 
      array (
        'ID' => '1094',
        'VALUE' => 'Пропускная система',
        'XML_ID' => 'access-control-system',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1096 => 
      array (
        'ID' => '1096',
        'VALUE' => 'Смотровая яма',
        'XML_ID' => 'inspection-pit',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1098 => 
      array (
        'ID' => '1098',
        'VALUE' => 'Подвал или погреб',
        'XML_ID' => 'cellar',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1100 => 
      array (
        'ID' => '1100',
        'VALUE' => 'Автомойка',
        'XML_ID' => 'car-wash',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1102 => 
      array (
        'ID' => '1102',
        'VALUE' => 'Автосервис',
        'XML_ID' => 'auto-repair',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1104 => 
      array (
        'ID' => '1104',
        'VALUE' => 'Гараж в новостройке',
        'XML_ID' => 'new-parking',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_360' => 
    array (
      1106 => 
      array (
        'ID' => '1106',
        'VALUE' => 'Апартаменты',
        'XML_ID' => 'apartments',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1108 => 
      array (
        'ID' => '1108',
        'VALUE' => 'Свободная планировка',
        'XML_ID' => 'open-plan',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1110 => 
      array (
        'ID' => '1110',
        'VALUE' => 'Кондиционер',
        'XML_ID' => 'air-conditioner',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1112 => 
      array (
        'ID' => '1112',
        'VALUE' => 'Телефон',
        'XML_ID' => 'phone',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1114 => 
      array (
        'ID' => '1114',
        'VALUE' => 'Интернет',
        'XML_ID' => 'internet',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1116 => 
      array (
        'ID' => '1116',
        'VALUE' => 'Мебель',
        'XML_ID' => 'room-furniture',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1118 => 
      array (
        'ID' => '1118',
        'VALUE' => 'Мебель на кухне',
        'XML_ID' => 'kitchen-furniture',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1120 => 
      array (
        'ID' => '1120',
        'VALUE' => 'Телевизор',
        'XML_ID' => 'television',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1122 => 
      array (
        'ID' => '1122',
        'VALUE' => 'Стиральная машина',
        'XML_ID' => 'washing-machine',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1124 => 
      array (
        'ID' => '1124',
        'VALUE' => 'Посудомоечная машина',
        'XML_ID' => 'dishwasher',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1126 => 
      array (
        'ID' => '1126',
        'VALUE' => 'Холодильник',
        'XML_ID' => 'refrigerator',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1128 => 
      array (
        'ID' => '1128',
        'VALUE' => 'Встроенная техника',
        'XML_ID' => 'built-in-tech',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_362' => 
    array (
      1130 => 
      array (
        'ID' => '1130',
        'VALUE' => 'Закрытая территория',
        'XML_ID' => 'guarded-building',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1132 => 
      array (
        'ID' => '1132',
        'VALUE' => 'Пропускная система',
        'XML_ID' => 'access-control-system',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1134 => 
      array (
        'ID' => '1134',
        'VALUE' => 'Лифт',
        'XML_ID' => 'lift',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1136 => 
      array (
        'ID' => '1136',
        'VALUE' => 'Мусоропровод',
        'XML_ID' => 'rubbish-chute',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1138 => 
      array (
        'ID' => '1138',
        'VALUE' => 'Наличие охраняемой парковки',
        'XML_ID' => 'parking',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1140 => 
      array (
        'ID' => '1140',
        'VALUE' => 'Парковка для гостей',
        'XML_ID' => 'parking-guest',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1142 => 
      array (
        'ID' => '1142',
        'VALUE' => 'Сигнализация в доме',
        'XML_ID' => 'alarm',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1144 => 
      array (
        'ID' => '1144',
        'VALUE' => 'Сигнализация в квартире',
        'XML_ID' => 'flat-alarm',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1146 => 
      array (
        'ID' => '1146',
        'VALUE' => 'Наличие охраны',
        'XML_ID' => 'security',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1148 => 
      array (
        'ID' => '1148',
        'VALUE' => 'Элитная недвижимость',
        'XML_ID' => 'is-elite',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_364' => 
    array (
      1150 => 
      array (
        'ID' => '1150',
        'VALUE' => 'Электричество',
        'XML_ID' => 'electricity-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1152 => 
      array (
        'ID' => '1152',
        'VALUE' => 'Водопровод',
        'XML_ID' => 'water-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1154 => 
      array (
        'ID' => '1154',
        'VALUE' => 'Газ',
        'XML_ID' => 'gas-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1156 => 
      array (
        'ID' => '1156',
        'VALUE' => 'Канализация',
        'XML_ID' => 'sewerage-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1158 => 
      array (
        'ID' => '1158',
        'VALUE' => 'Отопление',
        'XML_ID' => 'heating-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1160 => 
      array (
        'ID' => '1160',
        'VALUE' => 'Туалет',
        'XML_ID' => 'toilet',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1162 => 
      array (
        'ID' => '1162',
        'VALUE' => 'Душ',
        'XML_ID' => 'shower',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1164 => 
      array (
        'ID' => '1164',
        'VALUE' => 'Бассейн',
        'XML_ID' => 'pool',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1166 => 
      array (
        'ID' => '1166',
        'VALUE' => 'Бильярд',
        'XML_ID' => 'billiard',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1168 => 
      array (
        'ID' => '1168',
        'VALUE' => 'Сауна',
        'XML_ID' => 'sauna',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1170 => 
      array (
        'ID' => '1170',
        'VALUE' => 'Возможность ПМЖ',
        'XML_ID' => 'pmg',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_366' => 
    array (
      1172 => 
      array (
        'ID' => '1172',
        'VALUE' => 'ковролин',
        'XML_ID' => 'carpet',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1174 => 
      array (
        'ID' => '1174',
        'VALUE' => 'ламинат',
        'XML_ID' => 'laminate',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1176 => 
      array (
        'ID' => '1176',
        'VALUE' => 'линолеум',
        'XML_ID' => 'linoleum',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1178 => 
      array (
        'ID' => '1178',
        'VALUE' => 'паркет',
        'XML_ID' => 'parquet',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1180 => 
      array (
        'ID' => '1180',
        'VALUE' => 'кафель',
        'XML_ID' => 'tile',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_368' => 
    array (
      1182 => 
      array (
        'ID' => '1182',
        'VALUE' => 'помещение для банка',
        'XML_ID' => 'bank',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1184 => 
      array (
        'ID' => '1184',
        'VALUE' => 'салон красоты',
        'XML_ID' => 'beauty shop',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1186 => 
      array (
        'ID' => '1186',
        'VALUE' => 'продуктовый магазин',
        'XML_ID' => 'food store',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1188 => 
      array (
        'ID' => '1188',
        'VALUE' => 'медицинский центр',
        'XML_ID' => 'medical center',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1190 => 
      array (
        'ID' => '1190',
        'VALUE' => 'шоу-рум',
        'XML_ID' => 'show room',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1192 => 
      array (
        'ID' => '1192',
        'VALUE' => 'турагентство',
        'XML_ID' => 'touragency',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_370' => 
    array (
      1194 => 
      array (
        'ID' => '1194',
        'VALUE' => 'алкогольный склад',
        'XML_ID' => 'alcohol',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1196 => 
      array (
        'ID' => '1196',
        'VALUE' => 'фармацевтический склад',
        'XML_ID' => 'pharmaceutical storehouse',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1198 => 
      array (
        'ID' => '1198',
        'VALUE' => 'овощехранилище',
        'XML_ID' => 'vegetable storehouse',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_372' => 
    array (
      1200 => 
      array (
        'ID' => '1200',
        'VALUE' => 'A',
        'XML_ID' => 'a',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1202 => 
      array (
        'ID' => '1202',
        'VALUE' => 'A+',
        'XML_ID' => 'a_plus',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1204 => 
      array (
        'ID' => '1204',
        'VALUE' => 'B',
        'XML_ID' => 'b',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1206 => 
      array (
        'ID' => '1206',
        'VALUE' => 'B+',
        'XML_ID' => 'b_plus',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1208 => 
      array (
        'ID' => '1208',
        'VALUE' => 'C',
        'XML_ID' => 'c',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1210 => 
      array (
        'ID' => '1210',
        'VALUE' => 'C+',
        'XML_ID' => 'c_plus',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1212 => 
      array (
        'ID' => '1212',
        'VALUE' => 'D',
        'XML_ID' => 'd',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_374' => 
    array (
      1214 => 
      array (
        'ID' => '1214',
        'VALUE' => 'Закрытая территория',
        'XML_ID' => 'guarded-building',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1216 => 
      array (
        'ID' => '1216',
        'VALUE' => 'Наличие пропускной системы',
        'XML_ID' => 'access-control-system',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1218 => 
      array (
        'ID' => '1218',
        'VALUE' => 'Доступ на объект 24/7',
        'XML_ID' => 'twenty-four-seven',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1220 => 
      array (
        'ID' => '1220',
        'VALUE' => 'Лифт',
        'XML_ID' => 'lift',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1222 => 
      array (
        'ID' => '1222',
        'VALUE' => 'Охраняемая парковка',
        'XML_ID' => 'parking',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1224 => 
      array (
        'ID' => '1224',
        'VALUE' => 'Парковка для гостей',
        'XML_ID' => 'parking-guest',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1226 => 
      array (
        'ID' => '1226',
        'VALUE' => 'Охрана',
        'XML_ID' => 'security',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1228 => 
      array (
        'ID' => '1228',
        'VALUE' => 'Предприятия общепита в здании',
        'XML_ID' => 'eating-facilities',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1230 => 
      array (
        'ID' => '1230',
        'VALUE' => 'Элитная недвижимость',
        'XML_ID' => 'is-elite',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_376' => 
    array (
      1232 => 
      array (
        'ID' => '1232',
        'VALUE' => 'Возможность добавить телефонные линии',
        'XML_ID' => 'adding-phone-on-request',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1234 => 
      array (
        'ID' => '1234',
        'VALUE' => 'Вентиляция',
        'XML_ID' => 'ventilation',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1236 => 
      array (
        'ID' => '1236',
        'VALUE' => 'Канализация',
        'XML_ID' => 'sewerage-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1238 => 
      array (
        'ID' => '1238',
        'VALUE' => 'Возможность выбрать оператора связи',
        'XML_ID' => 'self-selection-telecom',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1240 => 
      array (
        'ID' => '1240',
        'VALUE' => 'Мебель',
        'XML_ID' => 'room-furniture',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1242 => 
      array (
        'ID' => '1242',
        'VALUE' => 'Интернет',
        'XML_ID' => 'internet',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1244 => 
      array (
        'ID' => '1244',
        'VALUE' => 'Отопление',
        'XML_ID' => 'heating-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1246 => 
      array (
        'ID' => '1246',
        'VALUE' => 'Газоснабжение',
        'XML_ID' => 'gas-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1248 => 
      array (
        'ID' => '1248',
        'VALUE' => 'Пожарная сигнализация',
        'XML_ID' => 'fire-alarm',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1250 => 
      array (
        'ID' => '1250',
        'VALUE' => 'Электроснабжение',
        'XML_ID' => 'electricity-supply 	',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1252 => 
      array (
        'ID' => '1252',
        'VALUE' => 'Кондиционер',
        'XML_ID' => 'air-conditioner',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1254 => 
      array (
        'ID' => '1254',
        'VALUE' => 'Водопровод',
        'XML_ID' => 'water-supply',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_388' => 
    array (
      1256 => 
      array (
        'ID' => '1256',
        'VALUE' => 'Package',
        'XML_ID' => 'Package',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1258 => 
      array (
        'ID' => '1258',
        'VALUE' => 'PackageSingle',
        'XML_ID' => 'PackageSingle',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1260 => 
      array (
        'ID' => '1260',
        'VALUE' => 'Single',
        'XML_ID' => 'Single',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_390' => 
    array (
      1262 => 
      array (
        'ID' => '1262',
        'VALUE' => 'Y',
        'XML_ID' => '0f716d5ac4cb58481dcb463ca38a6880',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1264 => 
      array (
        'ID' => '1264',
        'VALUE' => 'срочно продаю!',
        'XML_ID' => 'f111bca3d4d2c172f6592f73bad78d46',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1266 => 
      array (
        'ID' => '1266',
        'VALUE' => 'срочно сдаю!',
        'XML_ID' => 'bd62d5ac0802eadd1b329e57a439ba4f',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_392' => 
    array (
      1268 => 
      array (
        'ID' => '1268',
        'VALUE' => 'Бесплатное',
        'XML_ID' => 'free',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1270 => 
      array (
        'ID' => '1270',
        'VALUE' => 'Выделение цветом',
        'XML_ID' => 'highlight',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1272 => 
      array (
        'ID' => '1272',
        'VALUE' => 'Платное',
        'XML_ID' => 'paid',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1274 => 
      array (
        'ID' => '1274',
        'VALUE' => 'Премиум-объявление',
        'XML_ID' => 'premium',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1276 => 
      array (
        'ID' => '1276',
        'VALUE' => 'Топ 3',
        'XML_ID' => 'top3',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_394' => 
    array (
      1278 => 
      array (
        'ID' => '1278',
        'VALUE' => 'Выделение цветом',
        'XML_ID' => 'highlight',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1280 => 
      array (
        'ID' => '1280',
        'VALUE' => 'Премиум-объявление',
        'XML_ID' => 'premium',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1282 => 
      array (
        'ID' => '1282',
        'VALUE' => 'Топ 3',
        'XML_ID' => 'top3',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_396' => 
    array (
      1284 => 
      array (
        'ID' => '1284',
        'VALUE' => 'Да',
        'XML_ID' => 'Y',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_398' => 
    array (
      1286 => 
      array (
        'ID' => '1286',
        'VALUE' => 'premium',
        'XML_ID' => 'premium',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1288 => 
      array (
        'ID' => '1288',
        'VALUE' => 'raise',
        'XML_ID' => 'raise',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1290 => 
      array (
        'ID' => '1290',
        'VALUE' => 'promotion',
        'XML_ID' => 'promotion',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_404' => 
    array (
      1292 => 
      array (
        'ID' => '1292',
        'VALUE' => 'Альтернатива',
        'XML_ID' => 'alternative',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1294 => 
      array (
        'ID' => '1294',
        'VALUE' => 'Свободная продажа',
        'XML_ID' => 'free',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1296 => 
      array (
        'ID' => '1296',
        'VALUE' => 'Договор уступки права требования (по умолчанию)',
        'XML_ID' => 'dupt',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1298 => 
      array (
        'ID' => '1298',
        'VALUE' => 'Договор ЖСК',
        'XML_ID' => 'dzhsk',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1300 => 
      array (
        'ID' => '1300',
        'VALUE' => '214-ФЗ',
        'XML_ID' => 'fz214',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1302 => 
      array (
        'ID' => '1302',
        'VALUE' => 'Договор инвестирования',
        'XML_ID' => 'investment',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1304 => 
      array (
        'ID' => '1304',
        'VALUE' => 'Предварительный договор купли-продажи',
        'XML_ID' => 'pdkp',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_406' => 
    array (
      1306 => 
      array (
        'ID' => '1306',
        'VALUE' => 'В доме',
        'XML_ID' => 'indoors',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1308 => 
      array (
        'ID' => '1308',
        'VALUE' => 'На улице',
        'XML_ID' => 'outdoors',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_422' => 
    array (
      1372 => 
      array (
        'ID' => '1372',
        'VALUE' => '(все)',
        'XML_ID' => '7d87a099d79e026eba437493931754a7',
        'SORT' => '100',
        'DEF' => 'N',
      ),
      1310 => 
      array (
        'ID' => '1310',
        'VALUE' => '[1] Циан',
        'XML_ID' => 'dff8292fd74bc7c134b61a20bdfa9c4a',
        'SORT' => '200',
        'DEF' => 'N',
      ),
      1312 => 
      array (
        'ID' => '1312',
        'VALUE' => '[2] Яндекс. Недвижимость',
        'XML_ID' => 'd3bcba7682cc0f343c3829792ec0864e',
        'SORT' => '300',
        'DEF' => 'N',
      ),
      1314 => 
      array (
        'ID' => '1314',
        'VALUE' => '[3] Авито',
        'XML_ID' => '055918690121a1e8b617be726a93b611',
        'SORT' => '400',
        'DEF' => 'N',
      ),
      1380 => 
      array (
        'ID' => '1380',
        'VALUE' => '(не выгружать)',
        'XML_ID' => 'ce5d541fd84fa02496dbc828d51223a3',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_454' => 
    array (
      1348 => 
      array (
        'ID' => '1348',
        'VALUE' => 'есть',
        'XML_ID' => 'ad9feec508a8e3982e7913c423368936',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1350 => 
      array (
        'ID' => '1350',
        'VALUE' => 'нет',
        'XML_ID' => 'c201717b803159f72179caa3c08f5a76',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_456' => 
    array (
      1352 => 
      array (
        'ID' => '1352',
        'VALUE' => 'есть',
        'XML_ID' => 'b594d3715d689316733ec7116ea89970',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1354 => 
      array (
        'ID' => '1354',
        'VALUE' => 'нет',
        'XML_ID' => '6775642f024092f2725ec87ec31ca5ef',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_458' => 
    array (
      1356 => 
      array (
        'ID' => '1356',
        'VALUE' => 'есть',
        'XML_ID' => '326caf3e681de30fdaf63dc1ad9b9a91',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1358 => 
      array (
        'ID' => '1358',
        'VALUE' => 'нет',
        'XML_ID' => '7282e7d376931592c24ccf7a87b3748d',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_460' => 
    array (
      1360 => 
      array (
        'ID' => '1360',
        'VALUE' => 'есть',
        'XML_ID' => '0018bb9bec86624cb7637a071015bb43',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1362 => 
      array (
        'ID' => '1362',
        'VALUE' => 'нет',
        'XML_ID' => '771bb5f7d45d8a4f294b56cdb305ad45',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_462' => 
    array (
      1364 => 
      array (
        'ID' => '1364',
        'VALUE' => 'да',
        'XML_ID' => '2cb1c22caf8cadf413a3e9a588cfbe00',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1374 => 
      array (
        'ID' => '1374',
        'VALUE' => 'нет',
        'XML_ID' => '8567126eba01e0d641170789c10ae94e',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
    'PROPERTY_464' => 
    array (
      1366 => 
      array (
        'ID' => '1366',
        'VALUE' => 'Активный',
        'XML_ID' => '09ed2c8bd05f3598da1c47d9aeaa34bf',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1368 => 
      array (
        'ID' => '1368',
        'VALUE' => 'Неактивный (Успех)',
        'XML_ID' => 'ccd1f8fe40167e64e2f433ad2a9a0cc1',
        'SORT' => '500',
        'DEF' => 'N',
      ),
      1370 => 
      array (
        'ID' => '1370',
        'VALUE' => 'Неактивный (Отказ)',
        'XML_ID' => '512b9399802b8cf23bc8330f004138bb',
        'SORT' => '500',
        'DEF' => 'N',
      ),
    ),
  ),
  'PRODUCT_FIELDS' => 
  array (
    'ID' => 
    array (
      'name' => 'ID',
      'type' => 'integer',
      'multi' => '',
    ),
    'CATALOG_ID' => 
    array (
      'name' => 'Каталог',
      'type' => 'integer',
      'multi' => '',
    ),
    'PRICE' => 
    array (
      'name' => 'Цена',
      'type' => 'double',
      'multi' => '',
    ),
    'CURRENCY_ID' => 
    array (
      'name' => 'Валюта',
      'type' => 'string',
      'multi' => '',
    ),
    'NAME' => 
    array (
      'name' => 'Название',
      'type' => 'string',
      'multi' => '',
    ),
    'CODE' => 
    array (
      'name' => 'CODE',
      'type' => 'string',
      'multi' => '',
    ),
    'DESCRIPTION' => 
    array (
      'name' => 'Описание',
      'type' => 'string',
      'multi' => '',
    ),
    'DESCRIPTION_TYPE' => 
    array (
      'name' => 'Тип описания',
      'type' => 'string',
      'multi' => '',
    ),
    'ACTIVE' => 
    array (
      'name' => 'Активен',
      'type' => 'char',
      'multi' => '',
    ),
    'SECTION_ID' => 
    array (
      'name' => 'Раздел',
      'type' => 'integer',
      'multi' => '',
    ),
    'SORT' => 
    array (
      'name' => 'Сортировка',
      'type' => 'integer',
      'multi' => '',
    ),
    'VAT_ID' => 
    array (
      'name' => 'Ставка НДС',
      'type' => 'integer',
      'multi' => '',
    ),
    'VAT_INCLUDED' => 
    array (
      'name' => 'НДС включён в цену',
      'type' => 'char',
      'multi' => '',
    ),
    'MEASURE' => 
    array (
      'name' => 'Единица измерения',
      'type' => 'integer',
      'multi' => '',
    ),
    'XML_ID' => 
    array (
      'name' => 'Внешний код',
      'type' => 'string',
      'multi' => '',
    ),
    'PREVIEW_PICTURE' => 
    array (
      'name' => 'Картинка для анонса',
      'type' => 'product_file',
      'multi' => '',
    ),
    'DETAIL_PICTURE' => 
    array (
      'name' => 'Детальная картинка',
      'type' => 'product_file',
      'multi' => '',
    ),
    'DATE_CREATE' => 
    array (
      'name' => 'Дата создания',
      'type' => 'datetime',
      'multi' => '',
    ),
    'TIMESTAMP_X' => 
    array (
      'name' => 'Дата изменения',
      'type' => 'datetime',
      'multi' => '',
    ),
    'MODIFIED_BY' => 
    array (
      'name' => 'Кем изменён',
      'type' => 'integer',
      'multi' => '',
    ),
    'CREATED_BY' => 
    array (
      'name' => 'Кем создан',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_250' => 
    array (
      'name' => 'Тип объявления',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_252' => 
    array (
      'name' => 'Категория объекта',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_254' => 
    array (
      'name' => 'Категория коммерческого объекта',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_256' => 
    array (
      'name' => 'Тип здания',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_258' => 
    array (
      'name' => 'Тип сделки (жилая новостройка)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_260' => 
    array (
      'name' => 'Тип сделки (жилая)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_262' => 
    array (
      'name' => 'Тип сделки (коммерция)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_264' => 
    array (
      'name' => 'Количество комнат',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_266' => 
    array (
      'name' => 'Количество комнат, участвующих в сделке',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_268' => 
    array (
      'name' => 'Адрес',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_270' => 
    array (
      'name' => 'Район',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_272' => 
    array (
      'name' => 'Цена (м2)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_274' => 
    array (
      'name' => 'Цена в валюте',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_276' => 
    array (
      'name' => 'Цена за',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_278' => 
    array (
      'name' => 'Цена за период',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_280' => 
    array (
      'name' => 'Комиссия агента',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_282' => 
    array (
      'name' => 'Предоплата (%)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_284' => 
    array (
      'name' => 'Условия аренды',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_286' => 
    array (
      'name' => 'Размер обеспечительного платежа (%)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_288' => 
    array (
      'name' => 'Номер квартиры',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_290' => 
    array (
      'name' => 'Номер лота',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_292' => 
    array (
      'name' => 'Категория гаража',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_294' => 
    array (
      'name' => 'Кадастровый номер',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_296' => 
    array (
      'name' => 'Форма налогообложения арендодателя (для аренды)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_298' => 
    array (
      'name' => 'Этаж',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_300' => 
    array (
      'name' => 'Условия сделки',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_302' => 
    array (
      'name' => 'Общая площадь',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_304' => 
    array (
      'name' => 'Этажность',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_306' => 
    array (
      'name' => 'Жилая площадь',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_308' => 
    array (
      'name' => 'Площадь комнат',
      'type' => 'integer',
      'multi' => 'Y',
    ),
    'PROPERTY_310' => 
    array (
      'name' => 'Площадь кухни',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_312' => 
    array (
      'name' => 'Площадь участка',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_314' => 
    array (
      'name' => 'Балкон',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_316' => 
    array (
      'name' => 'Размер доли (%)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_318' => 
    array (
      'name' => 'Статус собственности',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_320' => 
    array (
      'name' => 'Санузел',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_322' => 
    array (
      'name' => 'Тип комнат',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_324' => 
    array (
      'name' => 'Вид из окон',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_326' => 
    array (
      'name' => 'Ремонт',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_328' => 
    array (
      'name' => 'Название гаражно-строительного кооператива',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_330' => 
    array (
      'name' => 'Состояние',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_332' => 
    array (
      'name' => 'Категория земель',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_334' => 
    array (
      'name' => 'Тип дома',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_336' => 
    array (
      'name' => 'Материал стен',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_338' => 
    array (
      'name' => 'Серия дома',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_340' => 
    array (
      'name' => 'Корпус дома',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_342' => 
    array (
      'name' => 'Год постройки дома',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_344' => 
    array (
      'name' => 'Высота потолков в метрах',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_346' => 
    array (
      'name' => 'Количество телефонных линий',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_348' => 
    array (
      'name' => 'Тип окон',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_350' => 
    array (
      'name' => 'Вход в помещение',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_352' => 
    array (
      'name' => 'Тип парковки',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_354' => 
    array (
      'name' => 'Материал стен (гараж)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_356' => 
    array (
      'name' => 'Особенности (гараж)',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_358' => 
    array (
      'name' => 'Расстояние до города, км.',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_360' => 
    array (
      'name' => 'Характеристики жилой недвижимости',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_362' => 
    array (
      'name' => 'Характеристики дома',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_364' => 
    array (
      'name' => 'Удобства',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_366' => 
    array (
      'name' => 'Покрытие пола',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_368' => 
    array (
      'name' => 'Рекомендуемое назначение объекта',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_370' => 
    array (
      'name' => 'Назначение склада',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_372' => 
    array (
      'name' => 'Класс бизнес-центра',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_374' => 
    array (
      'name' => 'Характеристики здания (коммерция)',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_376' => 
    array (
      'name' => 'Особенности нежилого помещения',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_378' => 
    array (
      'name' => 'Количество парковочных мест',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_380' => 
    array (
      'name' => 'Стоимость парковочного места',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_382' => 
    array (
      'name' => 'Количество гостевых парковочных мест',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_384' => 
    array (
      'name' => 'Дополнительные фотографии',
      'type' => 'product_file',
      'multi' => 'Y',
    ),
    'PROPERTY_386' => 
    array (
      'name' => 'Видео (ссылка на youtube.com)',
      'type' => 'string',
      'multi' => 'Y',
    ),
    'PROPERTY_388' => 
    array (
      'name' => 'Вариант платного размещения для (выгрузки в Авито)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_390' => 
    array (
      'name' => 'Отображать в блоке «Срочная продажа»',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_392' => 
    array (
      'name' => 'Список размещений',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_394' => 
    array (
      'name' => 'Условия размещения, которые нельзя применять к объявлению',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_396' => 
    array (
      'name' => 'Не использовать пакет размещений при публикации объявления',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_398' => 
    array (
      'name' => 'Дополнительная услуга по продвижению объявления (для Яндекс.Недвижимость)',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_400' => 
    array (
      'name' => 'Ставка на объявление в аукционе (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_402' => 
    array (
      'name' => 'Залог собственнику (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_404' => 
    array (
      'name' => 'Тип продажи (для Циан)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_406' => 
    array (
      'name' => 'Расположение санузла (для Циан)',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_408' => 
    array (
      'name' => 'Количество балконов (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_410' => 
    array (
      'name' => 'Количество лоджий (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_412' => 
    array (
      'name' => 'Количество раздельных санузлов (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_414' => 
    array (
      'name' => 'Количество совместных санузлов (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_416' => 
    array (
      'name' => 'Количество пассажирских лифтов (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_418' => 
    array (
      'name' => 'Количество грузовых лифтов (для Циан)',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_420' => 
    array (
      'name' => 'Ответственный менеджер',
      'type' => 'employee',
      'multi' => '',
    ),
    'PROPERTY_422' => 
    array (
      'name' => 'Выгружать в фиды',
      'type' => 'list',
      'multi' => 'Y',
    ),
    'PROPERTY_424' => 
    array (
      'name' => 'Дата начала активности',
      'type' => 'date',
      'multi' => '',
    ),
    'PROPERTY_426' => 
    array (
      'name' => 'Дата окончания активности',
      'type' => 'date',
      'multi' => '',
    ),
    'PROPERTY_428' => 
    array (
      'name' => 'Описание для анонса',
      'type' => 'text',
      'multi' => '',
    ),
    'PROPERTY_432' => 
    array (
      'name' => 'Собственник',
      'type' => 'crm',
      'multi' => '',
      'type_setting' => 
      array (
        'CONTACT' => 'Y',
        'COMPANY' => 'Y',
        'VISIBLE' => 'Y',
        'LEAD' => 'N',
        'DEAL' => 'N',
      ),
    ),
    'PROPERTY_436' => 
    array (
      'name' => 'ID объекта в ReCRM',
      'type' => 'integer',
      'multi' => '',
    ),
    'PROPERTY_438' => 
    array (
      'name' => 'Комментарий',
      'type' => 'text',
      'multi' => '',
    ),
    'PROPERTY_440' => 
    array (
      'name' => 'Страна (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_442' => 
    array (
      'name' => 'Регион (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_444' => 
    array (
      'name' => 'Район (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_446' => 
    array (
      'name' => 'Город (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_448' => 
    array (
      'name' => 'Район города (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_450' => 
    array (
      'name' => 'Улица (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_452' => 
    array (
      'name' => 'Номер дома (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_454' => 
    array (
      'name' => 'Лифт',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_456' => 
    array (
      'name' => 'Бассейн',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_458' => 
    array (
      'name' => 'Паркинг во дворе или доме',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_460' => 
    array (
      'name' => 'Охранная сигнализация',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_462' => 
    array (
      'name' => 'От застройщика',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_464' => 
    array (
      'name' => 'Статус',
      'type' => 'list',
      'multi' => '',
    ),
    'PROPERTY_474' => 
    array (
      'name' => 'Заблокирован (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_476' => 
    array (
      'name' => 'Баня (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_478' => 
    array (
      'name' => 'Вентиляция (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_480' => 
    array (
      'name' => 'Вид из окна (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_482' => 
    array (
      'name' => 'Возможность ипотеки (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_484' => 
    array (
      'name' => 'Возможность рассрочки (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_486' => 
    array (
      'name' => 'Газ (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_488' => 
    array (
      'name' => 'Готовый бизнес (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_490' => 
    array (
      'name' => 'Дата и время публикации (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_492' => 
    array (
      'name' => 'Ж/д ветка (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_494' => 
    array (
      'name' => 'Загородная недвижимость (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_496' => 
    array (
      'name' => 'Залог (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_498' => 
    array (
      'name' => 'Зарубежная недвижимость (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_500' => 
    array (
      'name' => 'Инфраструктура района (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_502' => 
    array (
      'name' => 'Использование земли (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_504' => 
    array (
      'name' => 'Канализация (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_506' => 
    array (
      'name' => 'Категория земель (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_508' => 
    array (
      'name' => 'Кол-во входных групп (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_510' => 
    array (
      'name' => 'Кол-во зон загрузки (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_512' => 
    array (
      'name' => 'Количество месяцев предоплаты (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_514' => 
    array (
      'name' => 'Комиссия для других риэлторов (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_516' => 
    array (
      'name' => 'Комиссия для клиента (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_518' => 
    array (
      'name' => 'Комиссия от собственника (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_520' => 
    array (
      'name' => 'Кондиционирование (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_522' => 
    array (
      'name' => 'Круглогодичный подъезд (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_524' => 
    array (
      'name' => 'Лоджия (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_526' => 
    array (
      'name' => 'Название коттеджного поселка (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_528' => 
    array (
      'name' => 'Наличие воды в помещении (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_530' => 
    array (
      'name' => 'Направление (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_532' => 
    array (
      'name' => 'Населенный пункт (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_534' => 
    array (
      'name' => 'Новостройка (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_536' => 
    array (
      'name' => 'Обременения (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_538' => 
    array (
      'name' => 'Объект новостройки для выгрузки Cian(идентификатор ЖК) (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_540' => 
    array (
      'name' => 'Объект новостройки для выгрузки Cian(идентификатор корпуса) (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_542' => 
    array (
      'name' => 'Объект новостройки для выгрузки Cian(название новостройки) (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_544' => 
    array (
      'name' => 'Объект новостройки для выгрузки Yandex(идентификатор ЖК) (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_546' => 
    array (
      'name' => 'Объект новостройки для выгрузки Yandex(название новостройки) (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_548' => 
    array (
      'name' => 'Окружение (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_550' => 
    array (
      'name' => 'Оператор телефонной связи (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_552' => 
    array (
      'name' => 'Операторы галереи (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_554' => 
    array (
      'name' => 'Отображать цену (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_556' => 
    array (
      'name' => 'Отопление (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_558' => 
    array (
      'name' => 'Отопление есть (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_560' => 
    array (
      'name' => 'Офисные помещения (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_562' => 
    array (
      'name' => 'Пандус (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_564' => 
    array (
      'name' => 'Парковка (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_566' => 
    array (
      'name' => 'Парковка для легкового транспорта (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_568' => 
    array (
      'name' => 'Планировка (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_570' => 
    array (
      'name' => 'Площадка для маневрирования большегрузного транспорта (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_572' => 
    array (
      'name' => 'Погрузочное оборудование (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_574' => 
    array (
      'name' => 'Пожарная сигнализация (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_576' => 
    array (
      'name' => 'Принадлежность (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_578' => 
    array (
      'name' => 'Район региона (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_580' => 
    array (
      'name' => 'Расстояние от города (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_582' => 
    array (
      'name' => 'Рельеф (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_584' => 
    array (
      'name' => 'Срок сдачи (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_586' => 
    array (
      'name' => 'Стадия строительства (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_588' => 
    array (
      'name' => 'Статус (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_590' => 
    array (
      'name' => 'Субаренда (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_592' => 
    array (
      'name' => 'Тип аренды (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_594' => 
    array (
      'name' => 'Тип здания жилое/нежилое (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_596' => 
    array (
      'name' => 'Тип НДС (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_598' => 
    array (
      'name' => 'Форма участка (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_600' => 
    array (
      'name' => 'Фотографии планировок (ReCRM)',
      'type' => 'product_file',
      'multi' => 'Y',
    ),
    'PROPERTY_602' => 
    array (
      'name' => 'Эксклюзивный договор с собственником (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_604' => 
    array (
      'name' => 'Электрическая мощность (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_606' => 
    array (
      'name' => 'Электричество (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_608' => 
    array (
      'name' => 'Является ли объект горячим предложением (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_610' => 
    array (
      'name' => 'Является ли объект скрытым (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_612' => 
    array (
      'name' => 'Якорные операторы (ReCRM)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_614' => 
    array (
      'name' => 'Картинки галереи',
      'type' => 'product_file',
      'multi' => 'Y',
    ),
    'PROPERTY_632' => 
    array (
      'name' => 'Дополнительные фотографии (не выгружаем)',
      'type' => 'product_file',
      'multi' => 'Y',
    ),
    'PROPERTY_634' => 
    array (
      'name' => 'Индекс (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_636' => 
    array (
      'name' => 'Широта (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
    'PROPERTY_638' => 
    array (
      'name' => 'Долгота (адрес)',
      'type' => 'string',
      'multi' => '',
    ),
  ),
  'SECTIONS' => 
  array (
    88 => 
    array (
      0 => 'Коммерческая недвижимость',
      90 => 
      array (
        0 => 'Помещение свободного назначения',
      ),
      92 => 
      array (
        0 => 'Торговые помещения',
      ),
      94 => 
      array (
        0 => 'Отдельно стоящее здание',
      ),
      96 => 
      array (
        0 => 'Автосервис',
      ),
      98 => 
      array (
        0 => 'Земли коммерческого назначения',
      ),
      100 => 
      array (
        0 => 'Офисные помещения',
      ),
      102 => 
      array (
        0 => 'Кафе и рестораны',
      ),
      104 => 
      array (
        0 => 'Производственные помещения',
      ),
      106 => 
      array (
        0 => 'Салон красоты',
      ),
      108 => 
      array (
        0 => 'Склады',
      ),
      142 => 
      array (
        0 => 'Бизнес-центры',
      ),
      144 => 
      array (
        0 => 'Гостиница',
      ),
      146 => 
      array (
        0 => 'Магазины',
      ),
      148 => 
      array (
        0 => 'Цеха',
      ),
      150 => 
      array (
        0 => 'Юридический адрес',
      ),
    ),
    110 => 
    array (
      0 => 'Квартиры и комнаты',
      112 => 
      array (
        0 => 'Квартиры',
      ),
      140 => 
      array (
        0 => 'Комнаты',
      ),
    ),
    114 => 
    array (
      0 => 'Загородная недвижимость',
      116 => 
      array (
        0 => 'Участки',
      ),
      128 => 
      array (
        0 => 'Дачи и коттеджи',
      ),
      130 => 
      array (
        0 => 'Дома',
        134 => 
        array (
          0 => 'Дома с участком',
        ),
      ),
      132 => 
      array (
        0 => 'Таунхаусы',
      ),
    ),
    120 => 
    array (
      0 => 'Аренда',
    ),
    122 => 
    array (
      0 => 'Гаражи',
    ),
    124 => 
    array (
      0 => 'Новостройки',
    ),
    126 => 
    array (
      0 => 'Спецпредложения',
    ),
  ),
  'SECTIONS_LIST' => 
  array (
    88 => 'Коммерческая недвижимость',
    110 => 'Квартиры и комнаты',
    114 => 'Загородная недвижимость',
    120 => 'Аренда',
    122 => 'Гаражи',
    124 => 'Новостройки',
    126 => 'Спецпредложения',
    90 => 'Помещение свободного назначения',
    92 => 'Торговые помещения',
    94 => 'Отдельно стоящее здание',
    96 => 'Автосервис',
    98 => 'Земли коммерческого назначения',
    100 => 'Офисные помещения',
    102 => 'Кафе и рестораны',
    104 => 'Производственные помещения',
    106 => 'Салон красоты',
    108 => 'Склады',
    142 => 'Бизнес-центры',
    144 => 'Гостиница',
    146 => 'Магазины',
    148 => 'Цеха',
    150 => 'Юридический адрес',
    112 => 'Квартиры',
    140 => 'Комнаты',
    116 => 'Участки',
    128 => 'Дачи и коттеджи',
    130 => 'Дома',
    132 => 'Таунхаусы',
    134 => 'Дома с участком',
    136 => 'Отдельные дома',
    138 => 'Часть дома',
  ),
  'ADMINS' => 
  array (
    0 => '1',
    1 => '8',
    2 => '14',
    3 => '16',
    4 => '72',
    5 => '182',
    6 => '182',
  ),
  'HOST' => 'https://commm.bitrix24.ru',
)
?>