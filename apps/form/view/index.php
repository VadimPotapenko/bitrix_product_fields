<div class='container-fluid'>
	<div class='row'>
		<!-- панель с разделами товаров -->
		<div class='col p-2 topborder'>
			<form class='form-inline'>
				<div class="form-group">
	    			<select class="form-control pr-5 pl-5 mr-5 ml-3 section-id" id="exampleFormControlSelect1">
	      				<?php foreach($appsConfig['SECTIONS'] as $key => $value): ?>
	      					<?php foreach($value as $k => $v): ?>
	      						<?php if(is_array($v)): ?>

	      							<?php foreach($v as $id => $data): ?>
	      								<?php if(is_array($data)): ?>
	      									<option value="<?=$id;?>"><?='-- '.current($data);?></option>
	      								<?php else: ?>
	      									<option value="<?=$k;?>"><?='- '.$data;?></option>
	      								<?php endif; ?>
	      							<?php endforeach; ?>

	      						<?php else: ?>
	      							<option value="<?=$key;?>"><?=$v;?></option>
	      						<?php endif; ?>
	      					<?php endforeach; ?>
	      				<?php endforeach; ?>
	    			</select>
	    			<button type='button' class='btn btn-info form-control pr-5 pl-5 save-editor'>Сохранить</button>
					<button type='button' class='btn btn-success form-control ml-5 pr-5 pl-5 load-editor'>Загрузить</button>
	  			</div>
	  		</form>
		</div>
	</div>
	<div class='row'>
		<!-- Поля товаров -->
		<div class='col-3 p-2 sideborder text-center'>
			<p class='h3 h3-color-grey'>Поля товара:</p>
			<ul class="list-group list-group-flush allfields">
				<?php foreach($appsConfig['PRODUCT_FIELDS'] as $key => $value): ?>
					<?php if ($key == 'NAME') continue; ?>
					<li 
						class="list-group-item input-field fields"
						data-multi="<?=$value['multi'];?>"
						data-type="<?=$value['type'];?>"
						data-fieldId="<?=$key;?>">
						<?=$value['name'];?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>

		<!-- Редактор -->
		<div class='col-9 central'>
			<div class='container bg-white border-dotted'>
				<div class='row mt-3'>
					<div class='col'>
						<p class="h4 formName" contenteditable="true"></p>
					</div>
				</div>

				<div class='row mt-3 text-right'>
					<div class='col'>
						<span class='logic-button'></span>
					</div>
				</div>
				<div class='row logic-display-border'>
					<div class='col logic-display'>
						<p class='h5 text-center pt-3 pb-3'>Настройка логический дествий</p>
						<div class='row'>
							<div class='col'>
								<div class='if-display'></div>
								<div class='add-if'>+ Добавить новое условие</div>
							</div>
						</div>

						<div class='row'>
							<div class='col'>
								<p>Обязательные поля:</p>
								<ul class='require-fields'></ul>
							</div>
						</div>
					</div>
				</div>

				<!-- левая/правая зона -->
				<div class='row mt-3 display-editor'>
					<div class='col'>
						<ul class='left p-5'></ul>
					</div>
					<div class='col'>
						<ul class='right p-5'></ul>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="alert alert-danger" role="alert">Не установлены поля для формы! Для успешного сохранения необходимо установить поля хотя бы для одной из зон.</div>
<div class="alert alert-success" role="alert">Сохранение формы прошло успешно!</div>