// скрипты клиентской части модуля редактор форм
function form () {
	// формируем поле адрес которое учавствует в формирование обязательного поля name 
	let left = document.querySelector('.left');
	let title = document.createElement('li');
	title.classList.add('list-group-item');
	title.classList.add('editor-field');

	title.innerText = 'Адрес';
	title.dataset.fieldid = ADDRESS_CLASSIFIER;
	title.dataset.type = 'string';
	title.dataset.multi = '';
	title.draggable = true;
	title.addEventListener('mousedown', dragDrop);
	addRequireField(title, 1);
	left.append(title);

	// смена наименования формы
	let titleName = document.querySelector('.formName');
	let selectSection = document.querySelector('.section-id');
	titleName.innerText = selectSection.children[0].innerText;

	selectSection.addEventListener('change', function (e) {
		for (let i = 0; i < e.target.children.length; i++) {
			if (e.target.children[i].selected == true)
				titleName.innerText = [...e.target.children[i].innerText].filter(value => value != '-').join('');
		}
	});

	// работа редактора
	[...document.querySelectorAll('.fields')].map(value => {
		value.addEventListener('click', function (e) {
			// создаем копию выбраного поля
			let copyTarget = document.createElement('li');
			copyTarget.classList.add('list-group-item');
			copyTarget.classList.add('editor-field');

			copyTarget.innerText = e.target.innerText;
			copyTarget.dataset.fieldid = e.target.dataset.fieldid;
			copyTarget.dataset.type = e.target.dataset.type;
			copyTarget.dataset.multi = e.target.dataset.multi;
			copyTarget.draggable = true;

			// добавляем поле в редактор
			const left = document.querySelector('.left');
			left.append(copyTarget);
			addRequireField(e.target);
			copyTarget.addEventListener('mousedown', dragDrop);
		});
	});

	// перетаскивание полей и добавление в правую/левую зону
	function dragDrop (event) {
		event.target.ondragstart = function() {
			return false;
		};
		let shiftX = event.clientX - event.target.getBoundingClientRect().left;
		let shiftY = event.clientY - event.target.getBoundingClientRect().top;
		event.target.style['position'] = 'absolute';
		event.target.style['min-width'] = '250px';
		event.target.style['box-shadow'] = '3px 3px 10px grey';
		event.target.style['cursor'] = 'grabbing';

		const centralZone = document.querySelector('.central');
		const display = document.querySelector('.display-editor');
		const leftZone = document.querySelector('.left');
		const rightZone = document.querySelector('.right');
		centralZone.style['backgroundColor'] = 'rgba(250, 103, 92, .4)';
		leftZone.style['border'] = '1px dotted lightgreen';
		leftZone.style['min-height'] = '50px';
		rightZone.style['border'] = '1px dotted lightgreen';
		rightZone.style['min-height'] = '50px';

		document.body.append(event.target);
		moveAt(event.pageX, event.pageY);

		function moveAt (x, y) {
			event.target.style.left = x - shiftX + 'px';
		    event.target.style.top = y - shiftY  + 'px';
		}
		function onMouseMove (e) {
			moveAt(e.pageX, e.pageY);
		}

		document.addEventListener('mousemove', onMouseMove);
		event.target.onmouseup = function (downevent) {
			document.removeEventListener('mousemove', onMouseMove);
			event.target.onmouseup = null;
			event.target.style['cursor'] = 'grab';

			rightZone.style['border'] = 'none';
			leftZone.style['border'] = 'none';
			centralZone.style['backgroundColor'] = 'white';

			let widthDropZone = display.getBoundingClientRect().x + display.getBoundingClientRect().width;
			let heightDropZone = display.getBoundingClientRect().y + display.getBoundingClientRect().height;
			if (downevent.pageX < display.getBoundingClientRect().x || downevent.pageX > widthDropZone){
				event.target.remove();
				removeRequireField(event.target);
			}

			if (downevent.pageY < display.getBoundingClientRect().y || downevent.pageY > heightDropZone) {
				event.target.remove();
				removeRequireField(event.target);
			}

			// левая зона
			let leftZoneWidth = leftZone.getBoundingClientRect().x + leftZone.getBoundingClientRect().width;
			let leftZoneHeight = leftZone.getBoundingClientRect().y + leftZone.getBoundingClientRect().height;
			if (
				(downevent.pageX >= leftZone.getBoundingClientRect().x && downevent.pageX <= leftZoneWidth) &&
				(downevent.pageY >= leftZone.getBoundingClientRect().y && downevent.pageY <= leftZoneHeight)
			) {
				sortZone(leftZone, event.target, downevent);
				event.target.style['position'] = 'static';
				event.target.style['min-width'] = '0';
				event.target.style['box-shadow'] = 'none';
			}

			// правая зона
			let rightZoneWidth = rightZone.getBoundingClientRect().x + rightZone.getBoundingClientRect().width;
			let rightZoneHeight = rightZone.getBoundingClientRect().y + rightZone.getBoundingClientRect().height;
			if (
				(downevent.pageX >= rightZone.getBoundingClientRect().x && downevent.pageX <= rightZoneWidth) &&
				(downevent.pageY >= rightZone.getBoundingClientRect().y && downevent.pageY <= rightZoneHeight)
			) {
				sortZone(rightZone, event.target, downevent);
				event.target.style['position'] = 'static';
				event.target.style['min-width'] = '0';
				event.target.style['box-shadow'] = 'none';
			}
		};
	}

	// сортировка внутри зон
	function sortZone (zone, target, event) {
		// сортировка списков
		if (!zone.children.length) zone.append(target);
		else {
			let arias = [], tmp = [], add = false;
			for (let i = 0; i < zone.children.length; i++) {
				arias.push({
					i: i,
					x: zone.children[i].getBoundingClientRect().x,
					yUp: zone.children[i].getBoundingClientRect().y,
					width: zone.children[i].getBoundingClientRect().x + zone.children[i].getBoundingClientRect().width,
					heightUp: zone.children[i].getBoundingClientRect().y + zone.children[i].getBoundingClientRect().height,
					element: zone.children[i]
				});
			}

			for (let i = 0; i < arias.length; i++) {
				if (event.pageY >= arias[i].yUp && event.pageY <= arias[i].heightUp) {
					zone.insertBefore(target, arias[i].element);
					add = true;
				}
			}
			if (!add) zone.append(target);
		}
	}

	// работа логической зоны
	// обязательные поля
	document.querySelector('.logic-button').addEventListener('click', function () {
		document.querySelector('.logic-display').classList.toggle('active');
	});
	function addRequireField (target, req = false) {
		let item = document.createElement('li');
		item.dataset.fieldid = target.dataset.fieldid;
		item.innerText = target.innerText;
		item.dataset.required = req ? '1' : '0';

		let required = document.createElement('span');
		required.classList.add('required-dot');
		if (item.dataset.fieldid == ADDRESS_CLASSIFIER)
			required.classList.add('required-dot-active');
		item.append(required);

		item.addEventListener('click', clickRequereField);
		document.querySelector('.require-fields').append(item);
	}

	function clickRequereField (requiredTarget) {
		if (requiredTarget.target.dataset.required == '0') requiredTarget.target.dataset.required = '1';
		else requiredTarget.target.dataset.required = '0';
		requiredTarget.target.children[0].classList.toggle('required-dot-active');
	}

	function removeRequireField (target) {
		let requireFields = document.querySelector('.require-fields');
		for (let i in requireFields.children) {
			if (target.dataset.fieldid == requireFields.children[i].dataset.fieldid) {
				requireFields.children[i].remove();
				break;
			}
		}
	}

	// условие
	document.querySelector('.add-if').addEventListener('click', function (ifTarget) {
		createIfElement([...document.querySelector('.allfields').children]);
	});
	function createIfElement (fields, If = false, To = false) {
		let parent = document.createElement('div');
		parent.classList.add('parent-if');
		let close = document.createElement('span');
		close.classList.add('close-if');
		close.addEventListener('click', function (closeTarget) {
			closeTarget.target.parentNode.remove();
		});

		let strFrom = document.createElement('p');
		strFrom.innerText = 'Если';
		let selectFrom = document.createElement('select');
		selectFrom.style.display = 'block';
		selectFrom.style.margin = '0 auto';

		for (let i = 0; i < fields.length; i++) {
			let option = document.createElement('option');
			option.innerText = fields[i].innerText;
			option.value = fields[i].dataset.fieldid;
			if (If == fields[i].dataset.fieldid) option.selected = true;
			selectFrom.append(option);
		}

		let strTo = document.createElement('p');
		strTo.innerText = 'то';
		let selectTo = document.createElement('select');
		selectTo.style.display = 'block';
		selectTo.style.margin = '0 auto';
		for (let i = 0; i < fields.length; i++) {
			let option = document.createElement('option');
			option.innerText = fields[i].innerText;
			option.value = fields[i].dataset.fieldid + '|' + fields[i].innerText + '|' + fields[i].dataset.type + '|' + fields[i].dataset.multi;
			if (To == fields[i].dataset.fieldid) option.selected = true;
			selectTo.append(option);
		}

		parent.append(strFrom);
		parent.append(selectFrom);
		parent.append(strTo);
		parent.append(selectTo);
		parent.append(close);
		document.querySelector('.if-display').append(parent);
	}

	// сохранение формы
	document.querySelector('.save-editor').addEventListener('click', function () {
		let danger = document.querySelector('.alert-danger');
		let success = document.querySelector('.alert-success');

		let section = document.querySelector('.section-id').value;
		let editor = getEditorData();
		let logic = getLogicData();
		if (isEmpty(editor.left) || isEmpty(editor.right)) {
			saveEditorToServer({[section]:{editor, logic}});
			success.style.display = 'block';
			setTimeout(() => success.style.display = 'none', 4000);
		} else {
			danger.style.display = 'block';
			setTimeout(() => danger.style.display = 'none', 4000);
		}
	});

	// собираем списки полей
	function getEditorData () {
		let right = {}, left = {};
		let rightList = document.querySelector('.right');
		let leftList = document.querySelector('.left');
		let formName = document.querySelector('.formName').innerText;
		
		for (let i = 0; i < rightList.children.length; i++)
			right[rightList.children[i].dataset.fieldid] = rightList.children[i].innerText + '|' + rightList.children[i].dataset.type + '|' + rightList.children[i].dataset.multi;
		for (let i = 0; i < leftList.children.length; i++)
			left[leftList.children[i].dataset.fieldid] = leftList.children[i].innerText + '|' + leftList.children[i].dataset.type + '|' + leftList.children[i].dataset.multi;

		return {formName, left, right};
	}

	// собираем данные логики
	function getLogicData () {
		let tmpOne = {}, tmpTwo = {};
		let logicDisplay = document.querySelector('.if-display');

		let requiredField = document.querySelector('.require-fields');
		for (let i = 0; i < requiredField.children.length; i++) {
			if (requiredField.children[i].dataset.required == '1')
				tmpTwo[i] = requiredField.children[i].dataset.fieldid;
		}

		for (let i = 0; i < logicDisplay.children.length; i++) {
			tmpOne[i] = {
				if: logicDisplay.children[i].children[1].value,
				to: logicDisplay.children[i].children[3].value,
			};
		}
		return {ifelse:tmpOne, required:tmpTwo};
	}

	// сохранение
	async function saveEditorToServer (data) {
		let url = PATH + '/apps/form/save.php';
		let response = await fetch(url, {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify(data)
		});
		let result = await response.json();
		// removeAllFields(); } // очищение полей после сохранения: откл
		console.log('Answer server: ' + result);
	}
	function isEmpty (collection) {
		for (let i in collection) return true;
		return false;
	}
	// очищение полей после сохранения: откл
	function removeAllFields () {
		document.querySelector('.right').innerHTML = '';
		document.querySelector('.left').innerHTML = '';
		document.querySelector('.require-fields').innerHTML = '';
		document.querySelector('.if-display').innerHTML = '';
	}


	// загрузка и отрисовка формы формы
	document.querySelector('.load-editor').addEventListener('click', function (e) {
		let section = document.querySelector('.section-id'), loadSection;
		for (let i = 0; i < section.children.length; i++)
			if (section.children[i].selected == true) loadSection = section.children[i].value;

		loadEditor(loadSection);
	});
	async function loadEditor (data) {
		let url = PATH + '/apps/form/load.php';
		let response = await fetch (url, {
			method: 'POST',
			body: JSON.stringify(data)
		});
		let result = await response.json();
		console.log('Answer server: OK');

		if (!result) {
			let danger = document.querySelector('.alert-danger');
			danger.innerText = 'Шаблон формы не найден!';
			danger.style.display = 'block';
			setTimeout(() => danger.style.display = 'none', 4000);

		} else {
			let left = document.querySelector('.left');
			let right = document.querySelector('.right');
			let requiredFields = document.querySelector('.require-fields');
			left.innerHTML  = '';
			right.innerHTML = '';
			requiredFields.innerHTML = '';

			for (let i in result.EDITOR.left) {
				let liElement = document.createElement('li');
				liElement.classList.add('list-group-item');
				liElement.classList.add('editor-field');
				liElement.draggable = true;
				liElement.dataset.fieldid = i;

				let attr = result.EDITOR.left[i].split('|');
				liElement.dataset.type = attr[1];
				liElement.dataset.multi = attr[2];
				liElement.innerText = attr[0];
				liElement.addEventListener('mousedown', dragDrop);
				left.append(liElement);
				if (result.LOGIC.required)
					addRequireField(liElement, Object.values(result.LOGIC.required).includes(i));
			}

			for (let i in result.EDITOR.right) {
				let liElement = document.createElement('li');
				liElement.classList.add('list-group-item');
				liElement.classList.add('editor-field');
				liElement.draggable = true;
				liElement.dataset.fieldid = i;

				let attr = result.EDITOR.right[i].split('|');
				liElement.dataset.type = attr[1];
				liElement.dataset.multi = attr[2];
				liElement.innerText = attr[0];
				liElement.addEventListener('mousedown', dragDrop);
				right.append(liElement);
				if(result.LOGIC.required)
					addRequireField(liElement, Object.values(result.LOGIC.required).includes(i));
			}

			if (result.LOGIC.ifelse.length > 0) {
				document.querySelector('.if-display').innerHTML = '';
				for (let i in result.LOGIC.ifelse) {
					createIfElement(
						[...document.querySelector('.allfields').children],
						result.LOGIC.ifelse[i].if,
						result.LOGIC.ifelse[i].to.split('|')[0]
					);
				}
			}
		}
	}
}