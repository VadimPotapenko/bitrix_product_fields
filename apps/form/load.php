<?php
/**
* Загрузка шаблона формы
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#====================== settings ==========================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);
$getItem = CRestPlus::call('entity.item.get', array('ENTITY' => ENTITY_NAME, 'filter' => array('NAME' => 'SECTION_'.$json_arr)));
if (isset($getItem['result']) && !empty($getItem['result'])) {
	$returnArr = array(
		'EDITOR' => json_decode($getItem['result'][0]['PROPERTY_VALUES']['EDITOR']),
		'LOGIC'  => json_decode($getItem['result'][0]['PROPERTY_VALUES']['LOGIC'])
	);
	echo json_encode($returnArr);
} else echo json_encode(null);