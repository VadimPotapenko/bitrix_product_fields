<?php
/**
* Сохранение формы
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#====================== settings ==========================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);
$getItem = CRestPlus::call('entity.item.get', array('ENTITY' => ENTITY_NAME, 'filter' => array('NAME' => 'SECTION_'.key($json_arr))));
if (isset($getItem['result']) && !empty($getItem['result'])) {
	$updateElement = CRestPlus::call('entity.item.update', array(
		'ENTITY'          => ENTITY_NAME,
		'ID'              => $getItem['result'][0]['ID'],
		'PROPERTY_VALUES' => array(
			PROPERTY_EDITOR => json_encode(current($json_arr)['editor']),
			PROPERTY_LOGIC  => json_encode(current($json_arr)['logic']),
		)
	));

} else {
	$addElement = CRestPlus::call('entity.item.add', array(
		'ENTITY'  => ENTITY_NAME,
		'NAME'    => 'SECTION_'.key($json_arr),
		'PROPERTY_VALUES' => array(
			PROPERTY_EDITOR => json_encode(current($json_arr)['editor']),
			PROPERTY_LOGIC  => json_encode(current($json_arr)['logic']),
		)
	));
}
echo json_encode('OK');