<?php
/**
* Точка входа модуля редактор форм
* синхронизирует настройки модулей и подтягивает вид
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#====================== settings ==========================#
### настройка хранилища ###
$entity = CRestPlus::call('entity.get', array('ENTITY' => ENTITY_NAME));
if (!isset($entity['result']) || empty($entity['result']))
	CRestPlus::call('entity.add', array('ENTITY' => ENTITY_NAME, 'NAME' => ENTITY_NAME));

### настройка свойств элемента сущности ###
$propertyList = CRestPlus::call('entity.item.property.get', array('ENTITY' => ENTITY_NAME));
if (!isset($propertyList['result']) || empty($propertyList['result'])) {
	$propertyData = array(
		array(
			'method' => 'entity.item.property.add',
			'params' => array('ENTITY' => ENTITY_NAME, 'PROPERTY' => PROPERTY_EDITOR, 'NAME' => PROPERTY_EDITOR)
		),
		array(
			'method' => 'entity.item.property.add',
			'params' => array('ENTITY' => ENTITY_NAME, 'PROPERTY' => PROPERTY_LOGIC, 'NAME' => PROPERTY_LOGIC)
		)
	);
	CRestPlus::callBatch($propertyData);
}

require_once __DIR__.'/view/index.php';