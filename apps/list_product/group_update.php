<?php
/**
* Груповой апдейт товаров
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#============================ settings ================================#
if (isset($_POST) && !empty($_POST['ids'])) {
	$ids = explode(',', $_POST['ids']);
	unset($_POST['ids']);
	foreach ($_POST as $key => $value) {
		if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'char')
			$field[$key] = $value == 'on' ? 'Y' : 'N';
		if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'crm')
			$field[$key] = explode('|', $value)[1];
		else
			$field[$key] = $value;
	}

	foreach ($ids as $id) {
		$updateData[] = array(
			'method' => 'crm.product.update',
			'params' => array(
				'id' => $id,
				'fields' => array(key($field) => current($field))
			)
		);
	}
	if (count($updateData) > 50) $updateData = array_chunk($updateData, 50);
	else $updateData = array($updateData);
	for ($i = 0; $i < count($updateData); $i++)
		$result = CRestPlus::callBatch($updateData[$i]);
	echo json_encode('OK');
}