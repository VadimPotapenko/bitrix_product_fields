<?php
/**
* Удаление фото из товара
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#============================ settings ================================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);
if (!empty($json_arr)) {
	foreach ($json_arr as $value)
		$files[$value['file']] = true;

	$product = CRestPlus::call('crm.product.get', array('ID' => current($json_arr)['id']));
	foreach ($product['result'][current($json_arr)['field']] as $value)
		if ($files[$value['value']['id']]) $valueId[] = $value['valueId'];

	if (isset($valueId) && !empty($valueId)) {
		usort($valueId, function ($a, $b) { return $a < $b; });
		foreach ($valueId as $value) {
			$deleteData[] = array(
				'method' => 'crm.product.update',
				'params' => array(
					'ID' => current($json_arr)['id'],
					'fields' => array(
						current($json_arr)['field'] => array('valueId' => $value, 'value' => array('remove' => 'Y'))
					)
				)
			);
		}
	}
	CRestPlus::callBatch($deleteData);
}
echo json_encode('OK');