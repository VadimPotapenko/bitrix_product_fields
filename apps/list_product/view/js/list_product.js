// скрипты клиентской части модуля добавление товара
function list_product () {
	let months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
	let d = new Date();
	let from = d.setMonth(d.getMonth() - 1);
	let CHOOSE_FILTER = { // текущий фильтр таблицы
		[RESPONSE_MANAGER]:CURRENT_USER['user']['ID'],
		'>=DATE_CREATE': d.getFullYear() + '-' + months[d.getMonth()] + '-' + (d.getDate().length > 1 ? d.getDate() : '0' + d.getDate()),
	};
	let FILTER, BAD_FIELDS = [], currentFields = [], currentDetailFields = [], DELETE_PHOTOS = [], CHECKED_ID = [], UPDATE_PHOTOS = [];
	let CRM_ENTITY = {C:'contact', D:'deal', CO:'company', L:'lead'};
	let NULL_FIELDS = ['ID', 'PRICE', 'NAME', 'DESCRIPTION', 'DESCRIPTION_TYPE', 'DATE_CREATE', 'CATALOG_ID', 'CURRENCY_ID', 'ACTIVE'];
	let BUTTONS = document.createElement('div');
	let CURRENCY = {RUB:'Российский рубль', USD:'Доллар США', EUR:'Евро', UAH:'Гривна', BR:'Белорусский доллар'};
	const CURRENT_ADDRESS = {};
	getDefaultFields();
	// run
	getDataTable(currentFields, CHOOSE_FILTER, renderData);
	localStorage.setItem([RESPONSE_MANAGER], CURRENT_USER['user']['ID']);
	localStorage.setItem('DATE_CREATE|from', d.getFullYear() + '-' + months[d.getMonth()] + '-' + (d.getDate().length > 1 ? d.getDate() : '0' + d.getDate()));

	// достаем данные для таблицы
	async function getDataTable (select, filter, func) {
		let wait = document.querySelector('.wait');
		wait.style.display = 'block';
		let url = PATH + '/apps/list_product/get_datatable.php';
		let response = await fetch(url, {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify({select, filter, user:CURRENT_USER['user']['ID']})
		});

		if (response.ok) {
			let json = await response.json();
			FILTER  = json.filter;
			BAD_FIELDS = [...json.badField];

			func(json.products, initDataTable);
			console.log('Answer server: OK');
			wait.style.display = 'none';

			if (BUTTONS.children.length === 0) {
				for (let i in FIELDS) {
					if (['CODE', 'XML_ID', 'SORT'].includes(i)) continue;
					if (BAD_FIELDS.includes(i)) continue;
					if (['product_file', 'map'].includes(FIELDS[i].type)) continue;
					let button = document.createElement('button');
					button.addEventListener('click', reRenderTable);
					button.classList.add('btn');
					button.classList.add('btn-block');
					button.classList.add('btn-light');
					button.dataset.id = i;
					button.dataset.got = 0;
					button.innerText = FIELDS[i].name;
					BUTTONS.append(button);
				}
			}

		} else {
			console.log("Error HTTP: " + response.status);
			wait.children[0].innerText = 'Произошла ошибка. Перезагрузите страницу';
		}
	}

	// рендерим таблицу
	function renderData (data, init) {
		$('#example').DataTable().destroy();
		removeElementTable();
		if (data !== null) {
			[...document.querySelectorAll('.thead')].map(value => {
				value.innerHTML = '';
				let html = '<tr>';
				html += "<td><div class='check-box filter-menu text-center' data-checked='0'></div></td>";
				for (let i in data[Object.keys(data)[0]])
					html += `<td class='filter-menu text-center'>${FIELDS[i].name}</td>`;

				html += '</tr>';
				value.innerHTML = html;
			});

			let tbody = document.querySelector('.data-table');
			tbody.innerHTML = '';
			let html = '';
			for (let i in data) {
				html += '<tr class="data-detail">';
				html += "<td><div class='check-box' data-checked='0'></div></td>";
				for (let x in data[i]) {
					if (x == 'PREVIEW_PICTURE') {
						if (data[i][x] !== '') {
							// src='assets/images/no-photo.png'    no-photo src='${HOST + data[i][x]}'
							html += `<td data-img-src='${HOST + data[i][x]}'>
								<img class='rounded thumbnail previus images onload'  data-img-src='${HOST + data[i][x]}' alt='Не удалось загрузить'>
							</td>`;
						} else {
							html += `<td>${data[i][x]}</td>`;
						}
					} else if (FIELDS[x].type == 'crm') {
						let crmdata = data[i][x].split('|');
						html += `<td><a class='not-open' target='_blank' href='${HOST + '/crm/' + CRM_ENTITY[crmdata[2]] + '/details/' + crmdata[1] + '/'}'>${crmdata[0]}</a></td>`;

					} else html += `<td>${data[i][x]}</td>`;
				}
			}
			tbody.innerHTML = html;
			notOpen();
			// showImages('images');

		} else {
			[...document.querySelectorAll('.thead')].map(value => {
				value.innerHTML = '';
				let html = '<tr>';
				for (let i = 0; i < NULL_FIELDS.length; i++)
					html += `<td class='filter-menu text-center'>${FIELDS[NULL_FIELDS[i]].name}</td>`;

				html += '</tr>';
				value.innerHTML = html;
			});
			document.querySelector('.data-table').innerHTML = '';
		}
		init();
		formForGroupEditing();
		checkBox();
	}

	function notOpen () {
		[...document.querySelectorAll('.not-open')]
		.map (value => {
			value.addEventListener('click', e => e.stopPropagation());
		});
	}

	// получаем детальную информацию о товаре
	async function getDataProduct (id, funcRender) {
		let wait = document.querySelector('.wait');
		wait.style.display = 'block';
		let url = PATH + '/apps/list_product/product_details.php';
		let response = await fetch(url, {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify({id, user:CURRENT_USER['user']['ID']})
		});

		if (response.ok) {
			let json = await response.json();
			funcRender(json);
			console.log('Answer server: OK');
			wait.style.display = 'none';

		} else {
			console.log("Error HTTP: " + response.status);
			wait.children[0].innerText = 'Произошла ошибка. Перезагрузите страницу';
		}
	}

	// рендерим детальную информацию о товаре 
	function renderInfo (input) {
		let data = input.detail;
		let display = document.querySelector('.detail-info');
		display.innerHTML = '';
		if (input.render !== null) {
			let renderFields = [...Object.keys(input.render.left)].concat([...Object.keys(input.render.right)]);
			renderFields.unshift('ID');
			renderFields.push('SECTION_ID');
			input.render.left.ID = 'ID|number|';

			if (renderFields.includes(PUBLISH))
				renderFields.push(NOT_PUBLISH);

			let title = document.createElement('p');
			title.classList.add('h3');
			title.classList.add('text-center');
			title.innerText = data.NAME.value;
			display.append(title);

			let zone = document.createElement('div');
			zone.classList.add('row');
			let left = document.createElement('div');
			left.classList.add('col');
			let right = document.createElement('div');
			right.classList.add('col');
			zone.append(left);
			zone.append(right);

			let tableLeft = document.createElement('table');
			tableLeft.classList.add('table');
			tableLeft.classList.add('text-center');
			tableLeft.classList.add('table-striped');
			left.append(tableLeft);

			let tableRight = document.createElement('table');
			tableRight.classList.add('table');
			tableRight.classList.add('text-center');
			tableRight.classList.add('table-striped');
			right.append(tableRight);

			let html = '';
			let colspan = 0;
			for (let i in data) {
				if (!renderFields.includes(i)) continue;
				currentDetailFields.push(i);
				if (data[i].type == 'product_file') {
					if (FIELDS[i].multi == 'Y') {
						html = `<tr class='tr-detail'>
							<td data-multi='${(FIELDS[i].multi == 'Y') ? '1' : '0'}' data-type='${data[i].type}' data-name='${i}'>
								<b>${data[i].title}:</b>
							</td><td style='position:relative; height:200px;' class='gallary' ${i == PUBLISH ? 'data-public' : ''} ${i == NOT_PUBLISH ? 'data-notpublic' : ''}>`;
						if (data[i].value) {
							for (let x = 0; x < data[i].value.length; x++) {
								html += `<img 
									class="rounded thumbnail images"
									style='cursor:pointer;display:inline-block;'
									src='${HOST + data[i].value[x]}'
									data-src='${HOST + data[i].value[x]}'>`;
								colspan += 1;
							}
						}
						html += '</td></tr>';

					} else {
						html = `<tr class='tr-detail'>
							<td data-type='${data[i].type}' data-name='${i}'><b>${data[i].title}:</b></td>
							<td class='colspan gallary' style='cursor:pointer;'>`;
						if (data[i].value) html += `<img class="rounded thumbnail" data-src='${HOST + data[i].value}' src='${HOST + data[i].value}'>`;
						html += `</td></tr>`;
					}
					[tableLeft, tableRight][Object.keys(input.render.left).includes(i) ? '0' : '1'].innerHTML += html;
					continue;
				}

				if (i == 'SECTION_ID') {
					html = `<tr class='tr-detail'>
						<td 
							data-multi='${(FIELDS[i].multi == 'Y') ? '1' : '0'}' 
							data-type='${data[i].type}' ${data[i].type == 'crm' ? 'data-crm="' + (data[i].crm_code || 'не установлено') +'"' : ''} 
							data-name='${i}'
							data-int='${data[i].int}'
						>
							<b>${data[i].title}:</b>
						</td>
						<td class='colspan'>${(data[i].value)
							? Object.keys(CURRENCY).includes(data[i].value)
								? CURRENCY[data[i].value]
								: data[i].value
							: '<i>Значение не установлено</i>'}</td>
						</div>
					</div>`;
					[tableLeft, tableRight][Object.keys(input.render.left).includes(i) ? '0' : '1'].innerHTML += html
					continue;
				}

				if (data[i].type == 'char') {
					html = `<tr class='tr-detail'>
						<td data-type='${data[i].type}' data-name='${i}'><b>${data[i].title}:</b></td>
						<td class='colspan'>${(data[i].value == 'Y') ? 'Да' : 'Нет'}</td>
					</tr>`;
					[tableLeft, tableRight][Object.keys(input.render.left).includes(i) ? '0' : '1'].innerHTML += html;
					continue;
				}

				if (data[i].type == 'employee') {
					html = `<tr class='tr-detail'>
						<td data-type='${data[i].type}' data-name='${i}'><b>${data[i].title}:</b></td>
						<td class='colspan'>${USERS[data[i].value] || 'Значение не установлено'}</td>
					</tr>`;
					[tableLeft, tableRight][Object.keys(input.render.left).includes(i) ? '0' : '1'].innerHTML += html
					continue;
				}

				if (data[i].type == 'crm') {
					let value = data[i].value.split('|');
					html = `<tr class='tr-detail'>
						<td data-type='${data[i].type}' ${data[i].type == 'crm' ? 'data-crm="' + (data[i].crm_code || 'не установлено') +'"' : ''} data-name='${i}'>
							<b>${data[i].title}:</b>
						</td>
						<td class='colspan'><a href='${HOST + '/crm/' + CRM_ENTITY[value[2]] + '/details/' + value[1] + '/'}' target='_blank'>${value[0] || 'Значение не установлено'}</a></td>
					</tr>`;
					[tableLeft, tableRight][Object.keys(input.render.left).includes(i) ? '0' : '1'].innerHTML += html
					continue;
				}

				html = `<tr class='tr-detail'>
					<td 
						data-multi='${(FIELDS[i].multi == 'Y') ? '1' : '0'}' 
						data-type='${data[i].type}' ${data[i].type == 'crm' ? 'data-crm="' + (data[i].crm_code || 'не установлено') +'"' : ''} 
						data-name='${i}'
					>
						<b>${data[i].title}:</b>
					</td>
					<td class='colspan'>${(data[i].value)
						? Object.keys(CURRENCY).includes(data[i].value)
							? CURRENCY[data[i].value]
							: data[i].value
						: '<i>Значение не установлено</i>'}</td>
					</div>
				</div>`;
				[tableLeft, tableRight][Object.keys(input.render.left).includes(i) ? '0' : '1'].innerHTML += html
			}
			display.append(zone);
			if (colspan > 0)
				[...document.querySelectorAll('.colspan')].map(value => value.setAttribute('colspan', colspan));
			setTimeout(showImages, 1000, 'gallary');
		}
	}

	// окно с детальной информацией о товаре
	function openDetailWindow (e) {
		let displayDetail = document.querySelector('.detail-product-window');
		document.querySelector('.detail-info').dataset.update = '0';
		if (!displayDetail.style.display || displayDetail.style.display == 'none')
			displayDetail.style.display = 'block';

		let close = document.querySelector('.close-detail');
		close.addEventListener('click', closeW);
		getDataProduct(e.target.parentNode.children[1].innerText, renderInfo);
		function closeW () {
			let edit = document.querySelector('.detail-add');
			if (edit.style.display == 'block') {
				isSaveEditProduct()
				.then(() => {
					currentDetailFields = [];
					DELETE_PHOTOS = [];
					UPDATE_PHOTOS = [];
					document.querySelector('.detail-add').style.display = 'none';
					displayDetail.style.display = 'none';
					close.removeEventListener('click', closeW);
				});
			} else {
				currentDetailFields = [];
				DELETE_PHOTOS = [];
				UPDATE_PHOTOS = [];
				document.querySelector('.detail-add').style.display = 'none';
				displayDetail.style.display = 'none';
				close.removeEventListener('click', closeW);
			}
		}
	}

	// фильтр
	document.querySelector('.filter').addEventListener('click', function (e) {
		let container = document.querySelector('.detail-filter-window');
		if (!container.style.display || container.style.display == 'none')
			container.style.display = 'block';

		createForm(FILTER, SECTIONS, USERS);
		let close = document.querySelector('.close-filter');
		close.addEventListener('click', function () {
			container.style.display = 'none';
		});
	});

	function createForm (filter, sections, users) {
		let x = 0, html = '',
		form = document.querySelector('.create-form'),
		right = document.createElement('div'),
		left = document.createElement('div'),
		button = document.createElement('button');

		left.innerHTML = '';
		right.innerHTML = '';
		form.innerHTML = '';
		right.classList.add('row');
		right.classList.add('mt-3');
		left.classList.add('row');
		left.classList.add('mt-3');
		button.classList.add('btn');
		button.classList.add('btn-info');
		button.classList.add('btn-block');
		button.classList.add('mt-3');
		button.innerText = 'Применить';
		button.addEventListener('click', sendForm);

		for (let i in filter) {
			if (filter[i] == 'section') {
				html = `<div class='col-6 form-group'>
					<label for='form_${i}'>${FIELDS[i].name}:</label>
					<select class='form-control' name='${i}' id='form_${i}'><option value=''>Значение не установлено</option>`;
				for (let id in sections) html += `<option ${localStorage.getItem(i) == id ? 'selected' : ''} value='${id}'>${sections[id]}</option>`;
				html += '</select></div>';

			} else if (filter[i] == 'range') {
				html = `<div class='col-6 form-group'>
					<label for='form_${i}_from' class='col-12'>${FIELDS[i].name} <i>(диапозон от/до)</i>:</label>
					<input
						class='form-control col-6 range'
						type='number'
						id='form_${i}_from'
						placeholder='${FIELDS[i].name}'
						name='${i}|from'
						value='${localStorage.getItem(i+'|from')}'
					>
					<input
						class='form-control col-5 range'
						type='number'
						id='form_${i}_to'
						placeholder='${FIELDS[i].name}'
						name='${i}|to'
						value='${localStorage.getItem(i+'|to')}'
					>
				</div>`;

			} else if (filter[i] == 'daterange') {
				html = `<div class='col-6 form-group'>
					<label for='form_${i}_from' class='col-12'>${FIELDS[i].name} <i>(диапозон от/до)</i>:</label>
					<input
						class='form-control col-6 range'
						type='date'
						id='form_${i}_from'
						placeholder='${FIELDS[i].name}'
						name='${i}|from'
						value='${localStorage.getItem(i+'|from')}'
					>
					<input
						class='form-control col-5 range'
						type='date'
						id='form_${i}_to'
						placeholder='${FIELDS[i].name}'
						name='${i}|to'
						value='${localStorage.getItem(i+'|to')}'
					>
				</div>`;


			} else if (i == 'CURRENCY_ID') {
				html = `<div class='col-6 form-group'>
					<label for='form_${i}' class='col-12'>${FIELDS[i].name} <i>(диапозон от/до)</i>:</label>
					<select name='${i}' id='form_${i}' class='form-control'>
						<option value='RUB'>Российский рубль</option>
						<option value='USD'>Доллар США</option>
						<option value='EUR'>Евро</option>
						<option value='UAH'>Гривна</option>
						<option value='BR'>Белорусский рубль</option>
					</select>
				</div>`;

			} else if (filter[i] == 'crm') {
				html = `<div class='col-6 form-group'>
					<label class='col-12' for='form_${i}'>${FIELDS[i].name}:</label>
					<input 
						type='text'
						class='form-control range'
						name='${i}|crm'
						id='form_${i}'
						value='${localStorage.getItem(i) == null ? '' : localStorage.getItem(i)}'
					>
				</div>`;
				setCRMSetting('form_' + i, FIELDS[i]);

			} else if (filter[i] == 'employee') {
				html = `<div class='col-6 form-group'>
					<label for='form_${i}'>${FIELDS[i].name}:</label>
					<select class='form-control' name='${i}' id='form_${i}'><option value=''>Значение не установлено</option>`;
				for (let user in users) html += `<option ${localStorage.getItem(i) == user ? 'selected' : ''} value='${user}'>${users[user]}</option>`;
				html += '</select></div>';

			} else if (filter[i] == 'lists') { 
				html = `<div class='col-6 form-group'>
					<label for='form_${i}'>${FIELDS[i].name}:</label>
					<select class='form-control' name='${i}' id='form_${i}'><option value=''>Значение не установлено</option>`;
				html += '</select></div>';
				getList(i.split('_')[1], createOptions, 'form_' + i, [localStorage.getItem(i)]);

			} else {
				html = `<div class='col-6 form-group'>
					<label for='form_${i}'>${FIELDS[i].name}:</label>
					<input
						class='form-control'
						type='${filter[i]}'
						id='form_${i}'
						placeholder='${FIELDS[i].name}'
						name='${i}' value='${localStorage.getItem(i) == null ? '' : localStorage.getItem(i)}'
					>
				</div>`;
			}
			[left, right][x].innerHTML += html;
			x = 1 - x;
		}

		form.append(left);
		form.append(right);
		form.append(button);
	}

	// получаем значения списочных полей
	async function getList (id, callback, x, selected = []) {
		let response = await fetch(PATH + '/apps/add_product/get_list_value.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify(id)
		});

		if (response.ok) {
			let json = await response.json();
			let options = callback(json, x, selected);
			console.log('Answer server: OK');
		} else
			console.log("Error HTTP: " + response.status);
	}
	function createOptions (list, x, selected) {
		let select = document.querySelector(`#${x}`);
		for (let i in list)
			select.innerHTML += `<option ${selected.includes(list[i]) ? 'selected' : ''} value='${i}'>${list[i]}</option>`;
	}

	function sendForm (e) {
		let form = document.querySelector('.create-form');
		for (let i = 0; i < form.length - 1; i++){
			if (form[i].value == '') {
				localStorage.removeItem(form[i].name);
				delete CHOOSE_FILTER[form[i].name];
				continue;
			} else localStorage.setItem(form[i].name, form[i].value);
			CHOOSE_FILTER[form[i].name] = form[i].value;
		}
		getDataTable(currentFields, CHOOSE_FILTER, renderData);
		document.querySelector('.detail-filter-window').style.display = 'none';
		e.preventDefault();
	}

	// сброс фильтра
	document.querySelector('.filter_off').addEventListener('click', function () {
		CHOOSE_FILTER = {};
		getDataTable(currentFields, CHOOSE_FILTER, renderData);
	});

	// добавление полей в таблицу
	document.querySelector('.add').addEventListener('click', function (e) {
		let contextWindow = document.querySelector('.context');
		contextWindow.innerHTML = '';
		contextWindow.classList.toggle('block');
		contextWindow.style.top = e.target.getBoundingClientRect().top + e.target.getBoundingClientRect().height + 'px' ;
		contextWindow.style.left = e.target.getBoundingClientRect().left + 'px';
		document.addEventListener('click', function (event) {
			if ([...contextWindow.classList].includes('block') && event.target != contextWindow && event.target != e.target)
				contextWindow.classList.remove('block');
		});
		contextWindow.append(BUTTONS);
		fieldsInLocalStorage();
	});

	// перерендеринг таблицы
	function reRenderTable (e) {
		if (e.target.dataset.got == 0) {
			e.target.dataset.got = 1;
			e.target.classList.add('border');
			e.target.classList.add('border-success');
			localStorage.setItem('field&'+e.target.dataset.id, 1);
			currentFields.push(e.target.dataset.id);
			getDataTable(currentFields, CHOOSE_FILTER, renderData);
		} else {
			e.target.dataset.got = 0;
			e.target.classList.remove('border');
			e.target.classList.remove('border-success');
			localStorage.removeItem('field&'+e.target.dataset.id);
			currentFields = currentFields.filter(value => e.target.dataset.id !== value);
			getDataTable(currentFields, CHOOSE_FILTER, renderData);
		}
	}

	// сохранение полей таблицы в локалсторадж
	function fieldsInLocalStorage () {
		let buttons = [...BUTTONS.children];
		let storage = Object.keys(localStorage);
		for (let i = 0; i < buttons.length; i++) {
			if (storage.includes('field&' + buttons[i].dataset.id)){
				buttons[i].dataset.got = 1;
				buttons[i].classList.add('border');
				buttons[i].classList.add('border-success');
			}
		}
	}

	// апдейт детальной информации товара
	document.querySelector('.edit').addEventListener('click', function () {
		let add = document.querySelector('.detail-add');
		add.style.display = 'block';
		add.addEventListener('click', addFieldForDetail);

		let container = document.querySelector('.detail-info');
		if (container.dataset.update === '0') {
			container.dataset.update = '1';
			let btn = document.createElement('button');
			btn.innerText = 'Сохранить';
			btn.classList.add('btn');
			btn.classList.add('btn-info');
			btn.classList.add('btn-block');
			btn.classList.add('mt-3');
			btn.classList.add('mb-3');
			btn.addEventListener('click', updateProduct);

			let cols = [...document.querySelectorAll('.tr-detail')];
			for (let i = 0; i < cols.length; i++) {
				if (['ID', 'DATE_CREATE', 'MODIFIED_BY', 'CREATED_BY', 'SECTION_ID'].includes(cols[i].children[0].dataset.name)) continue;
				if (cols[i].children[0].dataset.name == 'CURRENCY_ID') {
					cols[i].children[1].innerHTML = `
					<select name='${cols[i].children[0].dataset.name}' class='form-control'>
						<option value='RUB'>Российский рубль</option>
						<option value='USD'>Доллар США</option>
						<option value='EUR'>Евро</option>
						<option value='UAH'>Гривна</option>
						<option value='BR'>Белорусский рубль</option>
					</select>`;

				} else if (cols[i].children[0].dataset.name == ADDRESS_CLASSIFIER) {
					cols[i].children[1].innerHTML = `<input
						type='${cols[i].children[0].dataset.type == 'string' ? 'text' : 'number'}'
						class='form-control' value='${cols[i].children[1].innerText == 'Значение не установлено' ? '' : cols[i].children[1].innerText}'
						name='${cols[i].children[0].dataset.name}'
					>`;
					bindAddress();

				} else if (['char'].includes(cols[i].children[0].dataset.type)) {
					cols[i].children[1].innerHTML = `<input type='checkbox'
						${cols[i].children[1].innerText == 'Да' ? 'checked' : ''}
						name='${cols[i].children[0].dataset.name}'
					>`;

				} else if (['date', 'datetime'].includes(cols[i].children[0].dataset.type)) {
					cols[i].children[1].innerHTML = `<input
						type='date' class='form-control' value='${cols[i].children[1].innerText}'
						name='${cols[i].children[0].dataset.name}'
					>`;

				} else if (['product_file'].includes(cols[i].children[0].dataset.type)) {
					for (let x = 2; x < cols[i].children.length; x++) cols[i].children[x].innerHTML = '';
					cols[i].children[1].removeEventListener('click', goGallery);
					for (let j = 0; j < cols[i].children[1].children.length; j++)
						cols[i].children[1].children[j].classList.add('updateImg');

					let tmpHtml = cols[i].children[1].innerHTML;
					cols[i].children[1].innerHTML = `<input
						type='file' class='form-control'
						name='${cols[i].children[0].dataset.name}${cols[i].children[0].dataset.multi == 1 ? '[]' : ''}'
						${cols[i].children[0].dataset.multi == 1 ? 'multiple' : ''}
					>`;
					cols[i].children[1].innerHTML += tmpHtml;
					updateImg();

				} else if (['list'].includes(cols[i].children[0].dataset.type)) {
					let selected = cols[i].children[0].dataset.multi == '1' ? cols[i].children[1].innerText.split(',') : [cols[i].children[1].innerText];
					cols[i].children[1].innerHTML = `<select
						${cols[i].children[0].dataset.multi == '1' ? 'multiple' : ''}
						id='${cols[i].children[0].dataset.name}'
						class='form-control'
						name='${cols[i].children[0].dataset.multi == '1' ? cols[i].children[0].dataset.name + '[]' : cols[i].children[0].dataset.name}'
					><option value=''>Значение не установлено</option></select>`;
					getList(cols[i].children[0].dataset.name.split('_')[1], createOptions, cols[i].children[0].dataset.name, selected);

				} else if (['crm'].includes(cols[i].children[0].dataset.type)) {
					let crm_code = cols[i].children[0].dataset.crm.split('_');
					let value = (cols[i].children[0].dataset.crm !== undefined) ? cols[i].children[0].dataset.crm : ' ';
					cols[i].children[1].innerHTML = `
					<input type='text' class='form-control range'
						name='${cols[i].children[0].dataset.name}'
						value='${cols[i].children[1].innerText + '|' + value}' id='${cols[i].children[0].dataset.name}'
					>`;
					setCRMSetting(cols[i].children[0].dataset.name, FIELDS[cols[i].children[0].dataset.name]);

				} else if (['employee'].includes(cols[i].children[0].dataset.type)) {
					let str = '';
					str = `<select class='form-control' name='${cols[i].children[0].dataset.name}'>`;
					for (let user in USERS)
						str += `<option ${cols[i].children[1].innerText == USERS[user] ? 'selected' : ''} value='${user}'>${USERS[user]}</option>`;
					cols[i].children[1].innerHTML = str + '</select>';

				} else if (['text'].includes(cols[i].children[0].dataset.type) || cols[i].children[0].dataset.name == 'DESCRIPTION') {
					cols[i].children[1].innerHTML = `<textarea class='form-control'
						name='${cols[i].children[0].dataset.name}'>${cols[i].children[1].innerText}</textarea>`
					
				} else if (['string', 'integer', 'money', 'double'].includes(cols[i].children[0].dataset.type)){
					cols[i].children[1].innerHTML = `<input
						type='${cols[i].children[0].dataset.type == 'string' ? 'text' : 'number'}'
						class='form-control' value='${cols[i].children[1].innerText}'
						name='${cols[i].children[0].dataset.name}'
					>`;
				}
			}
			container.append(btn);
		}
	});

	async function updateProduct () {
		if (DELETE_PHOTOS) deletePhoto(DELETE_PHOTOS);
		if (UPDATE_PHOTOS) updatePhoto(UPDATE_PHOTOS);
		DELETE_PHOTOS = [];
		UPDATE_PHOTOS = [];

		let cols = [...document.querySelectorAll('.tr-detail')];
		let formData = new FormData();
		formData.append('ID', cols[0].children[1].innerText);
		for (let i = 0; i < cols.length; i++){
			if (cols[i].children[1].children.length <= 0) continue;
			if (cols[i].children[0].dataset.type == 'product_file') {
				for (let x = 0; x < cols[i].children[1].children[0].files.length; x++)
					formData.append(cols[i].children[1].children[0].name, cols[i].children[1].children[0].files[x]);

			} else if (cols[i].children[0].dataset.type == 'list' && cols[i].children[0].dataset.multi == '1') {
				let listValue = [];
				for (let j = 0; j < cols[i].children[1].children[0].length; j++) {
					if (cols[i].children[1].children[0][j].selected == true)
						listValue.push(cols[i].children[1].children[0][j].value);
				}
				formData.append(cols[i].children[1].children[0].name, listValue);

			} else if (cols[i].children[0].dataset.name == ADDRESS_CLASSIFIER) {
				formData.append(cols[i].children[1].children[0].name, cols[i].children[1].children[0].value);
				for (let y in CURRENT_ADDRESS) formData.append(y, CURRENT_ADDRESS[y]);

			} else if (cols[i].children[0].dataset.type == 'char')
				formData.append(cols[i].children[1].children[0].name, (cols[i].children[1].children[0].checked == true ? 'Y' : 'N'));
			else
				formData.append(cols[i].children[1].children[0].name, cols[i].children[1].children[0].value);
		}
		formData.append('SECTION_ID', checkSection());
		[...document.querySelectorAll('.data-detail')].map(value => {
			value.removeEventListener('dblclick', openDetailWindow);
		});

		let response = await fetch(PATH + '/apps/list_product/update_product.php', {
			method: 'POST',
			body: formData
		});

		if (response.ok) {
			console.log('Answer server: OK');
			document.querySelector('.detail-add').style.display = 'none';
			let success = document.querySelector('.alert-success');
			success.style.display = 'block';
			success.innerText = 'Обновление данных товара прошло успешно!';
			setTimeout(() => success.style.display = 'none', 4000);
			getDataTable(currentFields, CHOOSE_FILTER, renderData);

		} else {
			console.log("Error HTTP: " + response.status);
			document.querySelector('.detail-add').style.display = 'none';
			DELETE_PHOTOS = [];
			UPDATE_PHOTOS = [];

			let alert = document.querySelector('.alert-danger');
			alert.style.display = 'block';
			alert.innerText = 'Не удалось обновить товар!';
			setTimeout(() => alert.style.display = 'none', 4000);
		}

		document.querySelector('.detail-info').dataset.update = '0';
		document.querySelector('.detail-product-window').style.display = 'none';
	}

	function checkSection () {
		return document.querySelector('[data-int]').nextElementSibling.innerText;
	}

	function updateImg() {
		[...document.querySelectorAll('.updateImg')].map(value => {
			value.removeEventListener('click', showImages);
			value.addEventListener('mousedown', dragDrop);
		});
	}

	async function deletePhoto (data) {
		let url = PATH + '/apps/list_product/delete_photo.php';
		let response = await fetch(url, {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify(data)
		});

		if (response.ok) {
			let json = await response.json();
			console.log('Answer server: ' + json);
		} else console.log("Error HTTP: " + response.status);
	}

	async function updatePhoto (data) {
		let url = PATH + '/apps/list_product/update_photo.php';
		let response = await fetch(url, {
			method: 'POST',
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			body: JSON.stringify(data)
		});

		if (response.ok) {
			let json = await response.json();
			console.log('Answer server: ' + json);
		} else console.log("Error HTTP: " + response.status);
	}

	// добавление поля в детальную информацию
	function addFieldForDetail (e) {
		let contextWindow = document.querySelector('.context-detail');
		contextWindow.innerHTML = '';
		contextWindow.classList.toggle('block');
		contextWindow.style.top = e.target.getBoundingClientRect().top + e.target.getBoundingClientRect().height + 'px' ;
		contextWindow.style.left = e.target.getBoundingClientRect().left - (contextWindow.getBoundingClientRect().width / 2) + 'px';
		document.addEventListener('click', function (event) {
			if ([...contextWindow.classList].includes('block') && event.target != contextWindow && event.target != e.target)
				contextWindow.classList.remove('block');
		});

		let buttons = document.createElement('div');
		for (let i in FIELDS) {
			if (['CODE', 'XML_ID', 'SORT'].includes(i)) continue;
			if (currentDetailFields.includes(i)) continue;
			if (['map'].includes(FIELDS[i].type)) continue;
			let button = document.createElement('button');
			button.addEventListener('click', addDetailField);
			button.classList.add('btn');
			button.classList.add('btn-block');
			button.classList.add('btn-light');
			button.dataset.id = i;
			button.innerText = FIELDS[i].name;
			buttons.append(button);
		}
		contextWindow.append(buttons);
	}

	function addDetailField (e) {
		let tbody = document.querySelector('.table'), td = '';
		currentDetailFields.push(e.target.dataset.id);
		if (['date', 'datetime'].includes(FIELDS[e.target.dataset.id].type)) {
			td = `<input type='date' class='form-control' name='${e.target.dataset.id}' placeholder='${e.target.dataset.id}'>`;

		} else if (e.target.dataset.id == ADDRESS_CLASSIFIER) {
			td = `<input
				type='${FIELDS[e.target.dataset.id].type == 'string' ? 'text' : 'number'}'
				class='form-control' name='${e.target.dataset.id}'>`;
			bindAddress();

		} else if (FIELDS[e.target.dataset.id].type == 'list') {
			td = `<select name='${e.target.dataset.id}' id='add_${e.target.dataset.id}' class='form-control'>
				<option value=''>Значение не установлено</option>
			</select>`;
			getList(e.target.dataset.id.split('_')[1], createOptions, 'add_' + e.target.dataset.id);

		} else if (e.target.dataset.id == 'CURRENCY_ID') {
			td = `<select name='${e.target.dataset.id}' class='form-control'>
						<option value='RUB'>Российский рубль</option>
						<option value='USD'>Доллар США</option>
						<option value='EUR'>Евро</option>
						<option value='UAH'>Гривна</option>
						<option value='BR'>Белорусский рубль</option>
					</select>`;

		} else if (FIELDS[e.target.dataset.id].type == 'crm') {
			td = `<input type='text' class='form-control range' name='${e.target.dataset.id}' id='${e.target.dataset.id}'>`;
			setCRMSetting(e.target.dataset.id, FIELDS[e.target.dataset.id]);

		} else if (FIELDS[e.target.dataset.id].type == 'employee') {
			td = `<select class='form-control' name='${e.target.dataset.id}'>`;
			for (let user in USERS) td += `<option value='${user}'>${USERS[user]}</option>`;
			td += '</select>'

		} else if (FIELDS[e.target.dataset.id].type == 'product_file') {
			td = `<input
				type='file'
				class='form-control' name='${(FIELDS[e.target.dataset.id].multi == 'Y') ? e.target.dataset.id + '[]' : e.target.dataset.id}'
				${(FIELDS[e.target.dataset.id].multi == 'Y') ? 'multiple' : ''}
			>`;

		} else {
			td = `<input
				type='${FIELDS[e.target.dataset.id].type == 'string' ? 'text' : 'number'}'
				class='form-control' name='${e.target.dataset.id}'>`;
		}

		let html = `<tr class='tr-detail'>
			<td data-type='${FIELDS[e.target.dataset.id].type}'>${e.target.innerText}</td>
			<td>${td}</td>
		</tr>`;
		tbody.children[0].innerHTML += html;
	}

	// галерея
	function showImages (imagesClass) {
		[...document.querySelectorAll(`.${imagesClass}`)].map(value => {
			value.addEventListener('click', goGallery);
		});
	}
	function goGallery (e) {
		let target, i = 0;
		let display = document.querySelector('.g-display');
		display.style.display = 'block';
		display.children[1].innerHTML = '';

		if (e.target.tagName == 'IMG') target = e.target.parentNode;
		else target = e.target;

		let img = document.createElement('img');
		img.src = target.children[i].src;
		img.classList.add('img-gallary');
		display.children[1].append(img);
		display.children[1].addEventListener('click', next);
		
		function next () {
			i += 1;
			if (i >= target.children.length) i = 0;
			display.children[1].innerHTML = '';

			let img = document.createElement('img');
			img.src = target.children[i].dataset.src;
			img.classList.add('img-gallary');
			display.children[1].append(img);
			event.stopPropagation();
		}

		document.querySelector('.g-close').addEventListener('click', function () {
			display.children[1].innerHTML = '';
			display.children[1].removeEventListener('click', next);
			display.style.display = 'none';
		});
		e.stopPropagation();
	}

	// сохраненные поля в localstorage
	function getDefaultFields () {
		for (let i in localStorage) {
			if (localStorage.hasOwnProperty(i))
				if(i.includes('field&')) currentFields.push(i.split('&')[1]);
		}
	}

	//
	function isSaveEditProduct () {
		return new Promise ((rs, rj) => {
			let modalSave = document.querySelector('.modalSave');
			modalSave.style.display = 'block';
			document.querySelector('.yes').addEventListener('click', yes);
			document.querySelector('.no').addEventListener('click', function () {
				modalSave.style.display = 'none';
				rs();
			});

			function yes () {
				updateProduct();
				modalSave.style.display = 'none';
				rs();
				document.querySelector('.yes').removeEventListener('click', yes);
			}
		});
	}

	// инициализация таблицы
	function initDataTable () {
		let scroll = document.querySelector('.thead').children[0].children.length > 10 ? true : false;
		$(document).ready(function() {
		    $('#example').DataTable({
        		"scrollY": 600,
        		"scrollX": scroll
    		});
    		resizeTable();
			renderPreview();
			initPreviewListener();
		});

		[...document.querySelectorAll('.data-detail')].map(value => {
			value.addEventListener('dblclick', openDetailWindow);
		});
	}

	// уменьшаем размер хедера таблицы
	function resizeTable () {
		let border = document.querySelector('.topborder');
		let length = document.querySelector('#example_length');
		let filter = document.querySelector('#example_filter');
		border.append(length);
		border.append(filter);

		length.style['position']   = 'fixed';
		length.style['margin-top'] = '3px';
		length.style['top']        = '2%';
		length.style['right']      = '27%';
		length.style['z-index']    = '999';

		filter.style['position']   = 'fixed';
		filter.style['top']        = '2%';
		filter.style['right']      = '3%';
		filter.style['z-index']    = '999';
	}

	function removeElementTable () {
		let length = document.querySelector('#example_length');
		let filter = document.querySelector('#example_filter');
		if (length && filter) {
			length.remove();
			filter.remove();
		}
	}

	// групповое редактирование
	document.querySelector('.btn-group-edit').addEventListener('click', getGroupUpdate);
	async function getGroupUpdate (e) {
		let form = document.querySelector('.f-bottom');
		let formData = new FormData(form);
		formData.append('ids', CHECKED_ID);
		CHECKED_ID = [];
		
		let url = PATH + '/apps/list_product/group_update.php';
		let response = await fetch(url, {
			method: 'POST',
			body: formData
		});

		if (response.ok) {
			let json = await response.json();
			[...document.querySelectorAll('.bottom-block')].forEach(value => {
				value.classList.remove('bottom-block');
			});
			getDataTable(currentFields, CHOOSE_FILTER, renderData);
			console.log('Answer server: OK');

		} else {
			let danger = document.querySelector('.alert-danger');
			danger.style.display = 'block';
			console.log("Error HTTP: " + response.status);
			setTimeout(() => danger.style.display = 'none', 4000);
		}
	}

	function checkBox () {
		let checkBox = [...document.querySelectorAll('.check-box')];
		checkBox.forEach(value => {
			value.addEventListener('click', function (e) {
				if ([...this.classList].includes('filter-menu')) {
					this.classList.toggle('checked');
					this.dataset.checked = 1 - this.dataset.checked;
					CHECKED_ID = [];

					checkBox.forEach(one => {
						if (this.dataset.checked > 0) {
							if (![...one.classList].includes('filter-menu')) {
								one.classList.add('checked');
								one.dataset.checked = 1;
								CHECKED_ID.push(one.parentNode.nextSibling.innerText);
							}

						} else {
							one.classList.remove('checked');
							one.dataset.checked = 0;
						}
					});

				} else {
					this.classList.toggle('checked');
					this.dataset.checked = 1 - this.dataset.checked;
					if (this.dataset.checked == 1)
						CHECKED_ID.push(this.parentNode.nextSibling.innerText);
					else
						CHECKED_ID.splice(CHECKED_ID.indexOf(this.parentNode.nextSibling.innerText), 1);
				}
				e.stopPropagation();
			})
		});
	}

	function formForGroupEditing () {
		let select = document.createElement('select');
		select.classList.add('form-control');
		select.classList.add('form-control-bottom');
		let option = document.createElement('option');
		option.value = '0';
		option.innerText = 'Поле не выбрано';
		select.append(option);

		let optionArray = [];
		for (let i in FIELDS) {
			optionArray.push({name:FIELDS[i].name, field:i});
		}

		optionArray.sort((a, b) => a.name > b.name ? 1 : -1);
		for (let i = 0; i < optionArray.length; i++) {
			if ([
				'CODE', 'XML_ID', 'SORT', 'NAME', 'ID', 'SECTION_ID', 'DATE_CREATE',
				'TIMESTAMP_X', 'MODIFIED_BY', 'CREATED_BY', 'CATALOG_ID', 'PREVIEW_PICTURE',
				'DETAIL_PICTURE', ADDRESS_CLASSIFIER, PUBLISH
			].includes(optionArray[i].field)) continue;
			let option = document.createElement('option');
			option.value = optionArray[i].field;
			option.innerText = optionArray[i].name;
			select.append(option);
		}
		select.addEventListener('change', createValueField);
		let bottomFields = document.querySelector('.bottom-fields');
		bottomFields.innerHTML = '';
		bottomFields.append(select);
	}

	function createValueField (e) {
		[...document.querySelectorAll('.bottom-hide')]
		.forEach(value => {
			if (![...value.classList].includes('bottom-block'))
				value.classList.add('bottom-block');

			if ([...value.classList].includes('value-fields')) {
				let field = document.querySelector('.form-control-bottom');
				if (field.value !== '0') {
					let html = '';
					if (FIELDS_TYPE[FIELDS[field.value].type] == 'textarea' || field.value == 'DESCRIPTION') {
						html += `<textarea name='${field.value}' class='form-control' rows='3'></textarea>`;

					} else if (FIELDS_TYPE[FIELDS[field.value].type] == 'select') {
						html += `<select 
							id='bottom-select'
							name='${FIELDS[field.value].multi == 'Y' ? field.value + '[]' : field.value}'
							${FIELDS[field.value].multi == 'Y' ? 'multiple' : ''} class='form-control'><option value=''>Значение не установлено</option></select>`;

						getList(field.value.split('_')[1], createOptions, 'bottom-select');

					} else if (FIELDS_TYPE[FIELDS[field.value].type] == 'crm') {
						html += `<input type='text' class='form-control col' name='${field.value}' id='${field.value}'>`;
						setCRMSetting(field.value, FIELDS[field.value]);

					} else if (FIELDS_TYPE[FIELDS[field.value].type] == 'employee') {
						html += ` <select class='form-control' name='${field.value}'>`;
						for (let i in USERS)
							html += `<option value='${i}'>${USERS[i]}</option>`;
						html += '</select>';

					} else if (field.value == 'CURRENCY_ID') {
						html += `
							<select name='${field.value}' class='form-control'>
								<option value='RUB'>Российский рубль</option>
								<option value='USD'>Доллар США</option>
								<option value='EUR'>Евро</option>
								<option value='UAH'>Гривна</option>
								<option value='BR'>Белорусский доллар</option>
							</select>`;

					} else {
						html += `
							<input 
								class='${
									FIELDS_TYPE[FIELDS[field.value].type] == 'file' 
										? 'form-control-file' 
										: FIELDS_TYPE[FIELDS[field.value].type] == 'checkbox' ? 'form-check-label' : 'form-control'}'
										type='${FIELDS_TYPE[FIELDS[field.value].type]}' name='${field.value}${FIELDS[field.value].multi == 'Y' ? '[]' : ''}'
							>`;
					}
					value.innerHTML = html;
				}
			}
		});
	}

	// поиск собственика по телефону
	function setCRMSetting (crmId, data) {
		let enitites = ['COMPANY', 'DEAL', 'LEAD', 'CONTACT'];
		let translate = {company:'Компания', deal:'Сделка', lead:'Лид', contact:'Контакт'}
		let entityTypes = enitites
		.filter(value => data.type_setting[value] == 'Y' || data.type_setting[value] == 'VISIBLE')
		.map(value => value.toLowerCase());
		let currentType = entityTypes[1];
		
		let promise = new Promise((rs) => {
			let crmField
			setTimeout(() => {
				crmField = document.querySelector(`#${crmId}`);
				rs(crmField);
			}, 500);
		});
		promise.then(data => {
			data.addEventListener('click', function (trigger) {
				let findWndw = document.createElement('div');
				findWndw.classList.add('finder');

				let titleWndw = document.createElement('p');
				titleWndw.innerText = 'Поиск данных CRM';
				titleWndw.classList.add('mt-4');
				titleWndw.classList.add('text-center');
				titleWndw.classList.add('h5');
				findWndw.append(titleWndw);

				let typesLinksContainer = document.createElement('div');
				typesLinksContainer.classList.add('mt-1');
				typesLinksContainer.classList.add('mb-1');
				typesLinksContainer.style['display'] = 'flex';
				typesLinksContainer.style['justify-content'] = 'space-around';
				typesLinksContainer.style['width'] = '90%';
				typesLinksContainer.style['margin'] = '0 auto';

				for (let i = 0; i < entityTypes.length; i++) {
					let link = document.createElement('span');
					link.classList.add('badge');
					link.classList.add('badge-light');
					link.innerText = translate[entityTypes[i]];
					link.dataset.type = entityTypes[i];
					link.addEventListener('click', function (e) {
						[...document.querySelectorAll('.badge')].forEach(value => value.classList.remove('badge-active'));
						e.target.classList.add('badge-active');
						currentType = link.dataset.type;
					});
					typesLinksContainer.append(link);
				}
				findWndw.append(typesLinksContainer);

				let titleField = document.createElement('input');
				titleField.classList.add('form-control');
				titleField.placeholder = 'по имени';
				titleField.style['width'] = '90%';
				titleField.style['display'] = 'block';
				titleField.style['margin'] = '0 auto';
				titleField.addEventListener('input', e => {
					getBXValueByTitle(currentType, e.target.value, trigger.target);
				});
				findWndw.append(titleField);

				let surname = document.createElement('input');
				surname.classList.add('form-control');
				surname.classList.add('mt-2');
				surname.placeholder = 'по фамилии';
				surname.style['width'] = '90%';
				surname.style['display'] = 'block';
				surname.style['margin'] = '0 auto';
				surname.addEventListener('input', e => {
					getBXValueBySurname(currentType, e.target.value, trigger.target);
				});
				findWndw.append(surname);

				let finderField = document.createElement('input');
				finderField.classList.add('form-control');
				finderField.classList.add('mt-2');
				finderField.placeholder = 'по телефону';
				finderField.style['width'] = '90%';
				finderField.style['display'] = 'block';
				finderField.style['margin'] = '0 auto';
				finderField.addEventListener('input', e => {
					getBXValue(currentType, e.target.value, trigger.target);
				});
				findWndw.append(finderField);

				let close = document.createElement('span');
				close.classList.add('finderClose');
				close.addEventListener('click', e => e.target.parentNode.remove());
				findWndw.append(close);

				let valueContainer = document.createElement('div');
				valueContainer.classList.add('border-top');
				valueContainer.classList.add('mt-3');
				valueContainer.classList.add('valueList');
				findWndw.append(valueContainer);

				document.body.append(findWndw);
			});
		});
	}

	function getBXValue (types, filterValue, trigger) {
		let filter = types == 'contact' ? '?' + PHONE_FINDER : 'PHONE';
		let value = filterValue + '%';
		let method = 'crm.' + types + '.list';
		BX24.callMethod(method, {filter:{[filter]:value}, select:['*','PHONE']}, function (res) {
			if (res.data) showAnswer(res.answer, trigger, types);
		});
	}
	function getBXValueByTitle (types, filterValue, trigger) {
		let filter = types == 'contact' ? '?NAME' : '?TITLE';
		let value = '%' + filterValue + '%';
		let method = 'crm.' + types + '.list';
		BX24.callMethod(method, {'filter':{[filter]:value}, select:['*','PHONE']}, function (res) {
			if (res.data) showAnswer(res.answer, trigger, types);
		});
	}
	function getBXValueBySurname (types, filterValue, trigger) {
		let filter = types == 'contact' ? '?LAST_NAME' : '?TITLE';
		let value = '%' + filterValue + '%';
		let method = 'crm.' + types + '.list';
		BX24.callMethod(method, {'filter':{[filter]:value}, select:['*','PHONE']}, function (res) {
			if (res.data) showAnswer(res.answer, trigger, types);
		});
	}

	function showAnswer (result, trigger, types) {
		let display = document.querySelector('.valueList');
		let type = {company:'CO', contact:'C', lead:'L', deal:'D'};
		display.innerHTML = '';
		if (result.result.length > 0) {
			for (let i = 0; i < result.result.length; i++) {
				let element = document.createElement('div');
				element.classList.add('border-bottom');
				element.classList.add('finder-element');
				element.addEventListener('click', e => {
					inputValue(e, trigger);
				});
				let title = document.createElement('p');
				title.classList.add('ml-3');
				title.classList.add('pt-2');

				if (types == 'contact') {
					let name = result.result[i].NAME || '';
					let lastName = result.result[i].LAST_NAME || '';
					let phone = result.result[i].PHONE ? result.result[i].PHONE[0]['VALUE'] : 'не указан';
					title.innerText = name + ' ' + lastName + ' тел.' + phone;
					title.dataset.value = result.result[i].NAME + '|' + type[types] + '_' + result.result[i].ID;

				} else {
					title.innerText = result.result[i].TITLE;
					title.dataset.value = result.result[i].TITLE + '|' + type[types] + '_' + result.result[i].ID;
				}

				element.append(title);
				display.append(element)
			}

		} else {
			let btnAddEntity = document.createElement('div');
			btnAddEntity.classList.add('addEntity');
			btnAddEntity.innerHTML = '<p class="text-center">Создать</p>';
			btnAddEntity.addEventListener('click', function () {addEntity(this, trigger)});
			display.append(btnAddEntity);
		}
	}

	function inputValue (e, trigger) {
		trigger.value = e.target.dataset.value;
		e.target.parentNode.parentNode.parentNode.remove();
	}

	function addEntity (e, trigger) {
		let badge = document.querySelector('.badge-active');
		let type = badge ? badge.dataset.type : 'company';

		let display = e.parentElement.parentElement;
		[...display.children]
		.forEach(value=> {
			if (!['P', 'SPAN'].includes(value.tagName)) value.remove();
			if (['P'].includes(value.tagName)) value.innerText = 'Создание нового элемента';
		});

		let form = document.createElement('form');
		form.classList.add('mt-3');
		form.classList.add('p-3');
		if (type == 'contact') {
			form.innerHTML = `
				<div class='form-group'>
					<label for='NAME'>Имя</label>
					<input class='form-control' type='text' name='NAME' id='NAME'>
				</div>

				<div class='form-group'>
					<label for='LAST_NAME'>Фамилия</label>
					<input class='form-control' type='text' name='LAST_NAME' id='LAST_NAME'>
				</div>

				<div class='form-group'>
					<label for='PHONE'>Телефон</label>
					<input class='form-control' type='text' name='PHONE' id='PHONE'>
				</div>

				<div class='form-group'>
					<label for='EMAIL'>Email</label>
					<input class='form-control' type='email' name='EMAIL' id='EMAIL'>
				</div>
			`;

		} else {
			form.innerHTML = `
				<div class='form-group'>
					<label for='TITLE'>Заглавие</label>
					<input class='form-control' type='text' name='TITLE' id='TITLE'>
				</div>

				<div class='form-group'>
					<label for='PHONE'>Телефон</label>
					<input class='form-control' type='text' name='PHONE' id='PHONE'>
				</div>

				<div class='form-group'>
					<label for='EMAIL'>Email</label>
					<input class='form-control' type='email' name='EMAIL' id='EMAIL'>
				</div>
			`;
		}

		let btn = document.createElement('button');
		btn.classList.add('btn');
		btn.classList.add('btn-success');
		btn.classList.add('btn-block');
		btn.style['width'] = '92%';
		btn.style['margin'] = '0 auto';
		btn.innerText = 'Создать';
		btn.addEventListener('click', async function (e) {
			let formData = new FormData(form);
			formData.append('ASSIGNED_BY_ID', CURRENT_USER['user']['ID']);
			formData.append('type', type);
			let url = PATH + '/apps/list_product/add_entity.php';
			let response = await fetch(url, {
				method: 'POST',
				body: formData
			});

			if (response.ok) {
				let json = await response.json();
				trigger.value = json;
				console.log('Answer server: Ok');
				display.remove();
			}
		});

		display.append(form);
		display.append(btn);
	}

	// новая система работы с картинками
	function dragDrop (event) {
		let field = event.target.parentNode.previousElementSibling.dataset.name;
		createColorDisplay();
		event.target.ondragstart = function() { return false; };
		let shiftX = event.clientX - event.target.getBoundingClientRect().left;
		let shiftY = event.clientY - event.target.getBoundingClientRect().top;
		event.target.style['position'] = 'absolute';
		event.target.style['z-index'] = '999';
		event.target.style['cursor'] = 'grabbing';

		document.body.append(event.target);
		moveAt(event.pageX, event.pageY);

		function moveAt (x, y) {
			event.target.style.left = x - shiftX + 'px';
		    event.target.style.top = y - shiftY  + 'px';
		}
		function onMouseMove (e) {
			moveAt(e.pageX, e.pageY);
		}

		document.addEventListener('mousemove', onMouseMove);
		event.target.onmouseup = function (downevent) {
			document.removeEventListener('mousemove', onMouseMove);
			event.target.onmouseup = null;
			event.target.style['cursor'] = 'grab';

			// удаление
			let trash = document.querySelector('.remove');
			let trashLeft = trash.getBoundingClientRect().x;
			let trashTop = trash.getBoundingClientRect().y;
			let trashRight = trashLeft + trash.getBoundingClientRect().width;
			let trashBottom = trashTop + trash.getBoundingClientRect().height;

			// публичные
			let public = document.querySelector('.public');
			let publicLeft = public.getBoundingClientRect().x;
			let publicTop = public.getBoundingClientRect().y;
			let publicRight = publicLeft + public.getBoundingClientRect().width;
			let publicBottom = publicTop + public.getBoundingClientRect().height;

			// не публичные
			let notpublic = document.querySelector('.not-public');
			let notpublicLeft = notpublic.getBoundingClientRect().x;
			let notpublicTop = notpublic.getBoundingClientRect().y;
			let notpublicRight = notpublicLeft + notpublic.getBoundingClientRect().width;
			let notpublicBottom = notpublicTop + notpublic.getBoundingClientRect().height;

			if ((downevent.pageX >= trashLeft && downevent.pageX <= trashRight) && (downevent.pageY >= trashTop && downevent.pageY <= trashBottom)) {
				let id = document.querySelector('td[data-name=ID]').nextElementSibling.innerText;
				let file = event.target.dataset.src.split('fileId=')[1];
				DELETE_PHOTOS.push({id, file, field, multi:FIELDS[field].multi});
				event.target.remove();

			} else if ((downevent.pageX >= publicLeft && downevent.pageX <= publicRight) && (downevent.pageY >= publicTop && downevent.pageY <= publicBottom)) {
				let id = document.querySelector('td[data-name=ID]').nextElementSibling.innerText;
				let file = event.target.dataset.src.split('fileId=')[1];
				event.target.style['position'] = 'static';
				UPDATE_PHOTOS.push({id, file, fromfield:NOT_PUBLISH, tofield:PUBLISH, multi:FIELDS[NOT_PUBLISH].multi});
				document.querySelector('[data-public]').append(event.target);

			} else if ((downevent.pageX >= notpublicLeft && downevent.pageX <= notpublicRight) && (downevent.pageY >= notpublicTop && downevent.pageY <= notpublicBottom)) {
				let id = document.querySelector('td[data-name=ID]').nextElementSibling.innerText;
				let file = event.target.dataset.src.split('fileId=')[1];
				event.target.style['position'] = 'static';
				UPDATE_PHOTOS.push({id, file, fromfield:PUBLISH, tofield:NOT_PUBLISH, multi:FIELDS[PUBLISH].multi});
				document.querySelector('[data-notpublic]').append(event.target);

			}
			destroyColorDisplay();
		};
	}

	function createColorDisplay () {
		let display = document.createElement('div');
		display.classList.add('photo-display');
		display.style['width'] = '100%';
		display.style['height'] = '100%';
		display.style['position'] = 'fixed';
		display.style['z-index'] = '2';
		display.style['top']    = 0;
		display.style['left']   = 0;
		display.style['bottom'] = 0;
		display.style['right']  = 0;

		let publishPic = document.createElement('div');
		publishPic.classList.add('public');
		publishPic.innerText = 'Опубликовать';
		let notPublish = document.createElement('div');
		notPublish.innerText = 'Снять с публикации';
		notPublish.classList.add('not-public');
		let remove = document.createElement('div');
		remove.innerText = 'Удалить';
		remove.classList.add('remove');

		let tmpDiv = document.createElement('div');
		tmpDiv.style['display'] = 'flex';
		tmpDiv.style['height'] = '50%';

		tmpDiv.append(publishPic);
		tmpDiv.append(notPublish);
		display.append(tmpDiv);
		display.append(remove);

		document.body.append(display);
	}

	function destroyColorDisplay () {
		document.querySelector('.photo-display').remove();
	}

	// адресный класификатор
	function bindAddress () {
		[...document.querySelectorAll(`[name="${PHONE_FINDER}"]`)]
		.forEach (value => value.addEventListener('input', function (event) {
			let url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
			let token = DADATA_TOKEN;
			let query = event.target.value;
			
			let options = {
			    method: "POST",
			    mode: "cors",
			    headers: {
			        "Content-Type": "application/json",
			        "Accept": "application/json",
			        "Authorization": "Token " + token
			    },
			    body: JSON.stringify({query: query})
			}

			fetch(url, options)
			.then(response => response.json())
			.then(result => renderAddress(result, event.target))
			.catch(error => console.log("error", error));
			event.preventDefault();
		}));
	}

	function renderAddress(data, target) {
		const field = target;
		let coords = getCoords(target);
		let display = document.querySelector('.display-address');
		display.style['top'] = coords['top'] + 'px';
		display.style['left'] = coords['left'] + 'px';
		display.style['display'] = 'block';
		display.innerHTML = '';

		if (data.suggestions) {
			for (let i = 0; i < data.suggestions.length; i++) {
				let value = document.createElement('div');
				value.classList.add('address-value');
				value.addEventListener('click', addValue);
				let title = document.createElement('span');
				title.classList.add('ml-3');
				title.innerText = data.suggestions[i].value;
				title.dataset.index = i;
				value.append(title);
				display.append(value);
			}
		}

		function addValue (e) {
			let index = this.children[0].dataset.index;
			CURRENT_ADDRESS[FLAT_NUMBER] = data['suggestions'][index]['data']['flat']             || ' ';
			CURRENT_ADDRESS[CITY]        = data['suggestions'][index]['data']['city_with_type']   || ' ';
			CURRENT_ADDRESS[INDEX]       = data['suggestions'][index]['data']['postal_code']      || ' ';
			CURRENT_ADDRESS[HOME_NUMBER] = data['suggestions'][index]['data']['house']            || ' ';
			CURRENT_ADDRESS[REGION]      = data['suggestions'][index]['data']['region_with_type'] || ' ';
			CURRENT_ADDRESS[COUNTRY]     = data['suggestions'][index]['data']['country']          || ' ';
			CURRENT_ADDRESS[STREET]      = data['suggestions'][index]['data']['street_with_type'] || ' ';
			CURRENT_ADDRESS[LATITUDE]    = data['suggestions'][index]['data']['geo_lat']          || ' ';
			CURRENT_ADDRESS[LONGITUDE]   = data['suggestions'][index]['data']['geo_lon']          || ' ';
			field.value = this.children[0].innerText;
			this.parentNode.style['display'] = 'none';
		}
	}

	function getCoords (elem) {
		let box = elem.getBoundingClientRect();
		return {
			top: box.top + pageYOffset + 40,
			left: box.left + pageXOffset
		};
	}

	// работа с превью
	function renderPreview() {
		[...document.querySelectorAll('[data-img-src]')]
		.forEach(value => value.src = value.dataset.imgSrc);
		showImages('images');
	}

	function initPreviewListener () {
		document.querySelectorAll('.filter-menu').forEach(value => value.addEventListener('click', renderPreview));
		document.querySelector('[name="example_length"]').addEventListener('change', renderPreview);
		document.querySelector('[type="search"]').addEventListener('input', renderPreview);
		document.querySelector('.dataTables_paginate').addEventListener('click', renderPreview);
	}

	// добавление товара в сделку
	if (DEAL_CONTEXT) addProdContext();
	function addProdContext () {
		let display = document.querySelector('.product-list-display');
		let btn = document.createElement('button');
		btn.classList.add('btn');
		btn.classList.add('btn-info');
		btn.classList.add('ml-4');
		btn.classList.add('mt-3');
		btn.classList.add('mb-3');
		btn.innerText = 'Добавить в сделку';
		btn.addEventListener('click', async function (e) {
			let addIds = [...document.querySelectorAll('[data-checked="1"]')]
			.map(value => value.parentElement.nextElementSibling.innerText);
			if (addIds.length > 0) {
				let url = PATH + '/apps/list_product/context_deal.php';
				let response = await fetch(url, {
					method: 'POST',
					headers: {'Content-Type': 'application/json;charset=utf-8'},
					body: JSON.stringify({'products': addIds, 'deal': DEAL_CONTEXT})
				});

				if (response.ok) {
					console.log('Answer server: OK');
					let success = document.querySelector('.alert-success');
					success.style.display = 'block';
					success.innerText = 'Товар добавлен в сделку';
					setTimeout(() => success.style.display = 'none', 4000);

				} else {
					console.log("Error HTTP: " + response.status);
					let alert = document.querySelector('.alert-danger');
					alert.style.display = 'block';
					alert.innerText = 'Произошла ошибка!';
					setTimeout(() => alert.style.display = 'none', 4000);
				}
			}
		});
		display.append(btn);
	}
}