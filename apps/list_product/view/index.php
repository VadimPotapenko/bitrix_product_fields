<div class='container-fluid'>
	<div class='row'>
		<!-- верхняя панель -->
		<div class='col p-3 topborder'>
			<span class='filter'></span>
			<span class='filter_off'></span>
		</div>
	</div>

	<div class='row'>
		<!-- центральная панель -->
		<div class='col'>
			<div class='product-list-display'>
				<table id="example" class="display" style="max-width:100%">
			        <thead class='thead'>
			            <tr>
			            	<th class='filter-menu'><div class='check-box filter-menu text-center' data-checked='0'></div></th>
			                <th class='filter-menu'>ID</th>
			                <th class='filter-menu'>Название</th>
			                <th class='filter-menu'>Стоимость</th>
			                <th class='filter-menu'>Активность</th>
			                <th class='filter-menu'>Дата создания</th>
			            </tr>
			        </thead>
			        <tbody class='data-table'></tbody>
			    </table>
			    <span class='add'></span>
			    <div class='form-bottom mb-3 mt-3'>
			    	<form class='f-bottom'>
			    		<div class='row'>
				    		<!-- <div class='col-1 '>
								<div class='filter-menu check-box mt-2'></div>
							</div> -->
							<div class='col-3 bottom-fields ml-4'></div>
							<div class="col-3 bottom-hide value-fields"></div>
	    					<div class="col-3 bottom-hide">
	      						<button type='button' class='btn btn-info btn-group-edit'>Применить</button>
	    					</div>
	    				</div>
			    	</form>
			    </div>
			</div>
		</div>
	</div>
</div>

<!-- окно детальной информации товара -->
<div class='detail-product-window'>
	<div class='detail-container'>
		<div class='container'>
			<div class='row mt-4'>
				<div class='col detail-info table-responsive'></div>
			</div>
		</div>
	</div>
	<span class='close-detail'></span>
	<span class='edit'></span>
	<span class='detail-add'></span>
</div>


<!-- фильтр -->
<div class='detail-filter-window'>
	<div class='filter-container'>
		<div class='container'>
			<div class='row'>
				<div class='col'>
					<span class='close-filter'></span>
					<p class='h3 mt-2 text-center'>Форма фильтрации данных</p>
					<form class='create-form'></form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- галлерея -->
<div class='g-display'>
	<span class='g-close'>&#10006;</span>
	<div class='g'></div>
</div>

<!-- уведомления -->
<div class='modalSave'>
	<p class='h3 text-center mt-3 mb-3'>Сохранить изменения?</p>
	<div class='row ml-2'>
		<div class='col'>
			<button class='btn btn-info pl-3 pr-3 no'>Не сохранять</button>
		</div>
		<div class='col'>
			<button class='btn btn-success pl-3 pr-3 yes'>Сохранить</button>
		</div>
	</div>
</div>
<div class='wait'><span>Идет загрузка данных...</span></div>
<div class="alert alert-danger" role="alert"></div>
<div class="alert alert-success" role="alert"></div>