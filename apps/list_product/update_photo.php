<?php
/**
* Серверная логика публичных/не публичных фото
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#============================ settings ================================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);
$file_name = 'pic_'.time();
if (!empty($json_arr)) {
	foreach ($json_arr as $value) {
		$product = CRestPlus::call('crm.product.get', array('ID' => $value['id']));
		foreach ($product['result'][$value['fromfield']] as $v) {
			$updateData = array();
			if ($v['value']['id'] == $value['file']) {
				$file = file_get_contents($appsConfig['HOST'].$v['value']['downloadUrl']);
				$pattern = "/^content-type\s*:\s*(.*)$/i";
				if (($header = preg_grep($pattern, $http_response_header)) && (preg_match($pattern, array_shift(array_values($header)), $match) !== false))
    				$content_type = '.'.explode('/', $match[1])[1];

				$path = __DIR__.'/files/'.$file_name.$content_type;
				file_put_contents($path, $file);
				$updateData[] = array(
					'method' => 'crm.product.update',
					'params' => array(
						'ID' => $value['id'],
						'fields' => array(
							$value['fromfield'] => array('valueId' => $v['valueId'], 'value' => array('remove' => 'Y'))
						)
					)
				);

				$updateData[] = array(
					'method' => 'crm.product.update',
					'params' => array(
						'ID' => $value['id'],
						'fields' => array(
							$value['tofield'] => array(array('fileData' => array($file_name.$content_type, base64_encode(file_get_contents($path)))))
						)
					)
				);
				$del = CRestPlus::callBatch($updateData);
				unlink($path);
			}
		}
	}
}

echo json_encode('OK');