<?php
/**
* Обновление данных товара
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#============================ settings ================================#
if (isset($_POST) && !empty($_POST)) {
	foreach ($_POST as $key => $value) {
		if (empty($value) || $value == 'Значение не установлено' || in_array($key, array('TITLE', 'SECTION_ID'))) continue;
		if ($key == 'ID') {
			$id = $value;
			continue;
		}
		if ($key == 'PROPERTY_268') {
			$update['NAME'] = $_POST['SECTION_ID'].' '.$value;
			$update[$key] = $value;
			continue;
		}

		if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'crm') {
			$update[$key] = explode('|', $value)[1];
			continue;
		}

		if ($appsConfig['PRODUCT_FIELDS'][$key]['multi'] == 'Y' && $appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'list') {
			$update[$key] = explode(',', current($value));
			continue;
		}

		$update[$key] = ($appsConfig['PRODUCT_FIELDS'][$key]['multi'] == 'Y') ? array('value' => $value) : $value;
	}

	if (!empty($_FILES)) {
		foreach ($_FILES as $key => $file) {
			if (is_array($file['name'])) {
				for ($i = 0; $i < count($file['name']); $i++) {
					move_uploaded_file($file['tmp_name'][$i], __DIR__.'/files/'.$file['name'][$i]);
					$update[$key][] = array('fileData' => array($file['name'][$i], base64_encode(file_get_contents(__DIR__.'/files/'.$file['name'][$i]))));
					$path[] = __DIR__.'/files/'.$file['name'][$i];
				}

			} else {
				move_uploaded_file($file['tmp_name'], __DIR__.'/files/'.$file['name']);
				$update[$key] = array('fileData' => array($file['name'], base64_encode(file_get_contents(__DIR__.'/files/'.$file['name']))));
				$path[] = __DIR__.'/files/'.$file['name'];
			}
		}
	}

	$upProd = CRestPlus::call('crm.product.update', array('id' => $id, 'fields' => $update));
	foreach ($path as $value) unlink($value);
	echo json_encode('Ok');
}