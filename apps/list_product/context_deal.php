<?php
/**
* Добавление товара в карточку сделки
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
#============================ settings ================================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);

if (!empty($json_arr['products']) && !empty($json_arr['deal'])) {
	$dealHadProd = CRestPlus::call('crm.deal.productrows.get', array('id' => $json_arr['deal']));
	if (isset($dealHadProd['result']) && !empty($dealHadProd['result'])) {
		foreach ($dealHadProd['result'] as $value) {
			if (!in_array($value['PRODUCT_ID'], $json_arr['products']))
				array_push($json_arr['products'], $value['PRODUCT_ID']);
		}
	}

	foreach ($json_arr['products'] as $value) {
		$getProductData[] = array(
			'method' => 'crm.product.get',
			'params' => array('id' => $value)
		);
	}

	$products = CRestPlus::callBatch($getProductData);
	foreach ($products['result']['result'] as $value) {
		$rows[] = array(
			'PRODUCT_ID' => $value['ID'],
			'QUANTITY'   => '1'
		);
	}
	$setProd = CRestPlus::call('crm.deal.productrows.set', array('id' => $json_arr['deal'], 'rows' => $rows));
}
echo json_encode('OK');