<?php
/**
* Получение детальной информации о товаре
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
define ('USER_TYPE', array('MODIFIED_BY', 'CREATED_BY'));
define ('NO_FIELDS', array('CODE', 'SORT', 'XML_ID', 'TIMESTAMP_X'));
define ('PUBLISH', 'PROPERTY_384');
define ('NOT_PUBLISH', 'PROPERTY_632');
#============================ settings ================================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);

$product = CRestPlus::call('crm.product.get', array('id' => $json_arr['id']));
foreach ($product['result'] as $key => $value) {
	if (in_array($key, NO_FIELDS)) continue;
	### показ собственика если ответственный == текущий ###
	if ($key == 'PROPERTY_432') {
		if ($json_arr['user'] != $product['result']['PROPERTY_420']['value'] && !in_array($json_arr['user'], $appsConfig['ADMINS'])) {
			$returnDetailInfo[$key] = array(
				'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
				'value' => 'Значение скрыто',
				'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
			);
			continue;
		}
	}

	### section id ###
	if ($key == 'SECTION_ID') {
		$returnDetailInfo[$key] = array(
			'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
			'value' => $appsConfig['SECTIONS_LIST'][$value],
			'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type'],
			'int'   => $value
		);
		continue;
	}

	if ($key == 'PROPERTY_438') {
		$returnDetailInfo[$key] = array(
			'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
			'value' => $value['value']['TEXT'],
			'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
		);
		continue;
	}

	### ндс ###
	if ($key == 'VAT_ID') {
		$returnDetailInfo[$key] = array(
			'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
			'value' => $value == '3' ? 'НДС 20%' : 'Без НДС',
			'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
		);
		continue;
	}
	### списки ###
	if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'list') {
		if ($appsConfig['PRODUCT_FIELDS'][$key]['multi'] == 'Y') {
			$strValue = array();
			for ($i = 0; $i < count($value); $i++)
				$strValue[] = $appsConfig['LISTS'][$key][$value[$i]['value']]['VALUE'];

			$returnDetailInfo[$key] = array(
				'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
				'value' => implode(',', $strValue),
				'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type'],
				'multi' => $appsConfig['PRODUCT_FIELDS'][$key]['multi']
			);

		} else {
			$returnDetailInfo[$key] = array(
				'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
				'value' => $appsConfig['LISTS'][$key][$value['value']]['VALUE'],
				'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
			);
		}
		continue;
	}

	### type date ###
	if (in_array($appsConfig['PRODUCT_FIELDS'][$key]['type'], array('date', 'datetime')) && !empty($value)) {
		$returnDetailInfo[$key] = array(
			'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
			'value' => !is_array($value) ? date('d.m.Y', strtotime($value)) : date('d.m.Y', strtotime($value['value'])),
			'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
		);
		continue;
	}

	### type file ###
	if (($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'product_file') && !empty($value)) {
		if (!isset($value['id'])) {
			if ($key == PUBLISH) {
				$path = array();
				$notpub = array();
				foreach ($value as $v) $path[] = $v['value']['downloadUrl'];
				foreach ($product['result'][NOT_PUBLISH] as $value) $notpub[] = $value['value']['downloadUrl'];
				$returnDetailInfo[$key] = array(
					'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
					'value' => $path,
					'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
				);
				$returnDetailInfo[NOT_PUBLISH] = array(
					'title' => $appsConfig['PRODUCT_FIELDS'][NOT_PUBLISH]['name'],
					'value' => $notpub,
					'type'  => $appsConfig['PRODUCT_FIELDS'][NOT_PUBLISH]['type']
				);

			} else {
				$path = array();
				foreach ($value as $v) $path[] = $v['value']['downloadUrl'];
				$returnDetailInfo[$key] = array(
					'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
					'value' => $path,
					'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
				);
			}

		} else {
			$path = $value['downloadUrl'];
			$returnDetailInfo[$key] = array(
				'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
				'value' => $path,
				'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
			);
		}
		continue;
	}

	### user fields ### 
	if (in_array($key, USER_TYPE)) {
		$returnDetailInfo[$key] = array(
			'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
			'value' => $appsConfig['USERS'][$value],
			'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
		);
		continue;
	}

	### crm fields ###
	if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'crm') {
		$crm_info = explode('_', $value['value']);
		if (!empty(current($crm_info))) {
			$crm = CRestPlus::call('crm.'.CRM_ID[$crm_info[0]].'.get', array('id' => $crm_info[1]));
			if ($crm_info[0] == 'C') {
				$returnDetailInfo[$key] = array(
					'title'    => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
					'value'    => $crm['result']['NAME']."<br> тел.".$crm['result']['PHONE']['0']['VALUE'].'|'.$crm['result']['ID'].'|'.$crm_info[0],
					'type'     => $appsConfig['PRODUCT_FIELDS'][$key]['type'],
					'crm_code' => $crm_info[0].'_'.$crm_info[1]
				);
			} else {
				$returnDetailInfo[$key] = array(
					'title'    => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
					'value'    => $crm['result']['TITLE'].'|'.$crm['result']['ID'].'|'.$crm_info[0],
					'type'     => $appsConfig['PRODUCT_FIELDS'][$key]['type'],
					'crm_code' => $crm_info[0].'_'.$crm_info[1]
				);	
			}

		} else {
			$returnDetailInfo[$key] = array(
				'title'    => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
				'value'    => 'Значение не установлено',
				'type'     => $appsConfig['PRODUCT_FIELDS'][$key]['type'],
				'crm_code' => $crm_info[0].'_'.$crm_info[1]
			);
		}
		continue;
	}

	### множественные поля ###
	if (isset($value['value'])) {
		$returnDetailInfo[$key] = array(
			'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
			'value' => $value['value'],
			'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
		);
		continue;
	}

	$returnDetailInfo[$key] = array(
		'title' => $appsConfig['PRODUCT_FIELDS'][$key]['name'],
		'value' => $value,
		'type'  => $appsConfig['PRODUCT_FIELDS'][$key]['type']
	);
}

$getItem = CRestPlus::call('entity.item.get', array('ENTITY' => ENTITY_NAME, 'filter' => array('NAME' => 'SECTION_'.$product['result']['SECTION_ID'])));
if (isset($getItem['result']) && !empty($getItem['result'])) {
	$render = json_decode($getItem['result'][0]['PROPERTY_VALUES']['EDITOR']);
} else $render = null;
echo json_encode(array('detail' => $returnDetailInfo, 'render' => $render));