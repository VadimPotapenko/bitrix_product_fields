<?php
/**
* Сохранение crm-сущности
* является бекендом для поисковика по сущностям crm
* в случае не нахождения нужной сущности менеджер может создать сущность
* данные созданной сущности приходят сюда
* PS: все сущности подставляются автоматом, но при создании контакта требуется имя
* а для други сущностей наименование поэтому разделение на контакт и другие (company, deal)
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
define ('TYPE', array('company' => 'CO', 'deal' => 'D'));
#============================ settings ================================#
if ($_POST['type'] == 'contact') {
	$name     = $_POST['NAME']      ?? '';
	$lastName = $_POST['LAST_NAME'] ?? '';
	$phone    = $_POST['PHONE']     ?? '';
	$email    = $_POST['EMAIL']     ?? '';

	$add = CRestPlus::call('crm.contact.add', array('fields' => array(
		'NAME'      => $name,
		'LAST_NAME' => $lastName,
		'PHONE'     => array(array('VALUE' => $phone)),
		'EMAIL'     => array(array('VALUE' => $email)),
		'ASSIGNED_BY_ID' => $_POST['ASSIGNED_BY_ID']
	)));

	$return = $name.' тел.'.$phone.'|C_'.$add['result'];

} else {
	$title = $_POST['TITLE'] ?? '';
	$phone = $_POST['PHONE'] ?? '';
	$email = $_POST['EMAIL'] ?? '';

	$add = CRestPlus::call('crm.'.$_POST['type'].'.add', array(
		'fields' => array(
			'TITLE' => $title,
			'PHONE'     => array(array('VALUE' => $phone)),
			'EMAIL'     => array(array('VALUE' => $email)),
			'ASSIGNED_BY_ID' => $_POST['ASSIGNED_BY_ID']
		)
	));

	$return = $title.'|'.TYPE[$_POST['type']].'_'.$add['result'];
}

echo json_encode($return);