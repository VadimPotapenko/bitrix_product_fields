<?php
/**
* Получения товара для таблицы
*/
require_once dirname(__DIR__).'/apps_settings.php';
require_once SYSTEM_FILE;
$select = $table_headers; //TABLE_HEADERS;
$filter = array();
#================================ settings ========================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);

### выборка ###
if (isset($json_arr['select']) && !empty(current($json_arr['select']))) {
	foreach ($json_arr['select'] as $value) {
		if (in_array($value, array_keys($appsConfig['PRODUCT_FIELDS'])) && !in_array($value, $table_headers)) //TABLE_HEADERS))
			$select[] = $value;
	}
}
### фильтр ###
if (isset($json_arr['filter']) && !empty(current($json_arr['filter']))) {
	foreach ($json_arr['filter'] as $key => $value) {
		if ($key == 'crm') continue;
		else if (strstr($key, '|from'))
			$filter['>='.explode('|', $key)[0]] = $value;
		else if (strstr($key, '|to'))
			$filter['<='.explode('|', $key)[0]] = $value;
		else if (strstr($key, '|crm'))
			$filter[explode('|', $key)[0]] = explode('|', $value)[1];
		else $filter[$key] = $value;
	}
}

$products = CRestPlus::callBatchList('crm.product.list', array('select' => $select, 'filter' => $filter));
foreach ($products as $product) {
	foreach ($product['result']['result'] as $value) {
		foreach ($value as $v){
			foreach ($v as $key => $item){
				if (empty($item)) $returnProducts[$v['ID']][$key] = '';
				else {
					if ($key == 'SECTION_ID')
						$returnProducts[$v['ID']][$key] = $appsConfig['SECTIONS_LIST'][$item]; // section_id
					else if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'crm') {
						if ($key == 'PROPERTY_432') {
							if ($json_arr['user'] == $v['PROPERTY_420']['value'] || in_array($json_arr['user'], $appsConfig['ADMINS'])) {
								$crm_info = explode('_', $item['value']);
								if (!empty(current($crm_info)) && count($crm_info) != 3) {
									$crm_info['field'] = $key;
									$getCrmArray[$v['ID']] = $crm_info;
								} else $returnProducts[$v['ID']][$key] = '';
							} else $returnProducts[$v['ID']][$key] = '';

						} else {
							$crm_info = explode('_', $value['value']);
							if (!empty(current($crm_info))) {
								$crm_info['field'] = $key;
								$getCrmArray[$v['ID']] = $crm_info;
							} else $returnProducts[$v['ID']][$key] = '';
						}
					} else if ($key == 'CURRENCY_ID')
						$returnProducts[$v['ID']][$key] = CURRENCY[$item]; //
					else if ($key == 'PROPERTY_438')
						$returnProducts[$v['ID']][$key] = $item['value']['TEXT'];
					else if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'product_file')
						$returnProducts[$v['ID']][$key] = $item['downloadUrl'];
					else if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'char')
						$returnProducts[$v['ID']][$key] = ($item == 'Y') ? 'Да' : 'Нет'; // bool
					else if (in_array($appsConfig['PRODUCT_FIELDS'][$key]['type'], array('date', 'datetime')))
						$returnProducts[$v['ID']][$key] = date('d.m.Y', strtotime($item)); // date
					else if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'list') {
						if ($appsConfig['PRODUCT_FIELDS'][$key]['multi'] == 'Y') {
							$options = $LISTS[explode('_', $key)[1]];
							$strVal = '';
							for ($i = 0; $i < count($item); $i++)
								$strVal .= $options[$item[$i]['value']]['VALUE']."\n";
							$returnProducts[$v['ID']][$key] = $strVal;

						} else {
							$options = $LISTS[explode('_', $key)[1]];
							$returnProducts[$v['ID']][$key] = $options[$item['value']]['VALUE']; // lists
						}

					} else if ($key == 'VAT_ID')
						$returnProducts[$v['ID']][$key] = $item == '3' ? 'НДС 20%' : 'Без НДС'; // nds
					else if ($appsConfig['PRODUCT_FIELDS'][$key]['type'] == 'employee')
						$returnProducts[$v['ID']][$key] = $appsConfig['USERS'][$item['value']]; // users
					else if (is_array($item)) $returnProducts[$v['ID']][$key] = $item['value']; // array
					else $returnProducts[$v['ID']][$key] = $item; // default
				}
			}
		}
	}
}

foreach ($getCrmArray as $k => $v) {
	$crmData[$k] = array(
		'method' => 'crm.'.CRM_ID[$v[0]].'.get',
		'params' => array('id' => $v[1])
	);
}
if (count($crmData) > 50) $crmData = array_chunk($crmData, 50, true);
else $crmData = array($crmData);
for ($i = 0; $i < count($crmData); $i++)
	$crm[] = CRestPlus::callBatch($crmData[$i]);

foreach ($crm as $value) {
	foreach ($value['result']['result'] as $k => $v) {
		$title = ($v['NAME'] ?? $v['TITLE']).'|'.$v['ID'].'|'.$getCrmArray[$k][0];
		if ($getCrmArray[$k][0] == 'C')
			$title = $v['NAME']."<br> тел.".$v['PHONE'][0]['VALUE'].'|'.$v['ID'].'|'.$getCrmArray[$k][0];
		else
			$title = $v['TITLE'].'|'.$v['ID'].'|'.$getCrmArray[$k][0];

		$returnProducts[$k][$getCrmArray[$k]['field']] = $title ?: '';
	}
}

foreach ($select as $field) {
	foreach ($returnProducts as $k => $v) {
		if (!isset($returnProducts[$k][$field]) || empty($returnProducts[$k][$field]))
			$returnProducts[$k][$field] = '';
	}
}

echo json_encode(array('products' => $returnProducts, 'filter' => $filter_fields, 'badField' => $table_headers));