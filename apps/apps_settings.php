<?php
/**
* Файл настроек модулей приложения:
* редактор форм, добавление товара, таблица товаров, админпанель
*/
### подключаем библиотеки приложениям ###
require_once dirname(__DIR__).'/libs/crest/CRestPlus.php';
require_once dirname(__DIR__).'/libs/debugger/Debugger.php';
define ('SYSTEM_FILE', dirname(__DIR__).'/apps/system_info_'.date('d').'.php'); // системная информация
define ('SETTINGS_ENTITY', 'SETTINGS_ENTITY');  // хранилище настроек
define ('ENTITY_FOR_USERS', 'USERS_SECTIONS'); // хранилище пользователей
define ('ENTITY_NAME', 'PRODUCT_SECTIONS');   // наименование раздела
define ('PROPERTY_EDITOR', 'EDITOR');        // свойство элемента для хранения настроек отрисовки полей формы
define ('PROPERTY_LOGIC', 'LOGIC');         // свойство элемента для хранения настроек логики формы
define ('CRM_ID', array('D' => 'deal', 'L' => 'lead', 'C' => 'contact', 'CO' => 'company')); // идентификаторы crm поля
define ('CURRENCY', array('RUB' => 'Российский рубль', 'USD' => 'Доллар США', 'EUR' => 'Евро', 'UAH' => 'Гривна', 'BR' => 'Белорусский рубль'));
define ('TABLE_HEADERS', array(    // дефолтные поля таблицы
	'ID',
	'PREVIEW_PICTURE',
	'NAME',
	'PRICE',
	'CURRENCY_ID',
	'SECTION_ID',
	'VAT_ID',
	'DATE_CREATE',
	'CREATED_BY',
));
define ('FILTER', array( // типы полей фильтра: section, lists, number, range(number)(число от/до), string, crm, employee, daterange(дата от/до)
	'SECTION_ID'   => 'section',
	'NAME'         => 'string',
	'VAT_ID'       => 'number',
	'PRICE'        => 'range',
	'CREATED_BY'   => 'employee',
	'CURRENCY_ID'  => 'list'
)); // дефолтные поля фильтра
#======================== settings ====================#
### списки ###
$lists = CRestPlus::callBatchList('crm.product.property.list', array());
foreach ($lists as $value) {
	foreach ($value['result']['result'] as $v)
		foreach ($v as $item) $LISTS[$item['ID']] = $item['VALUES'];
}

### поля фильтра и таблицы ###
$setting = CRestPlus::call('entity.item.get', array('ENTITY' => SETTINGS_ENTITY, 'FILTER' => array('NAME' => 'SETTING')));
if (isset($setting['result']) && !empty($setting['result'])) {
	$table_headers = json_decode($setting['result'][0]['PROPERTY_VALUES']['TABLE_FIELDS'], 1);
	$filters = json_decode($setting['result'][0]['PROPERTY_VALUES']['FILTER_FIELDS'], 1);
	foreach ($filters as $value)
		$filter_fields[$value['field']] = $value['type'];

} else {
	$table_headers = TABLE_HEADERS;
	$filter_fields = FILTER;
}

/**
* функция обновления массива лишних секции сущности (use in array_map)
* @var n (string) - элемент массива
* @return (string) - обновленный элемент массива
*/
function updateArray ($n) { return 'SECTION_'.$n; }

/**
* функция получает значения списков
*/
function getValueListProperty ($filter) {
	if (key($filter) == 'id')
		$list = CRestPlus::call('crm.product.property.get', $filter);
	else
		$list = CRestPlus::call('crm.product.property.list', array('filter' => $filter))['result'];

	foreach ($list as $value)
		foreach ($value['VALUES'] as $v) 
			$option[$v['ID']] = $v['VALUE'];

	return $option;
}

/**
* функция сброса кеша
*/
function deleteCash () {
	for ($i = 1; $i < 5; $i++) {
		if (file_exists(__DIR__.'/system_info_'.date('d', strtotime('-'.$i.'day')).'.php'))
			unlink(__DIR__.'/system_info_'.date('d', strtotime('-'.$i.'day')).'.php');
	}
}