<?php
/**
* Файл серверных настроек приложения.
* Используемые скоупы приложения: crm, entity, users, placement
*/
require_once __DIR__.'/libs/crest/CRestPlus.php';
require_once __DIR__.'/libs/debugger/Debugger.php';
define ('CLIENT', __DIR__.'/libs/crest/settings.json'); // токены битрикс
define ('INSTALL', __DIR__.'/libs/crest/install.php'); // файл установки приложения
define ('SYSTEM_FILE', __DIR__.'/apps/system_info_'.date('d').'.php'); // системная информация
define ('HANDLER', dirname((isset($_SERVER['HTTPS']) ? 'https:' : 'http:').'//'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']).'/index.php'); // точка входа
define ('ENTITY_FOR_USERS', 'USERS_SECTIONS');  // хранилище пользователей
define ('SETTINGS_ENTITY', 'SETTINGS_ENTITY'); // хранилище настроек
define ('PLACE', 'CRM_DEAL_DETAIL_TAB');      // место куда биндим приложение
define ('TRANSLATE_TYPE_FIELDS', array(      // преобразование типов полей
	'S'          => 'string',
	'N'          => 'integer',
	'L'          => 'list',
	'F'          => 'product_file',
	'E'          => 'crm',
	'HTML'       => 'text',
	'Date'       => 'date',
	'DateTime'   => 'datetime',
	'Money'      => 'double',
	'map_yandex' => 'map',
	'employee'   => 'employee',
	'ECrm'       => 'crm'
));

### Пользовательские настройки приложения ###
define ('DADATA_TOKEN', '69b8e1ce17ea7c4b210685752ed28b822efbd96b'); // токен dadata
define ('RESPONSE_MANAGER', 'PROPERTY_420');      // ответственный менеджер
define ('ADDRESS_CLASSIFIER', 'PROPERTY_268');   // поле с которым работает компонент адресный классификатор
define ('PHONE_FINDER', 'UF_CRM_1597066694');   // поле тел для поисковика по crm
define ('PUBLISH', 'PROPERTY_384');            // поле публичной фото
define ('NOT_PUBLISH', 'PROPERTY_632');       // поле не публичной фото
define ('FLAT_NUMBER', 'PROPERTY_288');      // номер квартиры
define ('CITY',  'PROPERTY_446');           // город
define ('INDEX', 'PROPERTY_634');          // индекс
define ('HOME_NUMBER', 'PROPERTY_452');   // номер дома
define ('REGION', 'PROPERTY_442');       // регион
define ('COUNTRY', 'PROPERTY_440');     // страна
define ('STREET', 'PROPERTY_450');     // улица
define ('LONGITUDE', 'PROPERTY_638'); // долгота
define ('LATITUDE', 'PROPERTY_636'); // широта
#====================================== settings ===================================#
### установка и настройка приложения ###
if (isset($_REQUEST['PLACEMENT']) && !file_exists(CLIENT)) require_once INSTALL;
$host = json_decode(file_get_contents(CLIENT), 1);
$HOST = substr($host['client_endpoint'], 0, -6);
$needPlaceBind = true;
$getPlacement = CRestPlus::call('placement.get', array());
if (!empty($getPlacement)) {
	foreach ($getPlacement['result'] as $value)
		if ($value['placement'] == PLACE && $value['handler'] == HANDLER) $needPlaceBind = false;
}
if ($needPlaceBind)
	$bind = CRestPlus::call('placement.bind', array('PLACEMENT' => PLACE, 'HANDLER' => HANDLER));

### хранилище настроек ###
$entity = CRestPlus::call('entity.get', array('ENTITY' => SETTINGS_ENTITY));
if (!isset($entity['result']) || empty($entity['result'])) {
	CRestPlus::call('entity.add', array('ENTITY' => SETTINGS_ENTITY, 'NAME' => SETTINGS_ENTITY));
	CRestPlus::callBatch(array(
		array(
			'method'   => 'entity.item.property.add',
			'params' => array(
				'ENTITY'   => SETTINGS_ENTITY,
				'PROPERTY' => 'ADMIN',
				'NAME'     => 'ADMIN',
				'TYPE'     => 'S'
			)
		),
		array(
			'method'   => 'entity.item.property.add',
			'params' => array(
				'ENTITY'   => SETTINGS_ENTITY,
				'PROPERTY' => 'FILTER_FIELDS',
				'NAME'     => 'FILTER_FIELDS',
				'TYPE'     => 'S'
			)
		),
		array(
			'method'   => 'entity.item.property.add',
			'params' => array(
				'ENTITY'   => SETTINGS_ENTITY,
				'PROPERTY' => 'TABLE_FIELDS',
				'NAME'     => 'TABLE_FIELDS',
				'TYPE'     => 'S'
			)
		),
		array(
			'method'   => 'entity.item.property.add',
			'params' => array(
				'ENTITY'   => SETTINGS_ENTITY,
				'PROPERTY' => 'ACCESS',
				'NAME'     => 'ACCESS',
				'TYPE'     => 'S'
			)
		)
	));
}
### текущий пользователь ###
$currentUser = CRestPlus::callCurrentUser($_REQUEST);
$getResponsible = CRestPlus::call('entity.item.get', array('ENTITY' => SETTINGS_ENTITY, 'FILTER' => array('NAME' => 'SETTING')));
if (isset($getResponsible['result']) && !empty($getResponsible['result'])) {
	$responsible = json_decode($getResponsible['result'][0]['PROPERTY_VALUES']['ADMIN'], 1);
	array_push($responsible, '182');
	foreach (json_decode($getResponsible['result'][0]['PROPERTY_VALUES']['ACCESS'], 1) as $value)
		$rules[$value['user']] = array('editor' => $value['editor'], 'add' => $value['add'], 'table' => $value['table']);
} else $responsible = array($currentUser['user']['ID']);

### контекст сделки ###
$DEAL_CONTEXT = '';
if (isset($_POST['PLACEMENT_OPTIONS']) && !empty($_POST['PLACEMENT_OPTIONS']))
	$DEAL_CONTEXT = json_decode($_POST['PLACEMENT_OPTIONS'], 1)['ID'];

### системная информация ###
if (!file_exists(SYSTEM_FILE)) {
	### разделы товаров ###
	$getProductSection = CRestPlus::callBatchList('crm.productsection.list', array());
	foreach ($getProductSection as $value) {
		foreach ($value['result']['result'] as $v) {
			foreach ($v as $item) {
				$sectionList[$item['ID']] = $item['NAME'];
				if (empty($item['SECTION_ID'])) $tmpProductSection[$item['ID']][] = $item['NAME'];
				else $tmpProductSection[$item['SECTION_ID']][$item['ID']][] = $item['NAME'];
			}
		}
	}
	foreach ($tmpProductSection as $key => $value) {
		foreach ($tmpProductSection as $k => $v) {
			foreach ($v as $index => $data) {
				if ($key == $index) {
					$tmpProductSection[$k][$index][key($value)] = current($value);
					unset($tmpProductSection[$key]);
				}
			}
		}
	}

	### поля товаров ###
	$getProductsFields = CRestPlus::call('crm.product.fields', array());
	foreach ($getProductsFields['result'] as $key => $value)
		if (!stristr($key, 'PROPERTY_'))
			$productFields[$key] = array('name' => $value['title'], 'type' => $value['type'], 'multi' => $value['isMultiple'] ? 'Y' : '');

	$getProductsProperty = CRestPlus::callBatchList('crm.product.property.list', array());
	foreach ($getProductsProperty as $value) {
		foreach ($value['result']['result'] as $v) {
			foreach ($v as $item) {
				if (TRANSLATE_TYPE_FIELDS[$item['USER_TYPE'] ?: $item['PROPERTY_TYPE']] == 'crm') {
					$productFields['PROPERTY_'.$item['ID']] = array(
						'name' => $item['NAME'],
						'type' => TRANSLATE_TYPE_FIELDS[$item['USER_TYPE'] ?: $item['PROPERTY_TYPE']],
						'multi' => ($item['MULTIPLE'] == 'Y') ? 'Y' : '',
						'type_setting' => $item['USER_TYPE_SETTINGS']
					);

				} else {
					$productFields['PROPERTY_'.$item['ID']] = array(
						'name' => $item['NAME'],
						'type' => TRANSLATE_TYPE_FIELDS[$item['USER_TYPE'] ?: $item['PROPERTY_TYPE']],
						'multi' => ($item['MULTIPLE'] == 'Y') ? 'Y' : ''
					);
				}

				if ($item['PROPERTY_TYPE'] == 'L')
					foreach ($item['VALUES'] as $i => $j) {
						$lists['PROPERTY_'.$item['ID']][$i] = $j;
				}
			}
		}
	}

	### пользователи портала ###
	$users = CRestPlus::callBatchList('user.get', array('filter' => array('ACTIVE' => 'Y'), 'select' => array('ID', 'NAME', 'LAST_NAME')));
	foreach ($users as $user) {
		foreach ($user['result']['result'] as $value) {
			foreach ($value as $v)
				$USERS[$v['ID']] = $v['LAST_NAME'].' '.$v['NAME'];
		}
	}

	$appsConfig = array(
		'USERS'          => $USERS,
		'LISTS'          => $lists,
		'PRODUCT_FIELDS' => $productFields,
		'SECTIONS'       => $tmpProductSection,
		'SECTIONS_LIST'  => $sectionList,
		'ADMINS'         => $responsible,
		'HOST'           => $HOST
	);

	### кэширование данных ###
	Debugger::saveParams($appsConfig, SYSTEM_FILE);
	for ($i = 1; $i < 5; $i++) {
		if (file_exists(__DIR__.'/apps/system_info_'.date('d', strtotime('-'.$i.'day')).'.php'))
			unlink(__DIR__.'/apps/system_info_'.date('d', strtotime('-'.$i.'day')).'.php');
	}
} else require_once (SYSTEM_FILE);